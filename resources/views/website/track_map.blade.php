@extends('layouts.website2')

@section('content')
<style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 500px;
        width: 100%;
       }
    </style>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34 mt-5" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">

                
	    </div> 

    <div class="summary entry-summary mt-4 pt-5">


    	<section class="intra-product-block-title">
	    	<div class="container-fluid my-5" style="">

	    		<div class="justify-content-center">
                    <!-- /.card-header -->

                            <div class="card-body table-responsive p-0">
                                @php 
                                if(Auth::user()->hasRole('user'))
                                {
                                  $location = 'https://www.google.com/maps/dir/'.@$order->nurse->latitude.','.@$order->nurse->longitude.'/'.@$order->address->address_line.'/'.@$order->nurse->address.'?,12zhl=mr-IN'; 
                                }else{
                                  $location = 'https://www.google.com/maps/dir/'.@$order->address->lat.','.@$order->address->lng.'/'.@$order->nurse->address.'/'.@$order->address->address_line.'?,12zhl=mr-IN'; 
                                }
                                @endphp
                                <div class="pull-left">
                                  <p> Nurse &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;<b>{{@$order->nurse->name}}</b> 
                                    <br> Customer&nbsp;&nbsp; - &nbsp;&nbsp;<b>{{@$order->customer_name}}</b>
                                    <br> Distance &nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;<b><span id="distance"></span> KM</b>
                                  </p>
                                
                                </div>

                                <div class="pull-right">
                                    <a class="btn btn-info text-decoration-none" href="{{$location}}" target="_blank">Track On Google Map</a>
                                    <a href="{{url('order/details/'.$order->id)}}" class="btn btn-warning text-decoration-none px-5"> Back </a>
                                </div>
                                
                                <div id="map"></div>
                                <div class="mt-3" id="msg"></div>
                            </div>
                            <!-- /.card-body -->
                </div>
            </div>
        </section>
    </div>


		
</main>

@section('pagescript')
    
    <script type="text/javascript">
        // Initialize and add the map
        var nurse_lat = {{@$order->nurse->latitude}};
        var nurse_lng = {{@$order->nurse->longitude}};
        var nurse = "{{@$order->nurse->name}}";
        var user = "{{@$order->customer_name}}";
        var user_lat = {{@$order->address->lat}};
        var user_lng = {{@$order->address->lng}};
        var map;

       //center midpoint

       
       document.getElementById('distance').innerHTML = calcCrow(nurse_lat,nurse_lng,user_lat,user_lng).toFixed(1);
       function calcCrow(lat1, lon1, lat2, lon2) 
            {
            var R = 6371; // km
            var dLat = toRad(lat2-lat1);
            var dLon = toRad(lon2-lon1);
            var lat1 = toRad(lat1);
            var lat2 = toRad(lat2);

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c;
            return d;
            }

            // Converts numeric degrees to radians
            function toRad(Value) 
            {
                return Value * Math.PI / 180;
            }
        
       //

        function initMap(){
            //var directionsService = new google.maps.DirectionsService;
            //var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: nurse_lat,lng:user_lng},
                zoom: 10,
            });
            /*
                directionsDisplay.setMap(map);
                var onChangeHandler = function() {
                    calculateAndDisplayRoute(directionsService , directionsDisplay);
                };
                document.getElementById('start').addEventListener('change',onChangeHandler);
                document.getElementById('end').addEventListener('change',onChangeHandler);
            */
            //call add maker function
            addMaker({lat:nurse_lat,lng:nurse_lng},nurse);
            addMaker({lat:user_lat,lng:user_lng},user);
            //add maker function
            function addMaker(coords,name)
            {
                var marker = new google.maps.Marker({
                    map: map,
                    position: coords,
                    title: name,
                });
            }
        }
        
</script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&callback=initMap"></script>
@stop
@endsection
