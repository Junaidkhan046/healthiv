<footer class="intra-bg-black py-4" id="mainfooter">
    <div class="container py-5 mt-1">
        <div class="row">
            <div class="col-12 col-sm-3 mb-4 pb-1">
                <h3 class="intra-logo-footer">Health<span class="intra-bg-blue-to-yellow">IV</span></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-3">
                <p><span class="intra-bg-blue-to-yellow">Site Map</span></p>
                <ul class="list-group list-unstyled">
                    <li><a class="" href="{{ route('landing') }}">Treatments</a></li>
                    <li><a class="" href="#">Covid Testing</a></li>
                    <li><a class="" href="{{ route('blog') }}">Blog</a></li>
                    <li><a class="" href="{{ route('about') }}">About</a></li>
                    <li><a class="" href="{{ route('contact') }}">Contact</a></li>
                    <li><a class="" href="{{ route('faq') }}">FAQ</a></li>
                    @if (Route::has('login'))
                        @auth
                            <li><a class="" href="{{ url('/home') }}">Dashboard</a></li>
                        @else
                            <li><a class="" href="{{ route('login') }}">Login</a></li>
                            @if (Route::has('register'))
                            <li><a class="" href="{{ route('register') }}">Register</a></li>
                            @endif
                        @endauth
                    @endif
                </ul>
            </div>
            <div class="col-12 col-md-3">
            </div>
            <div class="col-12 col-md-3 pl-md-4 pr-md-0">
                <p class="pt-4 pt-md-0">
                    <span class="intra-bg-blue-to-yellow">Support</span>
                </p>
                <ul class="list-group list-unstyled">
                    <li>
                        <a class="" href="tel:8773812190">(877) 381-2190</a>
                    </li>
                    <li>
                        <a class="" href="email:admin@healthiv.com">
                            admin@healthiv.com
                        </a>
                    </li>
                </ul>
                <ul class="list-group list-group-horizontal-md list-unstyled">
                    <li class="pt-3 pt-md-0">
                        <a class="pr-4" href="#">
                            <img class="intra-social-icon" src="{{asset('assets/images/40x40.webp')}}">
                        </a>
                    </li>
                    <li class="pt-3 pt-md-0">
                        <a class="pr-4" href="#">
                            <img class="intra-social-icon" src="{{asset('assets/images/40x40.webp')}}">
                        </a>
                    </li>
                    <li class="pt-3 pt-md-0">
                        <a class="pr-4" href="#">
                            <img class="intra-social-icon" src="{{asset('assets/images/40x40.webp')}}">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

@yield('pagescript')
<!--sweetalert-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if(\Session::has('success'))

<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

@if(\Session::has('nurseRegister'))
<script>
    swal("Success!", "{{ Session::get('nurseRegister') }}", "success");

</script>
@endif