@extends('layouts.website')

@section('content')
<div class="jumbotron mb-3">
    <div class="container mt-5 pt-4 mb-3">
        <header class="entry-header">
            <h1 class="entry-title text-white mt-2 mb-4 pb-2">Contact Us</h1>
        </header>
        <hr class="my-4">
        <p class="pt-3">
            <a class="text-white d-block pb-4" href="mailto:contact@healthiv.com">contact@healthiv.com</a>
            <a class="text-white d-block" href="tel:8773812190">(877) 381-2190</a>
        </p>
    </div>
</div>

<div class="container">
    <div class="row mb-5">
        <div class="col-12">
            <div class="pb-5">
                <br/><br/><br/>
                <h2 class="pb-4 mb-3 mt-n3">Contact Form</h2>
                <div class="contact-enquiry-form-div">
                    <div role="form">
                        <form action="{{ route('create-enquiry') }}" method="post" id="create-form">
                            @csrf
                            <div class="intra-form-contact py-4 px-4">
                                <div class="float-lg-left">
                                    <label>
                                        First Name<br>
                                        <span class="">
                                            <input type="text" name="first_name" size="40">
                                        </span>
                                    </label>
                                </div>
                                <div class="float-lg-right">
                                    <label>
                                        Last Name<br>
                                        <span class="">
                                            <input type="text" name="last_name" size="40">
                                        </span>
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        Email<br>
                                        <span class="">
                                            <input type="text" name="email" size="40">
                                        </span>
                                    </label>
                                </div>
                                <div class="float-lg-left">
                                    <label>
                                        Phone<br>
                                        <span class="">
                                            <input type="text" name="phone" size="40">
                                        </span>
                                    </label>
                                </div>
                                <div class="float-lg-right">
                                    <label>
                                        Zip<br>
                                        <span class="">
                                            <input type="text" name="zip_code" size="40">
                                        </span>
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        Message<br>
                                        <span>
                                            <textarea name="message"></textarea>
                                        </span>
                                    </label>
                                </div>
                                <p>
                                    <input type="submit" value="Send Message" class="intra-button-black mt-4 w-100">
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('pagescript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    let $ = jQuery;
    
    // Wait for the DOM to be ready
    $(document).ready(function() {

        $("#create-form").validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "Let us know your First name."
                },
                last_name: {
                    required: "Let us know your Last name."
                },
                email: {
                    required: "Please provide your email address",
                    email: "Email address entered is not valid"
                },
                message: {
                    required: "We would like to know your thoughts."
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@stop

@endsection