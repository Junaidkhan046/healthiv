<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContentMedia;

class Content extends Model {

    protected $table = 'contents';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'subtitle', 'description'];

    public function media()
    {
        return $this->hasMany(ContentMedia::class,'content_id','id');
    }

}