<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;

class EventMedia extends Model {

    protected $table = "event_medias";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id', 'media'];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}