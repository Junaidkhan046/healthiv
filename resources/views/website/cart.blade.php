@extends('layouts.website2')

@section('content')
<article id="">

	<header class="entry-header">

		<!-- Cart Hero -->
		<div class="jumbotron intra-bg-gradient-standard mb-0 ">
			<div class="container mt-5 pt-4 mb-3">
				<h1 class="entry-title mt-2 mb-4 pb-2">Book Session</h1>

				<h2 class="mt-4 mb-4">Confirmation
				</h2>

				<hr class="my-4">
				<p>Review the details before before continuing to checkout</p>
				<p class="pt-3">
				</p>

			</div>
		</div>
		<!-- END Cart Hero -->

	</header>
</article>
@endsection