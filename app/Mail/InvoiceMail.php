<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;
    public $contact_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contact_data)
    {
        $this->contact_data = $contact_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from_name = 'umesh';
        $from_email = 'rajputumesh707804@gmail.com';
        $subject = "Your order Placed Successfully.";
        return $this->from($from_email, $from_name)
                    ->view('website.mails.paymentmail')
                    ->subject($subject);
    }
}
