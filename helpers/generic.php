<?php
  
  function getSectionContent(){
    return DB::table('contents')->latest()->first();
}

/**
 * This function is used to get Category name
 * 
 * @param of category_id
 * 
 * @return  @name
 */

 function getCategoryName($id)
 {
    return DB::table('categories')->where('id', $id)->pluck('name')->first();
 }

if (! function_exists('getModelForGuard')) {
    /**
     * @param string $guard
     *
     * @return string|null
     */
    function getModelForGuard(string $guard)
    {
        return collect(config('auth.guards'))
            ->map(function ($guard) {
                if (! isset($guard['provider'])) {
                    return;
                }

                return config("auth.providers.{$guard['provider']}.model");
            })->get($guard);
    }
}

if (! function_exists('setPermissionsTeamId')) {
    /**
     * @param int $id
     *
     */
    function setPermissionsTeamId(int $id)
    {
        app(\Spatie\Permission\PermissionRegistrar::class)->setPermissionsTeamId($id);
    }
}

/**
 * This function is used check status
 * @param of boolean
 * @return yes and no
 */
function active($bool){
    return $bool ? 'Yes' : 'No';
}

/**
 * update env value
 *
 * @param [type] $key
 * @param [type] $value
 * @return void
 */
function envUpdate($credentials) {
    $path = base_path('.env');
    if (file_exists($path)) {
        foreach($credentials as $key => $value){
            file_put_contents($path, str_replace(
                env($key), "", file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                $key . '=', $key . '=' . $value, file_get_contents($path)
            ));
        }
    }
}
