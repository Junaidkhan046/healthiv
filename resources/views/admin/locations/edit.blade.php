@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('admin-locations') }}" class="btn btn-danger">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('admin-update-location') }}" method="post" id="edit-form" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ $location->id }}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="title" placeholder="Title" value="{{ $location->title }}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="address" class="col-md-12 col-form-label text-md-left">{{ __('Address') }}</label>
                                        <input id="autocompleteaddress" type="text" class="form-control" name="address" value="{{ $location->address }}" autocomplete="false" >   
                                        <input type="hidden" name="lat" id="latitude" value="{{ $location->lat }}">
                                        <input type="hidden" name="lng" id="longitude" value="{{ $location->lng }}">
                                        <div id="ShowMap">
                                            <div id="map" style="width:100%;height:300px;margin-top:1em;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control description" id="description" name="description" required>{{ $location->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Content Media</label>
                                        <input type="file" class="form-control image">
                                    </div>
                                </div>
                                <div class="row col-sm-12" id="media-preview">
                                    @foreach($location->media as $media)
                                    <div class="col-sm-3" id="stored-image-{{$media->id}}" class="text-center">
                                        <img src="{{url('')}}/{{$media->media}}" class="image-preview">
                                        <a href="javascript:void(0)" onclick="deleteImage({{$media->id}})" class="image-preview-remove">
                                            <i class="fa fa-trash text-danger image-remove-btn"></i>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-right">
                                        <br/>
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

<!--Image Crop Wizard-->
<div class="modal fade" id="crop-tool" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Adjust Your Image</h5>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12 text-center" style="padding-left:0px;">
                            <img id="image" src="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Okay, Save</button>
            </div>
        </div>
    </div>
</div>
<!--Image Crop Wizard-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<script>
    // Wait for the DOM to be ready
    $(function() {

        $(".description").summernote();

        $("#edit-form").validate({
            rules: {
                title: {
                    required: true
                },
                address: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Title cannot be left blank"
                },
                address: {
                    required: "Address cannot be left blank"
                },
                description: {
                    required: "Description cannot be left blank"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
<script>
    var $modal = $('#crop-tool');
    var image = document.getElementById('image');
    var cropper;
    var selectedImage = 0;

    $("body").on("change", ".image", function (e) {
        var files = e.target.files;
        var done = function (url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function () {
        cropper = new Cropper(image, {
            //aspectRatio: 1,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function () {
        canvas = cropper.getCroppedCanvas({
            // width: 160,
            // height: 160,
        });
        canvas.toBlob(function (blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
                var base64data = reader.result;
                selectedImage++;
                let html = `
                    <div class="col-sm-3" id="selected-image-${selectedImage}" class="text-center">
                        <input type="text" name="media[]" value="${base64data}" style="display:none;">
                        <img src="${base64data}" class="image-preview">
                        <a href="javascript:void(0)" onclick="removeImage(${selectedImage})" class="image-preview-remove">
                            <i class="fa fa-trash text-danger image-remove-btn"></i>
                        </a>
                    </div>
                `;

                $("#media-preview").append(html);
                $modal.modal('hide');
                
            }
        });
    });

    function removeImage(id){
        $("#selected-image-"+id).remove();
        selectedImage--;
    }

    function deleteImage(id){
        swal({
            title: "Are you sure?",
            text: "You want to delete this image?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/delete-location-media') }}/"+id, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('stored-image-'+id).remove();
                        
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&libraries=places&callback=initMap" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        //document.getElementById("ShowMap").style.display = "none";
        var input = document.getElementById('autocompleteaddress');
        // create new autocomplete object from Maps API using our input
        var autocomplete = new google.maps.places.Autocomplete(input);

        // when new places are selected, process the results
        autocomplete.addListener("place_changed", function() {
            var place = autocomplete.getPlace();
            console.log(place);
            var lat = place.geometry['location'].lat();
            var lng = place.geometry['location'].lng();
            $('#latitude').val(lat);
            $('#longitude').val(lng);

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: lat,lng:lng},
                zoom: 15,
            });
            addMaker({lat:lat,lng:lng});
            //add maker function
            function addMaker(coords)
            {
                var marker = new google.maps.Marker({
                    map: map,
                    position: coords,
                    title:"your Location"
                });
            }
            document.getElementById("ShowMap").style.display = "block";
        });
    });
</script>
<script>
    var lat1 = {{$location->lat}};
    var lng1 = {{$location->lng}};
    if(lat1 != null){
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat1,lng:lng1},
            zoom: 15,
        });
        addMaker({lat:lat1,lng:lng1});
        //add maker function
        function addMaker(coords)
        {
            var marker = new google.maps.Marker({
                map: map,
                position: coords,
                title:"your Location"
            });
        }
        document.getElementById("ShowMap").style.display = "block";
    }
</script>
@stop

@endsection