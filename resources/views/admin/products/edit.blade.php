@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('products.index') }}" class="btn btn-danger">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('products.update',  $product->id) }}" method="post" id="edit-form" enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PATCH') }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name" value="{{$product->name}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Please Select Category</label>
                                        <select class="form-control" name="category_id">
                                            <option value="">Please Select Category</option>
                                            @if($cate->isNotEmpty())
                                            @foreach($cate as $val)
                                            <option value="{{$val->id}}" {{$val->id == $product->category_id ? 'selected' : ''}}>{{$val->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>SKU</label>
                                        <input type="text" class="form-control" name="sku" placeholder="SKU" value="{{$product->sku}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Selling Price</label>
                                        <input type="text" class="form-control" name="selling_price" placeholder="Selling Price" value="{{$product->selling_price}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Purchasing Price</label>
                                        <input type="text" class="form-control" name="purchasing_price" placeholder="Purchasing Price" value="{{$product->purchasing_price}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Slug</label>
                                        <input type="text" class="form-control" name="slug" placeholder="Slug" value="{{$product->slug}}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control description" id="description" name="description" required>{{$product->description}}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Product Media</label>
                                        <input type="file" class="form-control image">
                                    </div>
                                </div>
                                <div class="row col-sm-12" id="media-preview">
                                    @if($product->productImage->isNotEmpty())
                                    @foreach($product->productImage as $val)
                                    <div class="col-sm-3" id="stored-image-{{$val->id}}" class="text-center">
                                        <img src="{{url('')}}/{{$val->path}}" class="image-preview">
                                        <a href="javascript:void(0)" onclick="deleteImage('{{$val->id}}')" class="image-preview-remove">
                                            <i class="fa fa-trash text-danger image-remove-btn"></i>
                                        </a>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-right">
                                        <br />
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

<!--Image Crop Wizard-->
<div class="modal fade" id="crop-tool" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Adjust Your Image</h5>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12 text-center" style="padding-left:0px;">
                            <img id="image" src="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Okay, Save</button>
            </div>
        </div>
    </div>
</div>
<!--Image Crop Wizard-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<script>
    // Wait for the DOM to be ready
    $(function() {

        $(".description").summernote();

        $("#edit-form").validate({
            rules: {
                name: {
                    required: true
                },
                category_id: {
                    required: true
                },
                selling_price: {
                    required : true
                },
                purchasing_price: {
                    required : true
                },
                sku: {
                    required: true
                },
                slug: {
                    required: true
                },
                description: {
                    required: true
                },
            },
            messages: {
                name: {
                    required: "Name cannot be left blank"
                },
                category_id: {
                    required: "Please select product category"
                },
                selling_price:{
                    required : "Selling price cannot be blank",
                },
                purchasing_price:{
                    required : "Purchase price cannot be blank",
                },
                sku: {
                    required: "SKU cannot be left blank"
                },
                slug: {
                    required: "SKU cannot be left blank"
                },
                description: {
                    required: "Description cannot be left blank"
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
<script>
    var $modal = $('#crop-tool');
    var image = document.getElementById('image');
    var cropper;
    var selectedImage = 0;

    $("body").on("change", ".image", function(e) {
        var files = e.target.files;
        var done = function(url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            //aspectRatio: 1,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function() {
        canvas = cropper.getCroppedCanvas({
            // width: 160,
            // height: 160,
        });
        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                selectedImage++;
                let html = `
                    <div class="col-sm-3" id="selected-image-${selectedImage}" class="text-center">
                        <input type="text" name="media[]" value="${base64data}" style="display:none;">
                        <img src="${base64data}" class="image-preview">
                        <a href="javascript:void(0)" onclick="removeImage(${selectedImage})" class="image-preview-remove">
                            <i class="fa fa-trash text-danger image-remove-btn"></i>
                        </a>
                    </div>
                `;

                $("#media-preview").append(html);
                $modal.modal('hide');

            }
        });
    });

    function removeImage(id) {
        $("#selected-image-" + id).remove();
        selectedImage--;
    }

    function deleteImage(id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this image?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/products/delete-product-media') }}/" + id, {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    }
                }).then(response => response.json()).then(res => {
                    if (res.status) {
                        document.getElementById('stored-image-' + id).remove();

                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>
@stop

@endsection