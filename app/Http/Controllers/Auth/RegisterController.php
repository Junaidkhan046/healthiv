<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\NurseDocument;
use App\Providers\RouteServiceProvider;
use App\User;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['role'] == 'nurse') {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'document.*' => ['required', 'mimes:jpg,jpeg,png'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ], [
                'document.*.required' => 'Please upload an document',
                'document.*.mimes' => 'Only jpeg,jpeg,png files are allowed',
            ]);
        } else {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        try {
            DB::beginTransaction();
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'address' => $data['address'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
                'password' => Hash::make($data['password']),
            ]);
            $user->assignRole($data['role']);
            if ($data['role'] == 'nurse') {
                $images = [];
                foreach ($data['document'] as $image) {
                    // dd($image);
                    $file = $this->uploadFIle($image, 'uploads/document/');
                    array_push($images, $file);
                }
                NurseDocument::create(['user_id' => $user->id, 'document' => json_encode($images)]);
            }
            DB::commit();
            return $user;
        } catch (Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }
}
