<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('admin-dashboard') }}" class="brand-link text-center">
        <span class="brand-text font-weight-light"> Health </span><span class="text-icon-blue">IV</span>
    </a>
    <!--sidebar opens-->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">Administration</li>
                <li class="nav-item">
                    <a href="{{ route('order') }}" class="nav-link">
                        <i class="nav-icon fa fa-hashtag text-icon-blue"></i>
                        <p>Orders</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-shopping-cart text-icon-blue"></i>
                        <p>
                            Product Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('categories.index') }}" class="nav-link">
                                <i class="fa fa-barcode nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('products.index') }}" class="nav-link">
                                <i class="fa fa-shopping-basket nav-icon"></i>
                                <p>Products</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('addon.index') }}" class="nav-link">
                                <i class="fa fa-cart-plus nav-icon"></i>
                                <p>Addons</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-users text-icon-blue"></i>
                        <p>
                            User Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('customer') }}" class="nav-link">
                                <i class="fa fa-user nav-icon"></i>
                                <p>Customers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('manager') }}" class="nav-link">
                                <i class="fa fa-user-secret nav-icon"></i>
                                <p>Managers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('nurse') }}" class="nav-link">
                                <i class="fa fa-user-plus nav-icon"></i>
                                <p>Nurses</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-folder text-icon-blue"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin-under-development') }}" class="nav-link">
                                <i class="fa fa-th nav-icon"></i>
                                <p>Sales</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin-under-development') }}" class="nav-link">
                                <i class="fa fa-th nav-icon"></i>
                                <p>Payouts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin-under-development') }}" class="nav-link">
                                <i class="fa fa-th nav-icon"></i>
                                <p>DSR</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('test.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-file text-icon-green" ></i>
                        <p>Tests</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('membership.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-file text-icon-green" ></i>
                        <p>Membership</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-posts') }}" class="nav-link">
                        <i class="nav-icon fa fa-file text-icon-green" ></i>
                        <p>Blog Posts</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-locations') }}" class="nav-link">
                        <i class="nav-icon fa fa-map-marker text-icon-green"></i>
                        <p>Locations</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-events') }}" class="nav-link">
                        <i class="nav-icon fa fa-camera text-icon-green"></i>
                        <p>Events / News</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-faq') }}" class="nav-link">
                        <i class="nav-icon fa fa-question text-icon-green"></i>
                        <p>FAQ</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-content') }}" class="nav-link">
                        <i class="nav-icon fas fa-columns text-icon-yellow"></i>
                        <p>Content</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-enquiries') }}" class="nav-link">
                        <i class="nav-icon fa fa-exclamation-triangle text-icon-yellow"></i>
                        <p>Enquiries</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('waiting.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-spinner text-icon-yellow"></i>
                        <p>Waiting List</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-cog text-icon-yellow"></i>
                        <p>
                            Settings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin-under-development') }}" class="nav-link">
                                <i class="fa fa-calendar nav-icon"></i>
                                <p>Working Days</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin-under-development') }}" class="nav-link">
                                <i class="fa fa-clock nav-icon"></i>
                                <p>Available Time</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin-under-development') }}" class="nav-link">
                                <i class="fa fa-bed nav-icon"></i>
                                <p>Holidays</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('configuration') }}" class="nav-link">
                                <i class="fa fa-envelope nav-icon"></i>
                                <p>Configuration</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <!--sidebar ends-->
</aside>