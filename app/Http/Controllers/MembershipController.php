<?php

namespace App\Http\Controllers;

use App\Membership;
use App\MembershipMedia;
use Session;
use Illuminate\Http\Request;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title' => 'Membership',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'membership' => route('membership.index')
            ]
        ];

        $memberships = Membership::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.membership.index', compact('meta','memberships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title' => 'Add Membership',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'membership' => route('membership.index')
            ]
        ]; 

        return view('admin.membership.create', compact('meta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membership = Membership::create([
            'name' => $request->name,
            'price' => $request->price,
            'period' => $request->period,
            'status' => $request->status,
            'description' => $request->description
        ]);

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/membership/', $image);
                $media = MembershipMedia::create([
                    'membership_id' => $membership->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Membership Created.'); 
        return redirect()->route('membership.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function show(Membership $membership)
    {
        $meta = [
            'title' => 'View Membership',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'membership' => route('membership.show',$membership->id)
            ]
        ];

        return view('admin.membership.show', compact('meta','membership'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function edit(Membership $membership)
    {
        $meta = [
            'title' => 'Edit Membership',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'membership' => route('membership.edit',$membership->id)
            ]
        ];

        return view('admin.membership.edit', compact('meta','membership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membership $membership)
    {
        $membership->name = $request->name;
        $membership->price = $request->price;
        $membership->period = $request->period;
        $membership->status = $request->status;
        $membership->description = $request->description;
        $membership->save();

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/membership/', $image);
                $media = MembershipMedia::create([
                    'membership_id' => $membership->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Membership Updated.'); 
        return redirect()->route('membership.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function destroy(Membership $membership)
    {
        //
    }

    public function status($id)
    {
        $c = Membership::find($id);
        if($c->status==2){
            $c->status = 1;
        }else{
            $c->status = 2;
        }
        $c->save();

        return response()->json([
            "status"=>true
        ]);
    }

    public function delete($id)
    {
        $c = Membership::find($id);

        foreach($c->media as $m){
            try{
                unlink($m->media);
            } catch(\Exception $e) {
                // nothing to do
            }
            MembershipMedia::find( $m->id )->delete();
        }

        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function deletMedia($id)
    {
        $m = MembershipMedia::find($id);

        try{
            unlink($m->media);
        } catch(\Exception $e) {
            // nothing to do
        }
        
        $m->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
