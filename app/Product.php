<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'sku', 'name', 'description', 'selling_price', 'purchasing_price', 'slug', 'published'];

    /**
     * @return the records on the besis of relationship
     */
    public function productImage()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @return  the category details
     */
    public function categoryDetail()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
