<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $table = 'memberships';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'price', 'period', 'description'];

    public function media()
    {
        return $this->hasMany(MembershipMedia::class,'membership_id','id');
    }
}
