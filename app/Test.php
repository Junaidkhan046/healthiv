<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'tests';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'short_description', 'description'];

    public function media()
    {
        return $this->hasMany(TestMedia::class,'test_id','id');
    }
}
