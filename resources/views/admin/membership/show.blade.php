@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('membership.index') }}" class="btn btn-danger">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Edit only if you are sure that what it is going to change.</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Period</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        <tr id="record-{{ $membership->id }}">
                                            <td>#{{ $membership->id }}</td>
                                            <td>{{ $membership->name }}</td>
                                            <td>{{ $membership->price }}</td>
                                            <td>{{ $membership->period }}</td>
                                            <td class="text-center">
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input customSwitches" id="customSwitches{{$membership->id}}" data-url="{{url('admin/membership_status/'.$membership->id)}}" @if($membership->status==1) checked @endif>
                                                    <label class="custom-control-label" for="customSwitches{{$membership->id}}"></label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('membership.edit',$membership->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="7">Description</th>
                                        </tr>
                                        <tr>
                                        <td colspan="7">{!! $membership->description !!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row m-4" id="media-preview">
                                    @foreach($membership->media as $media)
                                    <div class="col-sm-2" id="stored-image-{{$media->id}}" class="text-center">
                                        <img src="{{url('')}}/{{$media->media}}" class="image-preview">
                                        <a href="javascript:void(0)" onclick="deleteImage({{$media->id}})" class="image-preview-remove">
                                            <i class="fa fa-trash text-danger image-remove-btn"></i>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
<script>
    $('.customSwitches').click(function(){
        var url = $(this).data('url');
        fetch(url, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => response.json()).then( res => {
            if(res.status){
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    });

    function removeImage(id){
        $("#selected-image-"+id).remove();
        selectedImage--;
    }

    function deleteImage(id){
        swal({
            title: "Are you sure?",
            text: "You want to delete this image?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/delete-membership-media') }}/"+id, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('stored-image-'+id).remove();
                        
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>
@stop

@endsection