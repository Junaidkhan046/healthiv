@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">

                <!-- Product Hero -->
                <div class="jumbotron book-your-first-month mb-0 ">
                    <div class="container mt-5 pt-4 mb-3">
                        <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                            <figure class="woocommerce-product-gallery__wrapper">
                                <div data-thumb="https://gurujigroceries.com/frontend/images/misc/payments.png" data-thumb-alt="" class="woocommerce-product-gallery__image">
                                    <a href="https://gurujigroceries.com/frontend/images/misc/payments.png">
                                        <img style="max-width: 250px;" src="https://gurujigroceries.com/frontend/images/misc/payments.png" class="wp-post-image" alt="">
                                    </a>
                                </div>	
                            </figure>
                        </div>

                        <div style="clear:both;">
                            <h1 class="product_title entry-title mb-4 pb-2 pt-2">Checkout</h1>
                            
                            <hr class="my-4">

                            <div class="pt-3">

                                <p>All your health IV questions, answered</p>

                            </div>
                        </div>

                    </div>
                </div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">


    	<section class="intra-product-block-title">
	    	<div class="container my-5" style="">

	    		<div class="justify-content-center">
                    <form method="POST" action="{{url('checkout')}}">
                    @csrf
                        <input type="hidden" name="product_id" value="{{@$product->id}}">
                        <input type="hidden" name="location" value="{{$_REQUEST['location']}}">
                        <input type="hidden" name="addons_id" value="{{@$addons->id}}">
                        <input type="hidden" name="tests_id" value="{{@$tests->id}}">
                        <input type="hidden" name="schedule" value="{{$_REQUEST['schedule']}}">
                        <input type="hidden" name="address" value="{{$address}}">
                        <!-- <input type="hidden" name="nurse_id" id="nurse_id" value=""> -->

                        <div class="row pt-4">
                            <div class="col-md-9">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>{{$product->name}}</th>
                                        <th><span class="woocommerce-Price-currencySymbol">$</span>{{$product->selling_price}}</th>
                                    </tr>
                                    <tr>
                                        <th>Location</th>
                                        <td>{{ $_REQUEST['location'] }}</td>
                                    </tr>
                                    @if($addons)
                                    <tr>
                                        <th>Add Ons</th>
                                        <td>{{ @$addons->name }}</td>
                                    </tr>
                                    @endif
                                    @if($tests)
                                    <tr>
                                        <th>Tests</th>
                                        <td>{{ @$tests->name }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th>Schedule</th>
                                        <td>{{ date('d-m-Y',strtotime($_REQUEST['schedule'])) }}</td>
                                    </tr>
                                </table>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="name"> Name</label>
                                        <input type="text" class="form-control" name="customer_name" id="" placeholder="Name" value="@auth {{ Auth::user()->name }} @else {{ old('customer_name') }} @endauth" required>
                                    </div>
                                    <div class="col-6">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" name="customer_phone" id="" placeholder="Phone No" value="@auth {{ Auth::user()->phone }} @else {{ old('customer_phone') }} @endauth" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="12" minlength="10" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="email"> Email</label>
                                    <input type="email" class="form-control" name="customer_email" id="" placeholder="Email" value="@auth {{ Auth::user()->email }} @else {{ old('customer_email') }} @endauth" required>
                                </div>
                                @auth
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"> 
                                @else
                                <div class="form-group">
                                    <label for="password"> Password</label>
                                    <input type="password" class="form-control" name="password" id="" placeholder="Password" required>
                                    @if(Session::has('password'))
                                        <p class="text-danger"><b>{{ Session::get('password') }}</b><p>
                                    @endif
                                </div>
                                @endauth
                            </div>
                            <div class="col-md-3">
                                <p class="mb-2">Total price : <span class="close" style="font-size: 16px;">${{$product->selling_price}}</span> </p>
                                <hr/>
                                <p>Total : <span class="close" style="font-size: 16px;"><b>${{$product->selling_price}}</b></span></p>
                                    <hr/>
                                <p class="text-center">
                                    <img src="https://gurujigroceries.com/frontend/images/misc/payments.png" height="26">
                                </p>
                                <div>
                                    <button type="submit" class="btn btn-success btn-block mt-5">
                                        <b>Checkout</b>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</main>
{{-- 
<!-- @section('pagescript')
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&libraries=places&callback=initMap" type="text/javascript"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var nurses = JSON.parse('{!! json_encode($nurses) !!}');
            var address = JSON.parse('{!! $address !!}');
            var lat = parseFloat(address['latitude']);
            var lng = parseFloat(address['longitute']);
            var destancevalue = '';
            var nurse_id = '';
            //console.log(nurses);
            nurses.forEach(function(item, index) {
                var lat2 = parseFloat(item.latitude);
                var lng2 = parseFloat(item.longitude);
                //console.log(lat2+" = lat2, lng2 = "+lng2);
                var distance = calcCrow(lat2,lng2,lat,lng).toFixed(2);
                if(destancevalue == ''){
                    destancevalue = distance;
                }
                //console.log("distance = "+distance+", destancevalue = "+destancevalue);
                if(parseFloat(destancevalue) >= parseFloat(distance))
                {
                    destancevalue = distance;
                    nurse_id = item.id;
                    //console.log(nurse_id+" nurse_id");
                }
            });

            $('#nurse_id').val(nurse_id);
            
            //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
            function calcCrow(lat1, lon1, lat2, lon2) 
            {
                var R = 6371; // km
                var dLat = toRad(lat2-lat1);
                var dLon = toRad(lon2-lon1);
                var lat1 = toRad(lat1);
                var lat2 = toRad(lat2);
            
                var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                var d = R * c;
                return d;
            }
        
            // Converts numeric degrees to radians
            function toRad(Value) 
            {
                return Value * Math.PI / 180;
            }
        });
    </script>
@stop --> --}}
@endsection