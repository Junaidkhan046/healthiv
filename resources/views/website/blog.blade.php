@extends('layouts.website')

@section('content')

<div class="jumbotron mb-3">

    <div class="container mt-5 pt-4 mb-3">

        <header class="entry-header">

            <h1 class="entry-title text-white mt-2 mb-4 pb-2">Blog</h1>

        </header>

        <hr class="my-4">

    </div>

</div>

<div class="container pt-2">

    <div class="row intra-block-container pb-3 mt-5 pt-2">

        @if(count($posts) > 0)

            @foreach($posts as $post)

            <div class="col-12 col-md-6 mb-4 pb-3">

                <article id="post-417" class="h-100 post-417 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">

                    <header class="entry-header h-100">

                        <div class="py-4 px-4 d-flex flex-column h-100 intra-article-block">

                            <div class="d-flex justify-content-center align-items-center">

                                <div class="h-100">

                                    <a class="post-thumbnail" href="{{ route('post', $post->slug) }}" aria-hidden="true" tabindex="-1">
                                    @if(!empty($post->media()->first()))    
                                    <img width="1920" height="1200" src="{{asset($post->media()->first()->media)}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="The many health benefits of IV therapy"/>
                                    @else
                                    <img width="1920" height="1200" src="{{asset('assets/images/liquid_wuxga.jpg')}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="The many health benefits of IV therapy"/>
                                    @endif
                                    </a>

                                </div>

                            </div>

                            <h2 class="entry-title mt-4 mb-4">

                                <a href="{{ route('post', $post->slug) }}" rel="bookmark">{{ $post->title }}</a>

                            </h2>

                            <div class="mt-auto">

                                <p>{{ $post->short_description }}</p>

                            </div>

                            <a href="{{ route('post', $post->slug) }}" class="intra-button w-100 mt-1">Read</a>

                        </div>

                    </header>

                </article>

            </div>

            @endforeach

        @endif

    </div>

</div>

@include('website.partials.join_wait_list')

@include('website.partials.membership')

@endsection