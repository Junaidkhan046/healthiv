@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    @if($order->assigned_nurse_id!='')
                        <a href="{{url('order/tracking/'.$order->id)}}" class="btn btn-warning text-decoration-none" target="_blank">Track Order</a>
                    @endif
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                            <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone No</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="record-{{ $order->id }}">
                                            <td>#{{ $order->id }}</td>
                                            <td>{{ $order->customer_name }}</td>
                                            <td>{{ $order->customer_email }}</td>
                                            <td>{{ $order->customer_phone }}</td>
                                            <td class="text-center">
                                                <b>{{ $order->tranking_status }}</b>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                <div class="mt-5">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>TYPE</th>
                                            <th>SKU</th>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($order->orderitems))
                                            @foreach($order->orderitems as $item)
                                                @if($item->type=='product')
                                                <tr>
                                                    <td>{{$item->type}}</td>
                                                    <td>{{@$item->product->sku}}</td>
                                                    <td>{{@$item->product->name}}</td>
                                                    <td>{{$item->price}}</td>
                                                    <td>{{$item->total_amount}}</td>
                                                </tr>
                                                @endif

                                                @if($item->type=='addons')
                                                <tr>
                                                    <td>{{$item->type}}</td>
                                                    <td></td>
                                                    <td>{{@$item->addons->name}}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @endif

                                                @if($item->type=='tests')
                                                <tr>
                                                    <td>{{$item->type}}</td>
                                                    <td></td>
                                                    <td>{{@$item->tests->name}}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                </div>
                                @if($order->address)
                                <div id="address">
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Address</th>
                                            <th>Zipcode</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <tr id="record-{{ $order->id }}">
                                            <td>#{{ @$order->address->id }}</td>
                                            <td>{{ @$order->address->address_line }}</td>
                                            <td>{{ @$order->address->zipcode }}</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                </div>
                                @endif
                                @if($order->payment)
                                <div>
                                    <h4 class="pl-3">Payment</h4>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Amount</th>
                                                <th>Transaction Id</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr id="record-{{ $order->id }}">
                                                <td>#{{ @$order->payment->id }}</td>
                                                <td>{{ @$order->payment->amount }}</td>
                                                <td>{{ @$order->payment->transaction_id }}</td>
                                                <td>
                                                    @if(@$order->payment->status==1)
                                                    SUCCESS
                                                    @else
                                                    FAILED
                                                    @endif
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

@stop

@endsection