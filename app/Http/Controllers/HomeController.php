<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use App\UserMedia;
use App\Order;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->hasRole('super-admin')){
            return redirect()->route('admin-dashboard');
        } elseif($user->hasRole('admin')) {
            dd('admin');
        } elseif($user->hasRole('nurse')) {
            if($user->document->status){
                return redirect('nurse/home');
            }else{
                Auth::guard()->logout();
                Session::flash('nurseRegister', 'You have succesfully registered. Please wait sometime first complete your document verification process'); 
                return redirect()->route('landing');
            }
        }else{
            $meta = [
                'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
            ];
    
            return view('home', compact('meta'));
        }
    }

    public function myprofile()
    {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        return  view('website.profile',compact('meta'));
    }

    public function trackorder(Request $request)
    {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        $order = Order::where('id',$request->order_id)->where('user_id',$request->user()->id)->first();
        if(empty($order)){
            Session::flash('error', 'Order ID Not Found.'); 
            return back();
        }
        return  view('website.track_map',compact('meta','order'));
    }

    public function updatePicture(Request $request)
    {
        
        if($request->file('image')){
            $file = $request->file('image');
            @$name = $file->getClientOriginalName();
            $image_name = uniqid().$name;
            $image_path = public_path('uploads/user_profile/');
            $path_image = 'uploads/user_profile/'.$image_name;
            $request->image->move($image_path, $path_image);
            
            if($request->picture_id){
                $m = UserMedia::find( $request->picture_id );
                try{
                    unlink($m->media);
                } catch(\Exception $e) {
    
                }
                $m->delete();
            }

           // $file = $this->saveBase64Image('uploads/user_profile/', $image_name);

            $media = UserMedia::create([
                'user_id' => \Auth::user()->id,
                'media' => $path_image
            ]);
            
        }

        $user = User::find($request->user()->id);
        $user->name      = $request->name;
        $user->phone     = $request->phone;
        $user->address   = $request->address;
        $user->latitude  = $request->latitude;
        $user->longitude = $request->longitude;
        $user->save();
        
        Session::flash('success', 'Profile picture changed.'); 
        return back();
    }
}
