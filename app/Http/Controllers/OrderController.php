<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Order;
use App\OrderAddress;
use App\OrderItem;
use App\OrderPayment;
use App\User;
class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $meta = [
            'title' => 'Order',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Order' => route('order')
            ]
        ];
        $nurses = User::role('nurse')->orderBy('id', 'desc')->get();
        $orders = Order::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.order.index', compact('meta','orders','nurses'));
    }

    public function show($id)
    {
        $meta = [
            'title' => 'Order',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Order' => route('order')
            ]
        ];

        $order = Order::find($id);
        return view('admin.order.show', compact('meta','order'));
    }
    
    public function assign(Request $request, $id)
    {
        $order = Order::find($id);
        $order->assigned_nurse_id = $request->nurse_id;
        $order->save();
        Session::flash('success', 'Nurse Assigned.');
        return back();
    }
}
