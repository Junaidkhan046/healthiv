<section class="intra-waitlist mb-4 pb-3 mt-n4">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4 pb-3">
                <div id="mc_embed_signup" class="intra-waitlist-form py-4 px-4">
                    <h2 class="mb-4">Join our waitlist</h2>
                    <p class="mb-3 pb-2">Don’t see your region on our list? We’ll let you know when our service area expands.</p>
                    <div>
                        <section class="yikes-mailchimp-container yikes-mailchimp-container-1 ">
                            <form class="yikes-easy-mc-form yikes-easy-mc-form-1" method="POST" action="{{route('store-waitlist')}}">
                                @csrf
                                <label for="yikes-easy-mc-form-1-EMAIL"
                                    class="EMAIL-label yikes-mailchimp-field-required ">
                                    <!-- dictate label visibility -->
                                    <span class="EMAIL-label">Email Address </span>
                                    <!-- Description Above -->
                                    <input name="email" placeholder="" class="yikes-easy-mc-email "
                                        required="required" type="email" value="">
                                    <!-- Description Below -->
                                </label>
                                <label for="yikes-easy-mc-form-1-SERV_ADD"
                                    class="SERV_ADD-label yikes-mailchimp-field-required ">
                                    <!-- dictate label visibility -->
                                    <span class="SERV_ADD-label">Address</span>
                                    <!-- Description Above -->
                                    <input name="address" placeholder=""
                                        class="yikes-easy-mc-text pac-target-input" required="required"
                                        type="text" value="" autocomplete="off">
                                    <!-- Description Below -->
                                </label>
                                <!-- Submit Button -->
                                <button type="submit" class="yikes-easy-mc-submit-button yikes-easy-mc-submit-button-1 btn btn-primary button intra-button-black w-100 mt-1">
                                    <span class="yikes-mailchimp-submit-button-span-text">Submit</span>
                                </button>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
