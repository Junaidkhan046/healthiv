<?php

namespace App\Http\Controllers;

use App\Test;
use App\TestMedia;
use Session;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title' => 'Tests',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Tests' => route('test.index')
            ]
        ];

        $tests = Test::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.tests.index', compact('meta','tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title' => 'Add Tests',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Tests' => route('test.index')
            ]
        ]; 

        return view('admin.tests.create', compact('meta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = Test::create([
            'name' => $request->name,
            'slug' => $this->getSlug($request->name),
            'short_description' => $request->short_description,
            'description' => $request->description
        ]);

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/tests/', $image);
                $media = TestMedia::create([
                    'test_id' => $test->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Test Created.'); 
        return redirect()->route('test.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        $meta = [
            'title' => 'View Test',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Tests' => route('test.show',$test->id)
            ]
        ];

        return view('admin.tests.show', compact('meta','test'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        $meta = [
            'title' => 'Edit Test',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Tests' => route('test.edit',$test->id)
            ]
        ];

        return view('admin.tests.edit', compact('meta','test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        $test->name = $request->name;
        $test->short_description = $request->short_description;
        $test->slug = $this->getSlug($request->name);
        $test->description = $request->description;
        $test->save();

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/tests/', $image);
                $media = TestMedia::create([
                    'test_id' => $test->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Test Updated.'); 
        return redirect()->route('test.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test  $test)
    {
//
    }
    public function delete($id)
    {
        $c = Test::find($id);

        foreach($c->media as $m){
            try{
                unlink($m->media);
            } catch(\Exception $e) {
                // nothing to do
            }
            TestMedia::find( $m->id )->delete();
        }

        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function deletMedia($id)
    {
        $m = TestMedia::find($id);

        try{
            unlink($m->media);
        } catch(\Exception $e) {
            // nothing to do
        }
        
        $m->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
