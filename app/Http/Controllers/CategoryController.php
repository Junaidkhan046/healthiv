<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title' => 'Categories',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Categories' => route('categories.index')
            ]
        ];

        $categories = Category::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.categories.index', compact('meta','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $meta = [
            'title' => 'Add Category',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add Category' => route('categories.create')
            ]
        ];
        $categories = Category::all();
        return view('admin.categories.create', compact('meta', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create([
            'name' => $request->name,
            'parent_id' => isset($request->parent_id) ? $request->parent_id : 0,
            'description' => $request->description
        ]);

        $images = [];
        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/categories/', $image);                
                    array_push($images, $file);
            }
        }
        $category->image = json_encode($images);
        $category->save();

        Session::flash('success', 'Category Created.'); 
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = [
            'title' => 'Edit Category',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit Category' => route('categories.edit',$id)
            ]
        ];

        $categories = Category::find($id);
        $cate = Category::all();
        return view('admin.categories.edit', compact('meta','categories', 'cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($request->id);

        $category->name = $request->name;
        $category->parent_id = (isset($request->parent_id) ? $request->parent_id : 0);
        $category->description = $request->description;

        $images = json_decode($category->image);
        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/categories/', $image);                
                    array_push($images, $file);
            }
        }
        $category->image = json_encode($images);
        $category->save();

        Session::flash('success', 'Category Updated.'); 
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Category::find($id);
        if(!empty($c->image)){
        foreach(json_decode($c->image) as $m){
            try{
                unlink($m);
            } catch(\Exception $e) {
                // nothing to do
            }
        }
    }
        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }


    /**
     * This function is used to delete category Media.
     *
     * @param [int] $id
     * @param [int] $i
     * @return void
     */
    public function deletMedia($id, $i)
    {
        $m = Category::find($id);

        try{
            $x = json_decode($m->image);
            if(!empty($x))
            {
                unlink($x[$i]);
                unset($x[$i]);
            }
            $m->image = json_encode($x);
            $m->save();
        } catch(\Exception $e) {
            // nothing to do
        }

        return response()->json([
            "status"=>true
        ]);
    }
}
