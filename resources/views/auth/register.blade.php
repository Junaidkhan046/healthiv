@extends('layouts.app')

@section('content')

<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<style>
    * {
    box-sizing: border-box;
    }

    body {
    background-color: #f1f1f1;
    }

    #regForm {
    background-color: #ffffff;
    margin: 100px auto;
    font-family: Raleway;
    padding: 40px;
    width: 70%;
    min-width: 300px;
    }

    h1 {
    text-align: center;  
    }

    .label_type
    {
      margin: 0px;
      max-width: 100%;
      font-weight: unset;
      font-size: 24px;
      line-height: 32px;
    }
    input{
      display: block;
      width: 100%;
      font-family: inherit;
      color: rgb(81, 177, 231);
      padding: 0px 0px 8px;
      border: none;
      outline: none;
      border-radius: 0px;
      appearance: none;
      background-image: none;
      background-position: initial;
      background-size: initial;
      background-repeat: initial;
      background-attachment: initial;
      background-origin: initial;
      background-clip: initial;
      transform: translateZ(0px);
      font-size: 25px;
      -webkit-font-smoothing: antialiased;
      line-height: unset;
      -webkit-text-fill-color: rgb(81, 177, 231);
      animation: 1ms ease 0s 1 normal none running native-autofill-in;
      transition: background-color 1e+08s ease 0s, box-shadow 0.1s ease-out 0s;
      box-shadow: rgb(81 177 231 / 30%) 0px 4px;
      background-color: transparent !important;

    }

    /* Mark input boxes that gets an error on validation: */
    input.invalid {
    background-color: #fffff;
    }

    input:focus {
      border: none;
    }

    /* Hide all steps by default: */
    .tab {
       display: none;
    }

    button {
    background-color: #04AA6D;
    color: #ffffff;
    border: none;
    padding: 10px 50px;
    font-size: 17px;
    font-family: Raleway;
    cursor: pointer;
    }

    button:hover {
    opacity: 0.8;
    }

    #prevBtn {
    background-color: #bbbbbb;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #fffff;
    border: none;  
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
    }

    .step.active {
    opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
    background-color: #04AA6D;
    }
</style>
<section class="py-5 bg-white">
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-10 col-sm-12 pt-2">
                <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                      <a href="#" class="close" data-dismiss="alert"> X </a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                </div>
                <div class="py-4">
                    <h2 class="mb-5">Welcome! Let's start {{ __('Register') }}</h2>
                    <form method="POST" action="{{ route('register') }}" autocomplete="off">
                        @csrf
                        <input type="hidden" name="role" value="user">
                        <input type="hidden" name="latitude" id="latitude">
                        <input type="hidden" name="longitude" id="longitude">
                        <!-- One "tab" for each step in the form: -->
                        <div class="tab"><span class="label_type"><i class="fa fa-long-arrow-right"></i> What is your full name ? *</span>
                            <p class="pt-5 pb-4"><input type="text" id="name" class="input" name="name"></p>
                        </div>
                        <div class="tab"><span class="label_type"><i class="fa fa-long-arrow-right"></i> What is your work email address ? *</span>
                            <p class="pt-5 pb-4"><input type="text" id="email" class="input" name="email"></p>
                        </div>
                        <div class="tab"><span class="label_type"><i class="fa fa-long-arrow-right"></i> What is your Contact No ? </span>
                            <p class="pt-5 pb-4"><input type="text" id="phone" class="input" name="phone"></p>
                        </div>
                        <div class="tab"><span class="label_type"><i class="fa fa-long-arrow-right"></i> Where you live ? *</span>
                            <p class="pt-5 pb-4"><input id="autocompleteaddress" class="input" type="text" name="address" ></p>
                            
                            <div id="ShowMap">
                                <div id="map" style="width:100%;height:300px;margin:1em 0;"></div>
                            </div>
                        </div>
                        <div class="tab"><span class="label_type"><i class="fa fa-long-arrow-right"></i> Password *</span>
                            <p class="pt-5 pb-4"><input type="password" id="password" class="input" name="password"></p>
                            <span class="label_type"><i class="fa fa-long-arrow-right"></i> Password Confirmation *</span>
                            <p class="pt-5 pb-4"><input type="password" id="input_type" class="input" name="password_confirmation"></p>
                        </div>
                        <div class="tab">
                            <div id="setdata"></div>
                        </div>
                        <div style="overflow:auto;">
                            <div id="btnbacknext" style="float:right;">
                            <button type="button" class="formbuttom" id="prevBtn" onclick="nextPrev(-1)">Back</button>
                            <button type="button" class="formbuttom" id="nextBtn" onclick="nextPrev(1)" style="background:rgb(81, 177, 231);">Next</button>
                            </div>
                        </div>
                        <!-- Circles which indicates the steps of the form: -->
                        <div style="text-align:center;margin-top:40px;">
                            <span class="step d-none"></span>
                            <span class="step d-none"></span>
                            <span class="step d-none"></span>
                            <span class="step d-none"></span>
                            <span class="step d-none"></span>
                            <span class="step d-none"></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&libraries=places&callback=initMap" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        document.getElementById("ShowMap").style.display = "none";
        var input = document.getElementById('autocompleteaddress');
        // create new autocomplete object from Maps API using our input
        var autocomplete = new google.maps.places.Autocomplete(input);

        // when new places are selected, process the results
        autocomplete.addListener("place_changed", function() {
            var place = autocomplete.getPlace();
            console.log(place);
            var lat = place.geometry['location'].lat();
            var lng = place.geometry['location'].lng();
            $('#latitude').val(lat);
            $('#longitude').val(lng);

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: lat,lng:lng},
                zoom: 15,
            });
            addMaker({lat:lat,lng:lng});
            //add maker function
            function addMaker(coords)
            {
                var marker = new google.maps.Marker({
                    map: map,
                    position: coords,
                    title:"your Location"
                });
            }
            document.getElementById("ShowMap").style.display = "block";
        });
    });
</script>


<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("btnbacknext").style.display = "none";
    var htmldata = '<table class="table mb-4" style="width:100%;margin:0 auto;">'
                      +'<tr><td style="width:30%;">Name </td><td>: '+document.getElementById("name").value+'</td></tr>'
                      +'<tr><td style="width:30%;">Email </td><td>: '+document.getElementById("email").value+'</td></tr>'
                      +'<tr><td style="width:30%;">Address </td><td>: '+document.getElementById("autocompleteaddress").value+'</td></tr>'
                      +'<tr><td style="width:30%;">Phone </td><td>: '+document.getElementById("phone").value+'</td></tr>'
                      +'<tr><td style="width:30%;">Password </td><td>: '+document.getElementById("password").value+'</td></tr>'
                    +'</table><hr/>'
                    +'<div style="float:right;">'
                      +'<button type="submit" id="nextBtn" style="background:rgb(81, 177, 231);"> Submit </button>'
                    +'</div>';
    document.getElementById("setdata").innerHTML = htmldata;
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n);

}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    //document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

jQuery('body').on("keyup", function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    document.getElementById("nextBtn").click();
  }
});
</script>
@stop
@endsection
