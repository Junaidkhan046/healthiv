<?php

namespace App\Http\Controllers;

use App\EmailConfig;
use App\GoogleApi;
use App\PaymentGateway;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Session;

class ConfigController extends Controller
{
    public function index(Request $request)
    {
        $meta = [
            'title' => 'Configuration',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Configuration' => route('configuration')
            ]
        ];

        $email = EmailConfig::first();
        $payment = PaymentGateway::first();
        $googleapi = GoogleApi::first();
        $setting = Setting::first();

        return view('admin.config.configuration', compact('meta','email','payment','googleapi','setting'));
    }

    public function emailconfiguration(Request $request)
    {
        if($request->id!='')
        {
            $email = EmailConfig::find($request->id);
        }else{
            $email = new EmailConfig;
        }

        $email->driver = $request->driver; 
        $email->host = $request->host; 
        $email->port = $request->port; 
        $email->username = $request->username; 
        $email->password = $request->password; 
        $email->encryption = $request->encryption; 
        $email->from_address = $request->from_address; 
        $email->from_name = $request->from_name; 
        $email->save();
        $credentials = [
            'MAIL_DRIVER' => $request->driver,
            'MAIL_HOST' => $request->host,
            'MAIL_PORT' => $request->port,
            'MAIL_USERNAME' => $request->username,
            'MAIL_PASSWORD' => $request->password,
            'MAIL_ENCRYPTION' => $request->encryption,
            'MAIL_FROM_ADDRESS' => $request->from_address,
            'MAIL_FROM_NAME' => str_replace(' ', '', $request->from_name)
        ];
        envUpdate($credentials);
        // Artisan::call('config:cache');
        Artisan::call('config:clear');
        Session::flash('success', 'Email Configuration Saved.'); 
        return back();
        
    }

    public function paymentgateway(Request $request)
    {
        if($request->id!='')
        {
            $payment = PaymentGateway::find($request->id);
        }else{
            $payment = new PaymentGateway;
        }

        $payment->stripe_key = $request->stripe_key; 
        $payment->stripe_secret = $request->stripe_secret; 
        $payment->save();
        Session::flash('success', 'Stripe Payment Gateway Saved.'); 
        return back();
    }

    public function googleapi(Request $request)
    {
        if($request->id!='')
        {
            $googleapi = GoogleApi::find($request->id);
        }else{
            $googleapi = new GoogleApi;
        }

        $googleapi->api_key = $request->api_key; 
        $googleapi->client_id = $request->client_id; 
        $googleapi->distance = $request->distance; 
        $googleapi->save();
        Session::flash('success', 'Google API Gateway Saved.'); 
        return back();
    }

    public function config_setting(Request $request)
    {
        if($request->id!='')
        {
            $setting = Setting::find($request->id);
        }else{
            $setting = new Setting;
        }

        $setting->start_time = $request->start_time; 
        $setting->end_time = $request->end_time; 
        $setting->save();
        Session::flash('success', 'Working Time Saved.'); 
        return back();
    }
}
