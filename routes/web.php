<?php

use Illuminate\Support\Facades\Route;

// website routes
Route::get('/', 'WebsiteController@landingPage')->name('landing');

//production
Route::get('/production/{slug}', 'WebsiteController@production')->name('production');
//member
Route::get('/becomeamenber', 'WebsiteController@becomeamenber')->name('becomeamenber');
//frontend faq
Route::get('/faq', 'WebsiteController@faq')->name('faq');
//checkout
Route::get('checkout', 'WebsiteController@checkout')->name('checkout');
Route::post('checkout', 'WebsiteController@checkoutStore')->name('checkout');
//payment
Route::get('stripe/{id}', 'WebsiteController@stripe');
Route::post('stripe/{id}', 'WebsiteController@stripePost')->name('stripe.post');
//order list
Route::get('orders','WebsiteController@orders')->name('orders');
Route::post('orders/{id}','WebsiteController@complete_orders')->name('complete_orders');
//tracking
Route::get('order/tracking/{id}','WebsiteController@tracking')->name('order-tracking');
//order details
Route::get('order/details/{id}','WebsiteController@details')->name('order-details');
//check nurse assign schedule
Route::get('checknurseassign','WebsiteController@checknurseassign')->name('checknurseassign');
Route::get('re_schedule/{id}','WebsiteController@re_schedule')->name('re_schedule');
Route::post('re_schedule/{id}','WebsiteController@re_schedule_update')->name('re_schedule');
Route::get('/blog', 'WebsiteController@blog')->name('blog');

Route::get('/post/{slug}', 'WebsiteController@post')->name('post');
Route::get('/event', 'WebsiteController@event')->name('event');
Route::get('/news/{slug}', 'WebsiteController@news')->name('news');
Route::get('/about', 'WebsiteController@about')->name('about');

Route::get('/contact', 'WebsiteController@contact')->name('contact');
Route::post('/contact', 'WebsiteController@createEnquiry')->name('create-enquiry');
Route::post('/waitlist', 'WebsiteController@storeWaitlist')->name('store-waitlist');

Route::get('/cart', 'WebsiteController@cart')->name('cart');
Route::get('/checkout', 'WebsiteController@checkout')->name('checkout');

Route::get('/dump', 'WebsiteController@dump')->name('dump');
Route::get('/web-home', 'WebsiteController@home')->name('web-home');
Route::get('/informed-consent', 'WebsiteController@informedConsent')->name('informed-consent');
Route::get('/my-account', 'WebsiteController@myAcccount')->name('my-account');
Route::get('/privacy', 'WebsiteController@privacy')->name('privacy');
Route::get('/terms', 'WebsiteController@terms')->name('terms');

Route::get('nurse/register', 'WebsiteController@nurseRegisteration')->name('nurse-register');

//track order
Route::get('trackorder','HomeController@trackorder')->name('trackorder');

Route::group(['prefix'=>'nurse', 'middleware' => 'role:nurse'], function(){
    Route::get('home', 'NurseController@index')->name('nurse');
    Route::get('orders', 'NurseController@getorders')->name('orders');
    Route::Post('orders/{id}', 'NurseController@order_status')->name('orders.{id}');
    Route::get('profile', 'NurseController@profile')->name('profile');
});
// auth routes
Auth::routes();

// post login routes
Route::get('/home', 'HomeController@index')->name('home');
//testing mail
Route::get('/sendmail', 'WebsiteController@sendmail')->name('sendmail');

//user profile
Route::get('profile','HomeController@myprofile')->name('profile');
//profile upload
Route::post('profile-picture', 'HomeController@updatePicture')->name('update-picture');
// admin routes
Route::group(['prefix'=>'admin', 'middleware' => 'role:super-admin'], function(){

    // admin dashboard
    Route::get('/dashboard', 'AdminController@dashboard')->name('admin-dashboard');
    Route::get('/profile', 'AdminController@profile')->name('admin-profile');
    Route::post('/update-profile', 'AdminController@update')->name('admin-update-profile');
    Route::post('/update-profile-picture', 'AdminController@updatePicture')->name('admin-update-picture');

    // content management
    Route::get('/content', 'ContentController@index')->name('admin-content');
    Route::get('/add-content', 'ContentController@add')->name('admin-add-content');
    Route::post('/create-content', 'ContentController@create')->name('admin-create-content');
    Route::get('/delete-content/{id}', 'ContentController@delete')->name('admin-delete-content');
    Route::get('/edit-content/{id}', 'ContentController@edit')->name('admin-edit-content');
    Route::post('/update-content', 'ContentController@update')->name('admin-update-content');
    Route::get('/delete-media/{id}', 'ContentController@deletMedia')->name('admin-delete-media');

    // test management
    Route::resource('test','TestController');
    Route::get('delete-test-media/{id}','TestController@deletMedia');
    Route::get('delete-test/{id}', 'TestController@delete')->name('admin-delete-test');

    // membership management
    Route::resource('membership','MembershipController');
    Route::get('delete-membership-media/{id}','MembershipController@deletMedia');
    Route::get('delete-membership/{id}', 'MembershipController@delete')->name('admin-delete-membership');
    Route::get('membership_status/{id}', 'MembershipController@status')->name('membership_status');
    
    //configuration
    Route::get('configuration','ConfigController@index')->name('configuration');
    Route::post('emailconfiguration','ConfigController@emailconfiguration')->name('emailconfiguration');
    Route::post('paymentgateway','ConfigController@paymentgateway')->name('paymentgateway');
    Route::post('googleapi','ConfigController@googleapi')->name('googleapi');
    Route::post('config_setting','ConfigController@config_setting')->name('config_setting');
    //Addons
    Route::resource('addon','AddonController');
    Route::get('delete-addon-media/{id}','AddonController@deletMedia');
    Route::get('delete-addon/{id}', 'AddonController@delete')->name('admin-delete-addon');

    //product purchase order
    Route::get('order','OrderController@index')->name('order');
    Route::get('order/show/{id}','OrderController@show')->name('order-show');
    Route::post('order/assign/{id}','OrderController@assign')->name('order-assign');
    
    // posts management
    Route::get('/posts', 'PostController@index')->name('admin-posts');
    Route::get('/add-post', 'PostController@add')->name('admin-add-post');
    Route::post('/create-post', 'PostController@create')->name('admin-create-post');
    Route::get('/edit-post/{id}', 'PostController@edit')->name('admin-edit-post');
    Route::post('/update-post', 'PostController@update')->name('admin-update-post');
    Route::get('/delete-post/{id}', 'PostController@delete')->name('admin-delete-post');
    Route::get('/delete-post-media/{id}', 'PostController@deletMedia')->name('admin-delete-post-media');

    // locations management
    Route::get('/locations', 'LocationController@index')->name('admin-locations');
    Route::get('/add-location', 'LocationController@add')->name('admin-add-location');
    Route::post('/create-location', 'LocationController@create')->name('admin-create-location');
    Route::get('/edit-location/{id}', 'LocationController@edit')->name('admin-edit-location');
    Route::post('/update-location', 'LocationController@update')->name('admin-update-location');
    Route::get('/delete-location/{id}', 'LocationController@delete')->name('admin-delete-location');
    Route::get('/delete-location-media/{id}', 'LocationController@deletMedia')->name('admin-delete-location-media');

    // news & events management
    Route::get('/events', 'EventController@index')->name('admin-events');
    Route::get('/add-event', 'EventController@add')->name('admin-add-event');
    Route::post('/create-event', 'EventController@create')->name('admin-create-event');
    Route::get('/edit-event/{id}', 'EventController@edit')->name('admin-edit-event');
    Route::post('/update-event', 'EventController@update')->name('admin-update-event');
    Route::get('/delete-event/{id}', 'EventController@delete')->name('admin-delete-event');
    Route::get('/delete-event-media/{id}', 'EventController@deletMedia')->name('admin-delete-event-media');

    // faqs management
    Route::get('/faq', 'FAQController@index')->name('admin-faq');
    Route::get('/add-faq', 'FAQController@add')->name('admin-add-faq');
    Route::post('/create-faq', 'FAQController@create')->name('admin-create-faq');
    Route::get('/edit-faq/{id}', 'FAQController@edit')->name('admin-edit-faq');
    Route::post('/update-faq', 'FAQController@update')->name('admin-update-faq');
    Route::get('/delete-faq/{id}', 'FAQController@delete')->name('admin-delete-faq');

    // enquiry management
    Route::get('/enquiries', 'EnquiryController@index')->name('admin-enquiries');
    Route::get('/delete-enquiry/{id}', 'EnquiryController@delete')->name('admin-delete-enquiry');
    Route::get('/change-status-enquiry/{id}/{status}', 'EnquiryController@changeStatus')->name('admin-change-status-enquiry');

    // waiting list
    Route::resource('waiting', 'WaitingController')->only(['index', 'destroy']);

    // product management
    Route::resource('products', 'ProductController');
    Route::post('products/change-status/{id}', 'ProductController@changeStatus')->name('status');
    Route::resource('products', 'ProductController');
    Route::post('products/delete-product-media/{id}', 'ProductController@deletMedia');

    // category management
    Route::resource('categories', 'CategoryController');
    Route::post('delete-categories-media/{id}/{i}', 'CategoryController@deletMedia');

    //customer management
    Route::get('user', 'UserManagementConroller@getCustomer')->name('customer');
    Route::get('/user/edit/{id}', 'UserManagementConroller@editCustomer')->name('customer.edit');
    Route::patch('/user/update', 'UserManagementConroller@updateCustomer')->name('customer.update');
    Route::get('manager', 'UserManagementConroller@getManager')->name('manager');
    Route::get('/manager/edit/{id}', 'UserManagementConroller@editManager')->name('manager.edit');
    Route::patch('/manager/update', 'UserManagementConroller@updateManager')->name('manager.update');
    Route::get('nurse', 'UserManagementConroller@getNurse')->name('nurse');
    Route::get('/nurse/edit/{id}', 'UserManagementConroller@editNurse')->name('nurse.edit');
    Route::patch('/nurse/update', 'UserManagementConroller@updateNurse')->name('nurse.update');
    Route::post('change-user-status/{id}', 'UserManagementConroller@changeStatus')->name('active');
    Route::post('change-user-document-status/{id}', 'UserManagementConroller@changeDocumentStatus')->name('doc');
    Route::get('add-manager', 'UserManagementConroller@createManager')->name('createManager');
    Route::post('create-manager', 'UserManagementConroller@storeManager')->name('storeManager');

    // admin under development
    Route::get('/under-development', function(){
        $meta = [
            'title' => 'Under Development',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard')
            ]
        ];
        return view('admin/under-development', compact('meta'));
    })->name('admin-under-development');
});