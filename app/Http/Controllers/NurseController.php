<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
class NurseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request){
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        return view('nurse.index',compact('meta'));
    }

    public function getorders(Request $request){
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];

        $orders = Order::where('assigned_nurse_id',$request->user()->id)->get();
        if(!empty($request->status))
        {
            $orders = $orders->where('tranking_status',$request->status);
        }
        return view('nurse.orderlist', compact('meta','orders'));
    }

    public function profile(){
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        return view('nurse.index',compact('meta'));
    }
}
