@extends('admin.layout.admin-layout')

@section('content')
<style>
    label{
        font-weight: 400;
        font-size:15px;
    }
</style>
<div class="card card-default card-outline">
    <div class="card-header">
        @include('admin.partials.breadcrumbs')
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" role="tablist" aria-orientation="vertical">
                    <a class="nav-link mb-3 border-bottom active" id="email-configuration" data-toggle="pill" href="#vert-tabs-email" role="tab" aria-controls="vert-tabs-email" aria-selected="false">Email Configuration</a>
                    <a class="nav-link mb-3 border-bottom" id="payment-gateway" data-toggle="pill" href="#vert-tabs-payment" role="tab" aria-controls="vert-tabs-payment" aria-selected="false">Stripe Payment Gateway</a>
                    <a class="nav-link mb-3 border-bottom" id="google-api" data-toggle="pill" href="#vert-tabs-googleapi" role="tab" aria-controls="vert-tabs-googleapi" aria-selected="false">Google API</a>
                    <a class="nav-link mb-3 border-bottom" id="workingtime" data-toggle="pill" href="#vert-tabs-workingtime" role="tab" aria-controls="vert-tabs-workingtime" aria-selected="false">Working Time</a>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content">
                    <div class="tab-pane text-left fade active show" id="vert-tabs-email" role="tabpanel" aria-labelledby="email-configuration">
                        <form action="{{ route('emailconfiguration') }}" method="post" id="update-form">
                            @csrf
                            <h4>Email Configuration<h4>
                            <input type="hidden" name="id" value="{{@$email->id}}">
                                <hr/>
                            <div class="row">
                                <div class="col-sm-6 mb-4">
                                    <label>Driver</label>
                                    <input type="text" class="form-control" name="driver" placeholder="Driver" value="{{@$email->driver}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>Host</label>
                                    <input type="text" class="form-control" name="host" placeholder="Host" value="{{@$email->host}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>Port</label>
                                    <input type="text" class="form-control" name="port" placeholder="Port" value="{{@$email->port}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>User Name</label>
                                    <input type="text" class="form-control" name="username" placeholder="User Name" value="{{@$email->username}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="password" placeholder="Password" value="{{@$email->password}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>Encryption</label>
                                    <input type="text" class="form-control" name="encryption" placeholder="Encryption" value="{{@$email->encryption}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>From Address</label>
                                    <input type="text" class="form-control" name="from_address" placeholder="From Address" value="{{@$email->from_address}}">
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>From Name</label>
                                    <input type="text" class="form-control" name="from_name" placeholder="From Name" value="{{@$email->from_name}}">
                                </div>
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-success px-5">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-payment" role="tabpanel" aria-labelledby="payment-gateway">
                        <form action="{{ route('paymentgateway') }}" method="post" id="update-image-form">
                            @csrf
                            <h4>Stripe Payment Gateway<h4>
                            <hr/>
                            <input type="hidden" name="id" value="{{@$payment->id}}">
                            <div class="row">
                                <div class="col-sm-6 mb-4">
                                    <label>Stripe Key</label>
                                    <input type="text" class="form-control" name="stripe_key" placeholder="Stripe Key" value="{{@$payment->stripe_key}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>Stripe Secret</label>
                                    <input type="text" class="form-control" name="stripe_secret" placeholder="Stripe Secret" value="{{@$payment->stripe_secret}}" required>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-success px-5">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-googleapi" role="tabpanel" aria-labelledby="google-api">
                        <form action="{{ route('googleapi') }}" method="post" id="update-image-form">
                            @csrf
                            <h4>Google API<h4>
                            <hr/>
                            <input type="hidden" name="id" value="{{@$googleapi->id}}">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>API Key</label>
                                    <input type="text" class="form-control" name="api_key" placeholder="API Key" value="{{@$googleapi->api_key}}" required>
                                </div>
                                <div class="col-sm-6">
                                    <label>Client Id</label>
                                    <input type="text" class="form-control" name="client_id" placeholder="Client Id" value="{{@$googleapi->client_id}}" required>
                                </div>
                                <div class="col-sm-6 mt-3">
                                    <label>Google Map Distance</label>
                                    <input type="number" class="form-control" name="distance" placeholder="Distance" value="{{@$googleapi->distance}}" required>
                                    <span style="font-size:14px;">distance between location to customer</span>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <br/>
                                    <button type="submit" class="btn btn-success px-5">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-workingtime" role="tabpanel" aria-labelledby="workingtime">
                        <form action="{{ route('config_setting') }}" method="post" id="update-image-form">
                            @csrf
                            <h4>Working Time<h4>
                            <hr/>
                            <input type="hidden" name="id" value="{{@$setting->id}}">
                            <div class="row">
                                <div class="col-sm-6 mb-4">
                                    <label>Start Time</label>
                                    <input type="time" class="form-control" name="start_time" placeholder="Start Time" value="{{@$setting->start_time}}" required>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <label>End Time</label>
                                    <input type="time" class="form-control" name="end_time" placeholder="End Time" value="{{@$setting->end_time}}" required>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-success px-5">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>

@section('pagescript')

@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<!--Image Crop Wizard-->
<div class="modal fade" id="crop-tool" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Adjust Your Image</h5>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12 text-center" style="padding-left:0px;">
                            <img id="image" src="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Okay, Save</button>
            </div>
        </div>
    </div>
</div>
<!--Image Crop Wizard-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<script>
    // Wait for the DOM to be ready
    $(function() {

        $(".description").summernote();

        $("#update-form").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: "Name cannot be left blank"
                },
                email: {
                    required: "Email cannot be left blank",
                    email: "Invalid email address"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

<script>
    var $modal = $('#crop-tool');
    var image = document.getElementById('image');
    var cropper;

    $("body").on("change", ".image", function(e) {
        var files = e.target.files;
        var done = function(url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function() {
        canvas = cropper.getCroppedCanvas({
            // width: 160,
            // height: 160,
        });
        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                
                $(".media-preview").attr("src",base64data);
                $("#new-picture").val(base64data);
                $modal.modal('hide');

                $("#update-image-form").submit();

            }
        });
    });

</script>
@stop

@endsection