<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiry;
use App\User;
use App\UserMedia;

use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $meta = [
            'title' => 'Dashboard',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Dashboard' => ''
            ]
        ];

        $enquiries = Enquiry::all()->count();
        return view('admin/dashboard', compact('meta','enquiries'));
    }

    public function profile()
    {
        $meta = [
            'title' => 'Profile',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Profile' => route('admin-profile')
            ]
        ];

        $user = User::find(\Auth::user()->id);
        return view('admin/profile', compact('meta','user'));
    }

    public function update(Request $request)
    {
        $u = User::find($request->id);

        $u->email = $request->email;
        $u->name = $request->name;
        $u->save();

        Session::flash('success', 'Profile Updated.'); 
        return redirect()->route('admin-profile');

    }

    public function updatePicture(Request $request)
    {
        if($request->media){

            if($request->picture_id){
                $m = UserMedia::find( $request->picture_id );
                try{
                    unlink($m->media);
                } catch(\Exception $e) {
    
                }
                $m->delete();
            }

            $file = $this->saveBase64Image('uploads/user_profile/', $request->media);

            $media = UserMedia::create([
                'user_id' => \Auth::user()->id,
                'media' => $file
            ]);
            
        }

        Session::flash('success', 'Profile picture changed.'); 
        return redirect()->route('admin-profile');
    }
}
