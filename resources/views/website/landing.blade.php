@extends('layouts.website')

@section('content')

<div class="jumbotron mb-5">
    <div class="container mt-5 pt-4 mb-3">
        @php
        $s = getSectionContent();
        @endphp
<<<<<<< HEAD
        <h1 class="text-white mt-2 mb-4 pb-2">{{ @$s->title }}</h1>
        <h2 class="text-white mb-4 pb-3">{{ @$s->subtitle }}</h2>
=======
        <h1 class="text-white mt-2 mb-4 pb-2">{{ !isset($s->title) ? '' : $s->title }}</h1>
        <h2 class="text-white mb-4 pb-3">{{ !isset($s->subtitle) ? '' : $s->subtitle }}</h2>
>>>>>>> develop
        <hr class="my-4">
        <p class="pt-3">
            <a href="#intra-anchor-book-session" class="intra-button intra-button-hero-1 mt-1 mr-5 mb-4 mb-sm-0">Book Session</a>
            <a href="{{url('becomeamenber')}}" class="intra-button intra-button-hero-2 mt-1 ml-sm-n2">Become a Member</a>
        </p>
    </div>
</div>
<div class="container">
    <div class="row mb-5">
        <div class="col-12">
            @php
            $s = getSectionContent();
            @endphp
<<<<<<< HEAD
            <h2>{{ @$s->subtitle }}</h2>
=======
            <h2>{{ !isset($s->subtitle) ? '' : $s->subtitle }}</h2>
>>>>>>> develop
        </div>
    </div>
    <div class="row justify-content-around">
        <div class="col-12 col-sm-3 pb-4 pb-sm-0">
            <img class="img-fluid intra-img-rounded d-block mx-auto" src="assets/images/phone_square.png">
            <p class="my-4 text-center">Easy online booking</p>
        </div>
        <div class="col-12 col-sm-3 pb-4 pb-sm-0">
            <img class="img-fluid intra-img-rounded d-block mx-auto" src="assets/images/chair_square.png">
            <p class="my-4 text-center">At home service</p>
        </div>
        <div class="col-12 col-sm-3 pb-4 pb-sm-0">
            <img class="img-fluid intra-img-rounded d-block mx-auto" src="assets/images/test_square.png">
            <p class="my-4 text-center">Add a Covid-19 test</p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-5 mb-4">
        <div class="col-12">
            <h2 class="mb-4">Treatment menu</h2>
            <p>We offer a wide range of therapies and booster supplements</p>
            <a name="intra-anchor-book-session"></a>
        </div>
    </div>
    <div class="row pb-3">
    @if($product->isNotEmpty())
        <div class="col-12 mb-4 pb-3">
            <div class="py-4 px-4 intra-catalog-block intra-product-all-inclusive mw-100">
                <div
                    class="d-flex justify-content-center align-items-center intra-bg-gradient-standard intra-img-rounded-pill">
                    <div class="h-100 py-3">
                        <img class="w-100 h-100" src="{{asset($product->first()->productImage->first()->path)}}">
                    </div>
                </div>
                <h3 class="mt-4 mb-4">All Inclusive</h3>
                <hr>
                <h2 class="mt-4 mb-4">${{$product->first()->selling_price}}</h2>
                <p class="mb-4">{!! $product->first()->description !!}</p>
                <a href="{{url('production',$product->first()->slug)}}" class="intra-button w-100 mt-1">View</a>
            </div>
        </div>
        @endif
        @php
        $color = ['hydration', 'myers-cocktail', 'energy-lift', 'beauty', 'recovery', 'immune-boost', 'weight-loss']
        @endphp
        @if($product->isNotEmpty())
        @foreach($product as $val)
        <div class="col-12 col-sm-6 col-md-4 mb-4 pb-3">
            <div class="py-4 px-4 d-flex flex-column h-100 intra-catalog-block intra-product-{{$color[$loop->index]}}">
                <div class="d-flex justify-content-center align-items-center intra-img-rounded-pill">
                    <div class="h-100 py-3">
                        <img class="w-100 h-100" src="{{asset($val->productImage->first()->path)}}">
                    </div>
                </div>
                <h3 class="mt-4 mb-4">{{$val->name}}</h3>
                <hr class="m-0 w-100">
                <h2 class="mt-4 mb-4">${{$val->selling_price}}</h2>
                <p class="mb-4">{!! $val->description !!}.</p>
                <a href="{{url('production',$val->slug)}}" class="intra-button w-100 mt-1 mt-auto">View</a>

            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
<div class="container">
    <div class="row mt-5 pt-1">
        <div class="col-12 mt-5 mb-5">
            <h2 class="mb-4">How does it work?</h2>
            <p>At Health IV everything we do is based on one core principle. That health is for everybody. It doesn’t matter who you are or where you are, we want to help you feel rejuvenated.</p>
        </div>
        <div class="col-12 col-sm-6 mb-5 pb-4">
            <div class="intra-info-block">
                <img class="img-fluid intra-img-rounded-pill mb-4" src="assets/images/phone.png">
                <p class="px-4">Book appointments easily on your phone.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 mb-5 pb-4">
            <div class="intra-info-block float-sm-right">
                <img class="img-fluid intra-img-rounded-pill mb-4" src="assets/images/bag.png">
                <p class="px-5">Choose from a wide selection of revitalizing treatments.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 mb-5 pb-4">
            <div class="intra-info-block">
                <img class="img-fluid intra-img-rounded-pill mb-4" src="assets/images/test.png">
                <p class="px-4">Include an at-home COVID test.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 mb-5 pb-4">
            <div class="intra-info-block float-sm-right">
                <img class="img-fluid intra-img-rounded-pill mb-4" src="assets/images/chair.png">
                <p class="px-4">Enjoy the benefits of IV therapy from the comfort of your home.</p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-5 mb-4">
        <div class="col-12">
            <h2 class="mb-4">Covid testing</h2>
            <p>We believe in making wellness mobile. Healthcare shouldn’t be stressful. It should be accessible. Schedule an appointment online and one of our healthcare professionals will visit you in the comfort of your own home.</p>
        </div>
    </div>
    <div class="row pb-3">

        <div class="col-12 col-sm-6 mb-4 pb-3">
            @if(!empty($tests))
                @foreach($tests as $test)
                    <div class="py-4 px-4 intra-catalog-block intra-product-covid-pcr ">
                        <img class="img-fluid intra-img-rounded-pill" src="{{@$test->media->first()->media}}">
                        <h3 class="mt-4 mb-4">{{@$test->name}}</h3>
                        <p class="mt-4 mb-4">{{@$test->short_description}}</p>
                        <p class="mb-4">{!! @$test->description !!} </p>
                        <a href="{{url('/')}}" class="intra-button w-100 mt-1">View</a>
                    </div>
                @endforeach
            @endif
        </div>

    </div>
</div>
<div class="container">
    <div class="row mt-5 pt-1">
        <div class="col-12 mt-5 mb-5">
            <h2 class="mb-4">Our locations</h2>
            <p>We believe in making wellness mobile. Healthcare shouldn’t be stressful. It should be accessible. Schedule an appointment online and one of our healthcare professionals will visit you in the comfort of your own home.</p>
        </div>
        @if($location->isNotEmpty())
        @foreach($location as $val)
        <div class="col-12 col-sm-6 mb-5 pb-4">
            <div class="intra-info-block">
                <img class="img-fluid intra-img-rounded mb-4" src="{{asset($val->media->first()->media)}}">
                <h3 class="mb-4">{{$val->title}}</h3>
                <p class="px-4">{!! $val->description !!}</p>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
@include('website.partials.join_wait_list')
@include('website.partials.membership')
@endsection