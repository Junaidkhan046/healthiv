<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleApi extends Model
{
    protected $table = 'google_apis';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['api_key', 'client_id', 'distance'];
}
