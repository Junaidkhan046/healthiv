@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('addon.index') }}" class="btn btn-danger">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Edit only if you are sure that what it is going to change.</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        <tr id="record-{{ $addon->id }}">
                                            <td>#{{ $addon->id }}</td>
                                            <td>{{ $addon->name }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('addon.edit',$addon->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <th colspan="7">Description</th>
                                        </tr>
                                        <tr>
                                        <td colspan="7">{!! $addon->description !!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row m-4" id="media-preview">
                                    <div class="col-sm-2" id="stored-image-{{$addon->id}}" class="text-center">
                                        <img src="{{url('')}}/{{$addon->image}}" class="image-preview">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
<script>
    $('.customSwitches').click(function(){
        var url = $(this).data('url');
        fetch(url, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => response.json()).then( res => {
            if(res.status){
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    });
</script>
@stop

@endsection