<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model {

    protected $table = 'faqs';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'slug', 'answer'];

}