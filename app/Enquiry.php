<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model {

    protected $table = 'enquiries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'zip_code', 'message', 'status'];

}