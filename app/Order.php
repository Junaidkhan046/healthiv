<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_name', 'customer_email', 'customer_phone', 'assigned_nurse_id', 'tranking_status', 'delivery_working_day_id', 'delivery_time_slot_id'];

    public function orderitems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function address()
    {
        return $this->belongsTo(OrderAddress::class,'id','order_id');
    }

    public function payment()
    {
        return $this->belongsTo(OrderPayment::class,'id','order_id');
    }

    public function nurse()
    {
        return $this->belongsTo(User::class,'assigned_nurse_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
