<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EventMedia;

class Event extends Model {

    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'slug', 'title', 'subtitle', 'description'];

    public function media()
    {
        return $this->hasMany(EventMedia::class,'event_id','id');
    }

}