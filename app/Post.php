<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PostMedia;

class Post extends Model {

    protected $table = 'posts';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'short_description', 'description'];

    public function media()
    {
        return $this->hasMany(PostMedia::class,'post_id','id');
    }

}