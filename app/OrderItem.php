<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sku', 'qty', 'product_name', 'price', 'total_amount'];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function addons()
    {
        return $this->belongsTo(AddOn::class,'product_id');
    }

    public function tests()
    {
        return $this->belongsTo(Test::class,'product_id');
    }
}
