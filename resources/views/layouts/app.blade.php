<!doctype html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="msapplication-TileImage" content="assets/images/cropped-favicon-270x270.jpg" />

    <title>HealthIV</title>

    <link rel='stylesheet' href='{{asset("assets/css/style.min.css")}}' media='all' />
    <link rel='stylesheet' href='{{asset("assets/css/style.css")}}' media='all' />
    <link rel="icon" href="{{asset('assets/images/cropped-favicon-32x32.jpg')}}" sizes="32x32" />
    <link rel="icon" href="{{asset('assets/images/cropped-favicon-192x192.jpg')}}" sizes="192x192" />
    <link rel="apple-touch-icon" href="{{asset('assets/images/cropped-favicon-180x180.jpg')}}" />

    <script src='{{asset("assets/js/jquery.min.js")}}'></script>
    <script src='{{asset("assets/js/jquery-migrate.min.js")}}'></script>
    <script src='{{asset("assets/js/main.js")}}' id='main-js-js'></script>
</head>

<body class="single single-post single-format-standard theme-intra">
    <div id="page" class="site">
        @include('website.partials.nav')
        <main id="primary" class="site-main file-single intra-blog-post">
        @yield('content')
        </main>
    </div>
    <script src='{{asset("assets/js/navigation.js")}}'></script>
    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDJcFlSQt_eVhAfXBAKYatsG2H55m8wu_M&#038;libraries=places&#038;ver=5.8'></script>
    <script src='{{asset("assets/js/bootstrap.min.js")}}'></script>
    @include('website.partials.footer')
</body>

</html>