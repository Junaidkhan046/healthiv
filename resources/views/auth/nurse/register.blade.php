@extends('layouts.app')

@section('content')
<section class="mb-3 pb-3 mt-5 pt-4">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4 pt-2">
                <div class="py-4">
                    <h2 class="mb-4">{{ __('Register') }}</h2>
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" autocomplete="off">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <input type="hidden" name="role" value="nurse">

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Documents') }}</label>
                            @if (count($errors->all()) > 0)
                            @php $er = $errors->all(); @endphp
                            @endif
                            <div class="col-md-6">
                                <input id="document" type="file" class="form-control @isset($er[0]) is-invalid @endisset" name="document[]" required multiple>
                                
                                @isset($er[0])
                                    <span class="invalid-feedback" role="alert">
                                        <strong>
                                            {{ $er[0] }}
                                        </strong>
                                    </span>
                                @endisset    
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>
                            <div class="col-md-6">
                                <input id="autocompleteaddress" type="text" class="form-control" name="address" required autocomplete="false" >   
                                <input type="hidden" name="latitude" id="latitude">
                                <input type="hidden" name="longitude" id="longitude">
                                <div id="ShowMap">
                                    <div id="map" style="width:100%;height:300px;margin-top:1em;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="intra-button-black w-100 mt-1">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@section('pagescript')

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&libraries=places&callback=initMap" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        document.getElementById("ShowMap").style.display = "none";
        var input = document.getElementById('autocompleteaddress');
        // create new autocomplete object from Maps API using our input
        var autocomplete = new google.maps.places.Autocomplete(input);
    
        // when new places are selected, process the results
        autocomplete.addListener("place_changed", function() {
            var place = autocomplete.getPlace();
            //console.log(place);
            var lat = place.geometry['location'].lat();
            var lng = place.geometry['location'].lng();
            $('#latitude').val(lat);
            $('#longitude').val(lng);

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: lat,lng:lng},
                zoom: 15,
            });
            addMaker({lat:lat,lng:lng});
            //add maker function
            function addMaker(coords)
            {
                var marker = new google.maps.Marker({
                    map: map,
                    position: coords,
                    title:"your Location"
                });
            }
            document.getElementById("ShowMap").style.display = "block";
        });



    });
</script>
@stop
@endsection
