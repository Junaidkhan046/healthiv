<section class="intra-membership-bar mt-4 pt-3 pb-3">
    <div class="container mt-5 pt-3 mb-4">
        <div class="row ">
            <div class="col-12 d-flex col-sm-6 mb-4 pb-3">
                <h2 class="mt-4 mb-4 align-self-center">Get 23% off a treatment each month</h2>
            </div>
            <div class="col-12 col-sm-6 mb-4 pb-3">
                <div class="intra-membership-block py-4 px-4 float-sm-right">
                    <h3 class=" mb-4">Membership price</h3>
                    <h2 class="mt-4 mb-4">$99</h2>
                    <p class="mb-3 pb-1">Receive a $129 credit towards treatment each month.</p>
                    <a href="{{url('becomeamenber')}}" class="intra-button-black w-100 mt-1">Become a Member</a>
                </div>
            </div>
        </div>
    </div>
</section>