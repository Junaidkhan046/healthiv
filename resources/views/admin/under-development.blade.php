@extends('admin.layout.admin-layout')

@section('content')
<br/>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-12">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3 class="text-center">Under Development</h3>
                        <p class="text-center">This feature will be available very soon! We appreciate your patience.</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-settings"></i>
                    </div>
                    <a href="{{ route('admin-dashboard') }}" class="small-box-footer"><br/>Back<br/><br/></i></a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection