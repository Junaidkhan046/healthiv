@extends('admin.layout.admin-layout')

@section('content')

<div class="card card-default card-outline">
    <div class="card-header">
        @include('admin.partials.breadcrumbs')
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="general-info" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="false">General Information</a>
                    <a class="nav-link" id="profile-picture" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Profile Picture</a>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content">
                    <div class="tab-pane text-left fade active show" id="vert-tabs-home" role="tabpanel" aria-labelledby="general-info">
                        <form action="{{ route('admin-update-profile') }}" method="post" id="update-form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Name</label>
                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $user->name }}">
                                </div>
                                <div class="col-sm-6">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $user->email }}" readonly>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <br/>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="profile-picture">
                        <form action="{{ route('admin-update-picture') }}" method="post" id="update-image-form" enctype="multipart/form-data">
                            <div class="col-sm-12 text-center">
                                @php

                                if(isset($user->image)){
                                    $image = $user->image->media;
                                    echo '<input type="hidden" name="picture_id" value="'.$user->image->id.'">';
                                } else {
                                    $image = "img/avatar.png";
                                    echo '<input type="hidden" name="picture_id" value="0">';
                                }

                                @endphp
                                <style>
                                    .custom-file{
                                        width:unset;
                                    }
                                </style>
                                <img src="{{ url($image) }}" class="media-preview" height="200"><br/><br/>
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id }}">
                                <input type="hidden" name="media" id="new-picture" value="">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input image">
                                    <label class="custom-file-label" for="customFile">&nbsp;</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>

@section('pagescript')

@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<!--Image Crop Wizard-->
<div class="modal fade" id="crop-tool" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Adjust Your Image</h5>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12 text-center" style="padding-left:0px;">
                            <img id="image" src="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Okay, Save</button>
            </div>
        </div>
    </div>
</div>
<!--Image Crop Wizard-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<script>
    // Wait for the DOM to be ready
    $(function() {

        $(".description").summernote();

        $("#update-form").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: "Name cannot be left blank"
                },
                email: {
                    required: "Email cannot be left blank",
                    email: "Invalid email address"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

<script>
    var $modal = $('#crop-tool');
    var image = document.getElementById('image');
    var cropper;

    $("body").on("change", ".image", function(e) {
        var files = e.target.files;
        var done = function(url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function() {
        canvas = cropper.getCroppedCanvas({
            // width: 160,
            // height: 160,
        });
        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                
                $(".media-preview").attr("src",base64data);
                $("#new-picture").val(base64data);
                $modal.modal('hide');

                $("#update-image-form").submit();

            }
        });
    });

</script>
@stop

@endsection