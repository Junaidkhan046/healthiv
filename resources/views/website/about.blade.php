@extends('layouts.website')

@section('content')
<main id="primary" class="site-main file-page-about">

    <article id="post-340" class="post-340 page type-page status-publish hentry">


        <!-- Hero -->
        <div class="jumbotron mb-3">
            <div class="container mt-5 pt-4 mb-3">

                <header class="entry-header">
                    <h1 class="entry-title text-white mt-2 mb-4 pb-2">About Us</h1>
                </header><!-- .entry-header -->

                <hr class="my-4">

            </div>
        </div>
        <!-- END Hero -->

    </article><!-- #post-340 -->








    <div class="container pt-3">

        <div class="row mt-5 mb-4">
            <div class="col-12">

                <h5 class="pb-5">We believe in making wellness mobile. Healthcare shouldn’t be stressful. It should be
                    accessible. <a href="https://healthiv.com/#intra-anchor-book-session">Schedule an appointment</a>
                    online and one of our healthcare professionals will visit you in the comfort of your own home.</h5>
                <h2 class="mb-4">Our team</h2>
            </div>
        </div>


        <div class="row pb-5">




            <div class="col-12 mb-4 pb-3 border text-center">
                <div class="py-4 px-4 intra-content-block float-sm-right">

                    <img class="img-fluid intra-img-rounded-pill"
                        src="https://healthiv.com/wp-content/themes/intra/media/humza.jpg">
                    <h3 class="mt-4 mb-4">Founder and Chief Executive Officer</h3>
                    <h2 class="mt-4 mb-4">Humza Khan</h2>
                    <p class="mb-4">Humza Khan is the Founder and Chief Executive Officer of HealthIV™ and Vasa Health™.
                        Khan is a multi-sector entrepreneur with a strategic focus in healthcare, digital technology,
                        and international trade. With the launch of HealthIV and Vasa Health, Khan aims to create
                        unprecedented interoperability in healthcare for patients, physicians, pharmacists, and home
                        care workers. Also the co-founder of Prestige Infusions, Khan applies business management
                        expertise and the entrepreneurial spirit to the healthcare ecosystem to create intrinsically
                        accessible care and communication. Currently based in New York City, Khan leads strategic
                        collaborations with Silicon Valley giants, focusing on bringing the sensibilities of the
                        physical world into the digital space to drive down the costs of healthcare, open and maintain
                        lines of communication, and create a patient-centric digital healthcare and home care landscape.
                    </p>

                </div>
            </div>

            <div class="col-12 mb-4 pb-3 border text-center">
                <div class="py-4 px-4 intra-content-block">

                    <img class="img-fluid intra-img-rounded-pill"
                        src="https://healthiv.com/wp-content/themes/intra/media/aysha.jpg">
                    <h3 class="mt-4 mb-4">Chief Medical Director, President, and Co-Founder</h3>
                    <h2 class="mt-4 mb-4">Aysha Ahmed</h2>
                    <p class="mb-4">Aysha Ahmed, PharmD, is the Chief Medical Director, President and Co-Founder of
                        HealthIV™ and Vasa Health™. At the helm of HealthIV and Vasa Health, Ahmed coordinates medical
                        collaborations and sets priorities for the development of the company’s health offerings and
                        services. She also serves as the primary staff practitioner, taking a keen interest in the
                        wellbeing of patients. Dr. Ahmed brings a wealth of experience in medicine and pharmaceuticals
                        to infusion therapy and digital healthcare. She previously spent a decade building a network of
                        women-owned and operated specialty pharmacies and clinical centers in New York, New Jersey,
                        Texas, and Florida. She holds a PharmD and bachelor’s degree in biochemistry, and is expected to
                        receive a doctoral degree in naturopathic medicine from the Boucher Institute of Naturopathic
                        Medicine in 2022. Dr. Ahmed’s patient-centric, multidisciplinary healthcare approach is now the
                        subject of study throughout the healthcare industry. Her primary goal at HealthIV is to ensure
                        that medical care and health support are delivered to patients in the safest, most accessible
                        ways possible.</p>


                </div>
            </div>

            <div class="col-12 mb-4 pb-3 border text-center">
                <div class="py-4 px-4 intra-content-block float-sm-right">

                    <img class="img-fluid intra-img-rounded-pill"
                        src="https://healthiv.com/wp-content/themes/intra/media/arsalan.jpg">
                    <h3 class="mt-4 mb-4">Chief Medical Officer </h3>
                    <h2 class="mt-4 mb-4">Arsalan Sheik</h2>
                    <p class="mb-4">Arsalan Sheik, PharmD, is the Chief Medical Officer of HealthIV™. A recognized
                        biotech business leader and pharmaceuticals expert, Sheik has more than a decade of experience
                        in the development, management, and commercialization of specialty medical infusions for the
                        pharmaceutical industry.</p>

                </div>
            </div>

            <div class="col-12 mb-4 pb-3 border text-center">
                <div class="py-4 px-4 intra-content-block float-sm-right">

                    <img class="img-fluid intra-img-rounded-pill"
                        src="https://healthiv.com/wp-content/themes/intra/media/shawn.jpg">
                    <h3 class="mt-4 mb-4">Chief People Officer and Founding Partner</h3>
                    <h2 class="mt-4 mb-4">Shawn Ankari</h2>
                    <p class="mb-4">Shawn Ankari is a Founding Partner and Chief People Officer of HealthIV™, where
                        leads the People &amp; Organization Team, with a focus on the company’s most valuable asset: its
                        diverse talent. Ankari also serves as HealthIV’s Business Development Director, working to bring
                        IV infusion services to communities across the US. Prior to joining HealthIV, Ankari was
                        Managing Director for global leadership advisory and executive real estate firm, Ankari
                        Developments, where he advised organizations going through transformational growth on
                        organizational leadership issues and diversity and inclusion practices. Ankari also brings more
                        than a decade of experience in business development for emerging biotech companies backed by
                        blue-chip venture capital investors. He received a degree in biology from the University of
                        South Florida.</p>

                </div>
            </div>







        </div> <!-- END row -->

    </div> <!-- END container -->
</main>
{{--
@include('website.partials.join_wait_list')
@include('website.partials.membership')
--}}
@endsection