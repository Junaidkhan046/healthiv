<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiry;
use Session;

class EnquiryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $meta = [
            'title' => 'Enquiries',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Enquiries' => route('admin-content')
            ]
        ];

        $enquiries = Enquiry::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.enquiries.index', compact('meta','enquiries'));
    }

    public function delete($id)
    {
        $c = Enquiry::find($id);
        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function changeStatus($id, $status)
    {
        $e = Enquiry::find($id);

        $e->status = $status == 1 ? 2 : 1;
        $e->save();

        return response()->json([
            "status"=>true
        ]);
    }
}
