<?php

namespace App\Http\Controllers;

use App\WaitingList;
use Illuminate\Http\Request;
use Session;

class WaitingController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title' => 'Waiting List',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Waiting' => route('waiting.index')
            ]
        ];

        $waiting = WaitingList::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.waitinglist.index', compact('meta','waiting'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $c = WaitingList::find($id);
        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
