<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Enquiry;
use App\Event;
use App\Location;
use App\Post;
use App\Product;
use App\WaitingList;
use App\AddOn;
use App\Test;
use App\FAQ;
use App\Order;
use App\OrderAddress;
use App\OrderItem;
use App\OrderPayment;
use App\User;
use Illuminate\Http\Request;
use Session;
use Stripe;
use App\PaymentGateway;
use App\Mail\InvoiceMail;
use Illuminate\Support\Facades\Mail;
use App\GoogleApi;
use App\Setting;
class WebsiteController extends Controller
{
    public function landingPage()
    {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        $product = Product::all();
        $location = Location:: all();
        $tests = Test::orderBy('name','asc')->get();
        return view('website.landing', compact('meta', 'product', 'location','tests'));
    }

    //production description
    public function production($slug)
    {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        $product = Product::where('slug',$slug)->first();
        $addons = AddOn::orderBy('name','asc')->get();
        $tests = Test::orderBy('name','asc')->get();
        $locations = Location::orderBy('title','asc')->select('lat','lng')->get();
        $googleapi = GoogleApi::first();
        $setting = Setting::first();
        $nurses = User::role('nurse')->orderBy('id', 'desc')->select('id','latitude','longitude')->get();
        return view('website.production', compact('meta','product','addons','tests','locations','googleapi','setting','nurses'));
    }

    public function becomeamenber()
    {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        $products = Product::orderBy('name','asc')->get();
        $addons = AddOn::orderBy('name','asc')->get();
        $tests = Test::orderBy('name','asc')->get();
        $locations = Location::orderBy('title','asc')->get();
        return view('website.becomeamenber', compact('meta','products','addons','tests','locations'));
    }

    public function faq()
    {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        $faqs = FAQ::orderBy('id','asc')->get();
        return view('website.faq', compact('meta','faqs'));
    }

    public function checkout(Request $request)
    {
        if($request->zipcode==''){
            Session::flash('address', 'Please enter a valid address.');
            return back();
        }
        
        if($request->location==''){
            Session::flash('address', 'Location is Required.'); 
            return back();
        }

        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];
        $product = Product::where('id',$request->product)->first();
        $addons = AddOn::find($request->addons_id);
        $tests = Test::find($request->test_id);
        $address = [];
        $address['zipcode']  = $request->zipcode;
        $address['city'] = $request->city;
        $address['state'] = $request->state;
        $address['country'] = $request->country;
        $address['latitude'] = $request->latitude;
        $address['longitute'] = $request->longitute;
        $address['time'] = $request->timeschedule;
        $address['date'] = $request->schedule;
        $address['nurse_id'] = $request->nurse_id;
        $address = json_encode($address);

        $nurses = User::role('nurse')->orderBy('id', 'desc')->select('id','latitude','longitude')->get();

        return view('website.checkout', compact('meta','product','addons','tests','address','nurses'));
    }

    public function checkoutStore(Request $request)
    {
        $product = Product::find($request->product_id);
        $addons = AddOn::find($request->addons_id);
        $tests = Test::find($request->tests_id);
        $location = $request->location;
        $schedule = $request->schedule;
        $address = json_decode($request->address,true);
            
        if(User::find($request->user_id)){
            $user = User::find($request->user_id);
        }else{
            if(User::where('email',$request->customer_email)->exists())
            {
               $user = User::where('email',$request->customer_email)->first();
               if(Hash::check($request->password, $user->password)){
                    Auth::login($user);
               }else{
                    Session::flash('password', 'Opps : Wrong Password.');
                    return back();
               }
            }else{
                $user = new User;
                $user->name      = $request->customer_name;
                $user->email     = $request->customer_email;
                $user->phone     = $request->customer_phone;
                $user->latitude  = $address['latitude'] ;
                $user->longitude = $address['longitute'] ;
                $user->address   = $request->location;
                $user->password  = Hash::make($request->password);
                $user->save();
                $user->assignRole('user');
                Auth::login($user);
            }
        }

        $order = new Order;
        $order->user_id        = $user->id;
        $order->customer_name  = $request->customer_name;
        $order->customer_email = $request->customer_email;
        $order->customer_phone = $request->customer_phone;
        $order->amount         = $product->selling_price;
        if(!empty($address['nurse_id'])){
            $order->tranking_status= 'Assigned';
        }
    $order->assigned_nurse_id  = $address['nurse_id'];
        $order->time           = $address['time'];
        $order->date           = $address['date'];
        $order->status         = 'Unpaid';
        if($order->save())
        {
            $item = new OrderItem;
            $item->order_id = $order->id;
            $item->product_id = $request->product_id;
            $item->sku = $product->sku;
            $item->price = $product->selling_price;
            $item->total_amount = $product->selling_price;
            $item->qty = 1;
            $item->type = 'product';
            $item->save();
            if($request->addons_id!=''){
                $addons = new OrderItem;
                $addons->order_id = $order->id;
                $addons->product_id = $request->addons_id;
                $addons->type = 'addons';
                $addons->save();
            }
            if($request->tests_id!=''){
                $tests = new OrderItem;
                $tests->order_id = $order->id;
                $tests->product_id = $request->tests_id;
                $tests->type = 'tests';
                $tests->save();
            }
            $add = new OrderAddress;
            $add->order_id = $order->id;
            $add->address_line = $request->location;
            $add->zipcode      = $address['zipcode'] ;
            $add->city = $address['city'] ;
            $add->state = $address['state'] ;
            $add->country = $address['country'] ;
            $add->lat = $address['latitude'] ;
            $add->lng = $address['longitute'] ;
            $add->save();
            
            /* 
                $contact_data['url'] = url('order/details/',$order->id);
                $contact_data['msg'] = 'invoice';
                Mail::to($order->customer_email)->send(new InvoiceMail($contact_data));
            */

            Session::flash('success', 'Your Order Send Successfully.');
            return redirect('stripe/'.$order->id);
            //return redirect('order/details/'.$order->id);
        }

    }

    public function stripe(Request $request ,$id)
    {
        if(!$request->user()){
            return redirect('login');
        }
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];

        $apikey = PaymentGateway::first()->stripe_key;
        $order = Order::find($id);
        return view('website.payment', compact('meta','order','apikey'));
    }
    public function stripePost(Request $request,$id)
    {


        if(!$request->user()){
            return redirect('login');
        }
        $order = Order::find($id);
        //return $request;
        $payment = PaymentGateway::first();
        $amount = $order->amount;
		$amount *= 100;
        $amount = (int) $amount;
        Stripe\Stripe::setApiKey($payment->stripe_secret);
        $pymnt = \Stripe\PaymentIntent::create([
			'description' => 'Stripe Test Payment',
			'amount' => $amount,
			'currency' => 'usd',
			'description' => 'Payment From Codehunger',
			'payment_method_types' => ['card'],
		]);
		
        $order->status = 'Paid';
        $order->save();
        $pyament = new OrderPayment;
        $pyament->order_id = $id;
        $pyament->amount = $order->amount;
        $pyament->transaction_id = $pymnt->id;
        $pyament->payment_code = "0000";
        $pyament->save();
        return redirect('order/details/'.$order->id);
    }

    public function orders(Request $request)
    {
        if(!$request->user()){
            return redirect('login');
        }

        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];

        $orders = Order::where('user_id',$request->user()->id)->get();
        if(!empty($request->status))
        {
            $orders = $orders->where('tranking_status',$request->status);
        }
        return view('website.orderlist', compact('meta','orders'));
    }

    public function tracking(Request $request, $id)
    {
        if(!$request->user()){
            return redirect('login');
        }
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];

        $order = Order::find($id);
        return view('website.track_map', compact('meta','order'));
    }

    public function details(Request $request, $id)
    {
        if(!$request->user()){
            return redirect('login');
        }
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];

        $order = Order::find($id);
        return view('website.tracking', compact('meta','order'));
    }

    public function blog()
    {
        $meta = [
            'title' => 'HealthIV - Blog'
        ];

        $posts = Post::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));

        return view('website.blog', compact('meta','posts'));
    }

    public function post($slug)
    {
        $meta = [
            'title' => 'HealthIV - Post'
        ];

        $post = Post::where('slug',$slug)->get()->first();
        return view('website.post', compact('meta','post'));
    }

    /**
     * This function is used view of event
     */
    public function event(){
        $meta = [
            'title' => 'HealthIV - Event and News'
        ];

        $events = Event::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));

        return view('website.event', compact('meta','events'));
    }

    public function news($slug)
    {
        $meta = [
            'title' => 'HealthIV - Event and News'
        ];

        $event = Event::where('slug',$slug)->get()->first();
        return view('website.news', compact('meta','event'));
    }

    public function about()
    {
        $meta = [
            'title' => 'HealthIV - About'
        ];

        return view('website.about', compact('meta'));
    }

    public function contact()
    {
        $meta = [
            'title' => 'HealthIV - Contact'
        ];

        return view('website.contact', compact('meta'));
    }

    public function createEnquiry(Request $request)
    {
        $faq = Enquiry::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'zip_code' => $request->zip_code,
            'message' => $request->message,
            'status' => 1,
        ]);

        Session::flash('success', 'We received your message. We will get back to you shortly.'); 
        return redirect()->back();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeWaitlist(Request $request)
    {
        WaitingList::create($request->all());
        Session::flash('success', 'We received your message.'); 
        return redirect()->back();
    }

    public function cart()
    {
        $meta = [
            'title' => 'HealthIV - Cart'
        ];

        return view('website.cart', compact('meta'));
    }

    /**
     * This function is used to show registeration form nurse
     */
    public function nurseRegisteration() {
        return view('auth.nurse.register');
    }

    public function sendmail(Request $request)
    {
        $contact_data['url'] = url('testing');
        $contact_data['msg'] = 'invoice';
        Mail::to('rajputumesh117@gmail.com')->send(new InvoiceMail($contact_data));
        return "mail testing";
    }
    
    public function checknurseassign(Request $request) {
        if(!Order::where('assigned_nurse_id',$request->nurse_id)->where('time',$request->timeschedule)->where('date',$request->date)->exists())
        {
            return 'OK';
        }
        return 'NO';
    }

    public function re_schedule(Request $request, $id) {
        $meta = [
            'title' => 'HealthIV - IV Infusion Therapy Delivered to you'
        ];

        $googleapi = GoogleApi::first();
        $setting = Setting::first();
        $order = Order::find($id);
        return view('website.re_schedule', compact('meta', 'order','googleapi','setting'));
    }

    public function re_schedule_update(Request $request, $id) {
        $order = Order::find($id);
        $order->date = $request->schedule;
        $order->time = $request->timeschedule;
        $order->save();
        Session::flash('success', 'Schedule Updated.');
        return redirect('orders');
    }

    public function complete_orders($id)
    {
        $order = Order::find($id);
        $order->status = 'Complete';
        $order->save();
        Session::flash('success', 'Order Completed.');
        return back();
    }
}
