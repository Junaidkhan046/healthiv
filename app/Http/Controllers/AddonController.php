<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddOn;
use App\AddOnMedia;
use Session;

class AddonController extends Controller
{
    
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $meta = [
                'title' => 'Addons',
                'breadcrumbs' => [
                    'Home' => route('admin-dashboard'),
                    'Addons' => route('addon.index')
                ]
            ];
    
            $addons = AddOn::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
            return view('admin.addons.index', compact('meta','addons'));
        }
    
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $meta = [
                'title' => 'Add Addons',
                'breadcrumbs' => [
                    'Home' => route('admin-dashboard'),
                    'Addons' => route('addon.index')
                ]
            ]; 
    
            return view('admin.addons.create', compact('meta'));
        }
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $validator =  $request->validate([
                'name' => ['required', 'max:255'],
                'media' => ['required'],
            ]);
            if($request->media){
                $file = $this->saveBase64Image('uploads/addons/', $request->media);
            }
            $addon = AddOn::create([
                'name' => $request->name,
                'description' => $request->description,
                'image' => $file,
            ]);
    
            
    
            Session::flash('success', 'addon Created.'); 
            return redirect()->route('addon.index');
        }
    
        /**
         * Display the specified resource.
         *
         * @param  \App\addon  $addon
         * @return \Illuminate\Http\Response
         */
        public function show(AddOn $addon)
        {
            $meta = [
                'title' => 'View Addon',
                'breadcrumbs' => [
                    'Home' => route('admin-dashboard'),
                    'Addons' => route('addon.show',$addon->id)
                ]
            ];
    
            return view('admin.addons.show', compact('meta','addon'));
        }
    
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\addon  $addon
         * @return \Illuminate\Http\Response
         */
        public function edit(AddOn $addon)
        {
            $meta = [
                'title' => 'Edit Addon',
                'breadcrumbs' => [
                    'Home' => route('admin-dashboard'),
                    'Addons' => route('addon.edit',$addon->id)
                ]
            ];
    
            return view('admin.addons.edit', compact('meta','addon'));
        }
    
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\addon  $addon
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, AddOn $addon)
        {
            $validator =  $request->validate([
                'name' => ['required', 'max:255'],
            ]);

            $addon->name = $request->name;
            $addon->description = $request->description;

            if($request->media){
                try{
                    unlink($addon->image);
                } catch(\Exception $e) {
                    // nothing to do
                }
                $file = $this->saveBase64Image('uploads/addons/', $request->media);
                $addon->image = $file;
            }
            $addon->save();
    
            Session::flash('success', 'addon Updated.'); 
            return redirect()->route('addon.index');
        }
    
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\addon  $addon
         * @return \Illuminate\Http\Response
         */
        public function destroy(addon  $addon)
        {
    //
        }
        public function delete($id)
        {
            $c = AddOn::find($id);
            $c->delete();
    
            return response()->json([
                "status"=>true
            ]);
        }
    }
    