<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostMedia;
use Session;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $meta = [
            'title' => 'Blog Posts',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Blog Posts' => route('admin-posts')
            ]
        ];

        $posts = Post::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.blog.index', compact('meta','posts'));
    }

    public function add()
    {
        $meta = [
            'title' => 'Add Post',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add Post' => route('admin-add-post')
            ]
        ]; 

        return view('admin.blog.add', compact('meta'));
    }

    public function create(Request $request)
    {
        $post = Post::create([
            'title' => $request->title,
            'slug' => $this->getSlug($request->title),
            'short_description' => $request->short_description,
            'description' => $request->description
        ]);

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/posts/', $image);
                $media = PostMedia::create([
                    'post_id' => $post->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Post Created.'); 
        return redirect()->route('admin-posts');
    }

    public function edit($id)
    {
        $meta = [
            'title' => 'Edit Post',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit Post' => route('admin-edit-post',$id)
            ]
        ];

        $post = Post::find($id);

        return view('admin.blog.edit', compact('meta','post'));
    }

    public function update(Request $request)
    {
        $post = Post::find($request->id);

        $post->title = $request->title;
        $post->short_description = $request->short_description;
        $post->description = $request->description;
        $post->save();

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/posts/', $image);
                $media = PostMedia::create([
                    'post_id' => $post->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Post Updated.'); 
        return redirect()->route('admin-posts');
    }

    public function delete($id)
    {
        $c = Post::find($id);

        foreach($c->media as $m){
            try{
                unlink($m->media);
            } catch(\Exception $e) {
                // nothing to do
            }
            PostMedia::find( $m->id )->delete();
        }

        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function deletMedia($id)
    {
        $m = PostMedia::find($id);

        try{
            unlink($m->media);
        } catch(\Exception $e) {
            // nothing to do
        }
        
        $m->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
