<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LocationMedia;

class Location extends Model {

    protected $table = 'locations';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'address', 'lat', 'lng'];

    public function media()
    {
        return $this->hasMany(LocationMedia::class,'location_id','id');
    }

}