@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">

                <!-- Product Hero -->
                <div class="jumbotron book-your-first-month mb-0 ">
                    <div class="container mt-5 pt-4 mb-3">
                        <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                            <figure class="woocommerce-product-gallery__wrapper">
                                <div data-thumb="https://static.thenounproject.com/png/1328079-200.png" data-thumb-alt="" class="woocommerce-product-gallery__image">
                                    <a href="https://static.thenounproject.com/png/1328079-200.png">
                                        <img style="max-width: 100px;" src="https://static.thenounproject.com/png/1328079-200.png" class="wp-post-image" alt="">
                                    </a>
                                </div>	
                            </figure>
                        </div>

                        <div style="clear:both;">
                            <h1 class="product_title entry-title mb-4 pb-2 pt-2">Tracking Order</h1>
                            
                            <hr class="my-4">

                            <div class="pt-3">

                                <p>All your health IV</p>

                            </div>
                        </div>

                    </div>
                </div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">


    	<section class="intra-product-block-title">
	    	<div class="container my-5" style="">

	    		<div class="justify-content-center">
                    <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                
                                <a class="btn btn-primary text-decoration-none text-dark">{{$order->tranking_status}} Orders</a>
                                <div class="float-right">
                                    <a href="{{url('orders')}}" class="btn btn-success text-decoration-none">Back</a>
                                    @if($order->assigned_nurse_id != '' && $order->status == 'Paid')
                                        <a href="{{url('order/tracking/'.$order->id)}}" class="btn btn-warning text-decoration-none" target="_blank">Track Order</a>
                                    @endif
                                </div>
                                <div class="mt-5">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>TYPE</th>
                                            <th>SKU</th>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($order->orderitems))
                                            @foreach($order->orderitems as $item)
                                                @if($item->type=='product')
                                                <tr>
                                                    <td>{{$item->type}}</td>
                                                    <td>{{@$item->product->sku}}</td>
                                                    <td>{{@$item->product->name}}</td>
                                                    <td>${{$item->price}}</td>
                                                    <td>${{$item->total_amount}}</td>
                                                </tr>
                                                @endif

                                                @if($item->type=='addons')
                                                <tr>
                                                    <td>{{$item->type}}</td>
                                                    <td></td>
                                                    <td>{{@$item->addons->name}}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @endif

                                                @if($item->type=='tests')
                                                <tr>
                                                    <td>{{$item->type}}</td>
                                                    <td></td>
                                                    <td>{{@$item->tests->name}}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                </div>
                                @if($order->address)
                                <div id="address">
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Address</th>
                                            <th>Zipcode</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <tr id="record-{{ $order->id }}">
                                            <td>#{{ @$order->address->id }}</td>
                                            <td>{{ @$order->address->address_line }}</td>
                                            <td>{{ @$order->address->zipcode }}</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                </div>
                                @endif
                                @if($order->payment)
                                <div>
                                    <h4>Payment</h4>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Amount</th>
                                                <th>Transaction Id</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr id="record-{{ $order->id }}">
                                                <td>#{{ @$order->payment->id }}</td>
                                                <td>$ {{ @$order->payment->amount }}</td>
                                                <td>{{ @$order->payment->transaction_id }}</td>
                                                <td>
                                                    @if(@$order->payment->status==1)
                                                        SUCCESS
                                                    @else
                                                        FAILED
                                                    @endif
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                            <!-- /.card-body -->
                </div>
            </div>
        </section>
    </div>


		
</main>
@section('pagescript')
    @if(\Session::has('success'))
    <script>
        swal("Success!", "{{ Session::get('success') }}", "success");
    </script>
    @endif
@stop
@endsection