<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class PostMedia extends Model {

    protected $table = "post_medias";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'media'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}