<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserMedia extends Model {

    protected $table = "user_medias";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'media'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}