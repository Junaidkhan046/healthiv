@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">
                <!-- Product Hero -->
                <div class="jumbotron book-your-first-month mb-0 ">
                    <div class="container mt-5 pt-4 mb-3">
                        <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                            <figure class="woocommerce-product-gallery__wrapper">
                                <div data-thumb="https://static.thenounproject.com/png/363639-200.png" data-thumb-alt="" class="woocommerce-product-gallery__image">
                                    <a href="https://static.thenounproject.com/png/363639-200.png">
                                        <img style="max-width: 100px;" src="https://static.thenounproject.com/png/363639-200.png" class="wp-post-image" alt="">
                                    </a>
                                </div>	
                            </figure>
                        </div>

                        <div style="clear:both;">
                            <h1 class="product_title entry-title mb-4 pb-2 pt-2">My Profile</h1>
                            
                            <hr class="my-4">

                            <div class="pt-3">

                                <p>All your health IV</p>

                            </div>
                        </div>

                    </div>
                </div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">
    	<section class="intra-product-block-title">
	    	<div class="container my-5" style="">
	    		<div class="justify-content-center">
                    <!-- /.card-header -->
                            <div class="card-body p-0">
                                <form action="{{ route('update-picture') }}" method="post" id="update-image-form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row form-group">
                                        <div class="col-5">
                                        @php
                                            if(isset(Auth::user()->image)){
                                                $image = Auth::user()->image->media;
                                                echo '<input type="hidden" name="picture_id" value="'.Auth::user()->image->id.'">';
                                            } else {
                                                $image = asset('img/avatar.png');
                                                echo '<input type="hidden" name="picture_id" value="0">';
                                            }
                                        @endphp
                                        <style>
                                            .custom-file{
                                                width:unset;
                                            }
                                        </style>
                                            <img src="{{ url($image) }}" class="media-preview" height="200">
                                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                            <div class="my-4">
                                                <input id="image" type="file" class="form-control" name="image" style="width:220px">
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row form-group">
                                        <div class="col-6">
                                            <label for="name"> Name</label>
                                            <input type="text" class="form-control" name="name" id="" placeholder="Name" value="@auth {{ Auth::user()->name }} @endauth" required>
                                        </div>
                                        <div class="col-6">
                                            <label for="phone">Phone</label>
                                            <input type="text" class="form-control" name="phone" id="" placeholder="Phone No" value="@auth {{ Auth::user()->phone }} @endauth" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="12" minlength="10" required>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="email"> Email</label>
                                        <input type="email" class="form-control" name="email" disabled placeholder="Email" value="@auth {{ Auth::user()->email }} @endauth" required>
                                    </div>
                                    <div class="form-group mt-4">
                                        <label for="address">Address</label>
                                        <input id="autocompleteaddress" type="text" class="form-control" name="address" value="@auth {{ Auth::user()->address }} @endauth" autocomplete="false" >   
                                        <input type="hidden" name="latitude" id="latitude" value="@auth {{ Auth::user()->latitude }} @endauth">
                                        <input type="hidden" name="longitude" id="longitude" value="@auth {{ Auth::user()->longitude }} @endauth">
                                        <div id="ShowMap">
                                            <div id="map" style="width:100%;height:300px;margin-top:1em;"></div>
                                        </div>
                                    </div>
                                    <div class="row form-group justify-content-center mt-4 py-3">
                                        <div class="col-6">
                                            <button type="submit" class="btn btn-primary btn-block"> Upload </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                </div>
            </div>
        </section>
    </div>		
</main>

@section('pagescript')

@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<!--Image Crop Wizard-->
<div class="modal fade" id="crop-tool" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Adjust Your Image</h5>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12 text-center" style="padding-left:0px;">
                            <img id="image" src="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Okay, Save</button>
            </div>
        </div>
    </div>
</div>
<!--Image Crop Wizard-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&libraries=places&callback=initMap" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        //document.getElementById("ShowMap").style.display = "none";
        var input = document.getElementById('autocompleteaddress');
        // create new autocomplete object from Maps API using our input
        var autocomplete = new google.maps.places.Autocomplete(input);

        // when new places are selected, process the results
        autocomplete.addListener("place_changed", function() {
            var place = autocomplete.getPlace();
            console.log(place);
            var lat = place.geometry['location'].lat();
            var lng = place.geometry['location'].lng();
            $('#latitude').val(lat);
            $('#longitude').val(lng);

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: lat,lng:lng},
                zoom: 15,
            });
            addMaker({lat:lat,lng:lng});
            //add maker function
            function addMaker(coords)
            {
                var marker = new google.maps.Marker({
                    map: map,
                    position: coords,
                    title:"your Location"
                });
            }
            document.getElementById("ShowMap").style.display = "block";
        });
    });
</script>
<script>
    var lat1 = {{Auth::user()->latitude}};
    var lng1 = {{Auth::user()->longitude}};
    if(lat1 != null){
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat1,lng:lng1},
            zoom: 15,
        });
        addMaker({lat:lat1,lng:lng1});
        //add maker function
        function addMaker(coords)
        {
            var marker = new google.maps.Marker({
                map: map,
                position: coords,
                title:"your Location"
            });
        }
        document.getElementById("ShowMap").style.display = "block";
    }
</script>
<script>
        $("#update-form").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: "Name cannot be left blank"
                },
                email: {
                    required: "Email cannot be left blank",
                    email: "Invalid email address"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
</script>

@stop
@endsection