<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="msapplication-TileImage" content="{{asset('assets/images/cropped-favicon-270x270.jpg')}}" />

<title>{{ $meta['title'] }}</title>

<link rel='stylesheet' href='{{asset("assets/css/style.min.css")}}' media='all' />
<link rel='stylesheet' href='{{asset("assets/css/style.css")}}' media='all' />
<link rel="icon" href="{{asset('assets/images/cropped-favicon-32x32.jpg')}}" sizes="32x32" />
<link rel="icon" href="{{asset('assets/images/cropped-favicon-192x192.jpg')}}" sizes="192x192" />
<link rel="apple-touch-icon" href="{{asset('assets/images/cropped-favicon-180x180.jpg')}}" />

<style>
    .yikes-mailchimp-field-required{
        width:100%;
    }
</style>

<script src='{{asset("assets/js/jquery.min.js")}}'></script>
<script src='{{asset("assets/js/jquery-migrate.min.js")}}'></script>
<script src='{{asset("assets/js/main.js")}}' id='main-js-js'></script>