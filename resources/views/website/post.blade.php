@extends('layouts.website2')

@section('content')
<article class="post type-post status-publish format-standard has-post-thumbnail">
    <header class="entry-header">
        <div class="jumbotron bg-white mb-0 pb-0">
            <div class="container mt-5 pt-4 mb-4">

                <h2 class="entry-title">{{$post->title}}</h2>
                <!-- <div class="entry-meta">
                    <span class="byline">
                        Published by
                        <span class="author vcard">
                            <a class="url fn n" href="#">Humza Khan</a>
                        </span>
                    </span>
                    <span class="posted-on"><time class="entry-date published updated" datetime="2021-07-21T11:51:15-04:00">July 21, 2021</time></span>
                </div>
                <a href="#" class="intra-button mt-1 mr-5 mb-4">Share</a> -->
            </div>
        </div>
    </header>
    <div class="entry-content">
        <div class="container pb-5 pt-4">
            <div class="row">
                <div class="col-12">
                    <div class="post-thumbnail">
                        @if(!empty($post->media->first()))
                        <img src="{{asset($post->media->first()->media)}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="{{$post->title}}" />
                        @else
                        <img src="{{asset('assets/images/liquid_wuxga.jpg')}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="{{$post->title}}" />
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! $post->description !!}
                </div>
            </div>
        </div>
    </div>
    <footer class="entry-footer"></footer>
</article>
@include('website.partials.join_wait_list')
@include('website.partials.membership')
@endsection