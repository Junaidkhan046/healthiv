<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestMedia extends Model
{
    protected $table = "test_media";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['test_id', 'media'];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }
}
