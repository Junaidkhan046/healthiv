@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">
                <!-- Product Hero -->
                <div class="jumbotron book-your-first-month mb-0 ">
                    <div class="container mt-5 pt-4 mb-3">
                        <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                            <figure class="woocommerce-product-gallery__wrapper">
                                <div data-thumb="https://static.thenounproject.com/png/363639-200.png" data-thumb-alt="" class="woocommerce-product-gallery__image">
                                    <a href="https://static.thenounproject.com/png/363639-200.png">
                                        <img style="max-width: 100px;" src="https://static.thenounproject.com/png/363639-200.png" class="wp-post-image" alt="">
                                    </a>
                                </div>	
                            </figure>
                        </div>

                        <div style="clear:both;">
                            <h1 class="product_title entry-title mb-4 pb-2 pt-2">Dashboard</h1>
                            
                            <hr class="my-4">

                            <div class="pt-3">

                                <p>All your health IV</p>

                            </div>
                        </div>

                    </div>
                </div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">
    	<section class="intra-product-block-title">
	    	<div class="container my-5" style="">
	    		<div class="justify-content-center">
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="row mt-5 justify-content-center">
                            <div class="col-4 mb-5">
                                <div class="bg-info p-3 text-center">
                                    <h4>{{Auth::user()->nurseorders->count()}}</h4>
                                    <p>Total Order</p>
                                    <a href="{{url('nurse/orders')}}?status=" class="text-decoration-none">View</a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="bg-warning p-3 text-center">
                                    <h4>{{Auth::user()->nurseorders->where('tranking_status','Assigned')->count()}}</h4>
                                    <p>Assigned Order</p>
                                    <a href="{{url('nurse/orders')}}?status=Assigned" class="text-decoration-none">View</a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="bg-primary p-3 text-center">
                                    <h4>{{Auth::user()->nurseorders->where('tranking_status','On The Way')->count()}}</h4>
                                    <p>On The Way Order</p>
                                    <a href="{{url('nurse/orders')}}?status=On The Way" class="text-decoration-none">View</a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="bg-success p-3 text-center">
                                    <h4>{{Auth::user()->nurseorders->where('tranking_status','Delivered')->count()}}</h4>
                                    <p>Delivered Order</p>
                                    <a href="{{url('nurse/orders')}}?status=Delivered" class="text-decoration-none">View</a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="bg-danger p-3 text-center">
                                    <h4>{{Auth::user()->nurseorders->where('tranking_status','Cancelled')->count()}}</h4>
                                    <p>Cancelled</p>
                                    <a href="{{url('nurse/orders')}}?status=Cancelled" class="text-decoration-none text-dark">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </section>
    </div>		
</main>

@endsection