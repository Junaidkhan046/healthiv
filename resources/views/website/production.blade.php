@extends('layouts.website2')

@section('content')
<link rel="stylesheet" href="{{asset('calender/tavo-calendar.css')}}">
<main id="primary" class="site-main file-woocommerce">


    <div id="product-460"
        class="product type-product post-460 status-publish first instock product_cat-treatments has-post-thumbnail virtual sold-individually purchasable product-type-booking">


        <!-- Product Hero -->
        <div class="jumbotron all-inclusive mb-0 " style="background: #F59D81;">
            <div class="container mt-5 pt-4 mb-3">

                <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images"
                    data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                    <figure class="woocommerce-product-gallery__wrapper">
                        <div data-thumb="{{asset($product->productImage->first()->path)}}" data-thumb-alt=""
                            class="woocommerce-product-gallery__image"><a
                                href="{{asset($product->productImage->first()->path)}}">
                                <img style="max-width: 110px;"
                                    src="{{asset($product->productImage->first()->path)}}"></a></div>
                    </figure>
                </div>


                <div style="clear:both;">
                    <h1 class="product_title entry-title mt-4 mb-4 pb-2 pt-4">{{$product->name}}</h1>
                    <h2 class="mt-4 mb-4 pt-2 pb-3 price">
                        <span class="woocommerce-Price-amount amount"><bdi><span
                                    class="woocommerce-Price-currencySymbol">$</span>{{$product->selling_price}}</bdi></span>
                    </h2>

                    <hr class="my-4">

                    <div class="pt-3">

                        <p>{!! $product->description !!}</p>
                    </div>
                </div>



            </div>
        </div>
        <!-- END Product Hero -->



        <div class="summary entry-summary">

            <form action="{{url('checkout')}}" method="get">
            @csrf
                <input type="hidden" name="product" value="{{$product->id}}">
                <section class="intra-product-block-title">
                    <div class="container pt-5 pb-3">

                        <div class="row pt-2">
                            <div class="col-12">

                                <h2 class="my-4">Fill out the forms below to book your session</h2>


                                <div class="woocommerce-notices-wrapper"></div>
                                <noscript>Your browser must support JavaScript in order to make a booking.</noscript>

                                <form class="cart" method="post" enctype="multipart/form-data" data-nonce="25d2b58f05">



                                    <section class="intra-product-block-location">
                                        <div class="container py-5">

                                            <div class="row pt-2 pb-2 mb-1">
                                                <div class="col-12">

                                                    <h2 class="my-4">1. Location</h2>
                                                    <p class="mb-4 pb-3">We come to you. Let’s make sure you’re in our
                                                        service area.</p>

                                                    <div class="form-group mb-1 pb-1">

                                                        <div class="wc-pao-addons-container">
                                                            <div class="wc-pao-addon-container wc-pao-required-addon wc-pao-addon wc-pao-addon-service-location"
                                                                data-product-name="Energy Lift">


                                                                <label for="addon-460-service-location-2">Service
                                                                    Location <em class="required"
                                                                        title="Required field">*</em>&nbsp;</label>


                                                                <p
                                                                    class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-460-service-location-2">
                                                                    <input type="text" name="location" required
                                                                        class="autocomplete_address input-text wc-pao-addon-field wc-pao-addon-custom-text pac-target-input"
                                                                        placeholder="Enter a location"
                                                                        autocomplete="off">
                                                                    <input type="hidden" name="country" id="country">
                                                                    <input type="hidden" name="state" id="state">
                                                                    <input type="hidden" name="city" id="city">
                                                                    <input type="hidden" name="zipcode" id="zipcode">
                                                                    <input type="hidden" name="latitude" id="latitude">
                                                                    <input type="hidden" name="longitute" id="longitute">
                                                                    <input type="hidden" name="nurse_id" id="nurse_id">
                                                                    @if(Session::has('address'))
                                                                <p class="text-danger">
                                                                    <b>{{ Session::get('address') }}</b>
                                                                <p>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div id="in_range_wrapper" style="display:none;">
                                                            <span class="text-success">Great News! You're in range of one of our
                                                                locations. <br>Please book your session below.</span>

                                                        </div>

                                                        <div class="text-danger" id="out_of_range_wrapper" style="display:none;">
                                                            <span>We apologize, you are not close enough to any of our
                                                                locations.</span>
                                                        </div>

                                                        <div class="text-danger" id="fail_wrapper" style="display:none;">
                                                            <span>We can't find that, please try entering a different
                                                                address.</span>
                                                        </div>

                                                        <div id="ShowMap">
                                                            <div id="map" style="width:100%;height:300px"></div>
                                                        </div>




                                                        <div class="wc-pao-addons-container">
                                                            <div class="wc-pao-addon-container  wc-pao-addon wc-pao-addon-healthivbranchzone" data-product-name="Energy Lift">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                    </section>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function($) {
                                                var destancevalue = "{{ $googleapi->distance }}";
                                                var locations = JSON.parse('{!! json_encode($locations) !!}');
                                                //console.log(locations);
                                                document.getElementById("ShowMap").style.display = "none";
                                                //console.log("Google API related custom JS is loaded");

                                                var intra_location_input = document.querySelector(".autocomplete_address");

                                                // create new autocomplete object from Maps API using our input
                                                var autocomplete = new google.maps.places.Autocomplete(intra_location_input);

                                                // when new places are selected, process the results
                                                autocomplete.addListener("place_changed", function() {
                                                    var place = autocomplete.getPlace();
                                                    document.getElementById("ShowMap").style.display = "block";
                                                    //console.log(place);
                                                    var lat = place.geometry['location'].lat();
                                                    var lng = place.geometry['location'].lng();
                                                    $('#latitude').val(lat);
                                                    $('#longitute').val(lng);

                                                    var map = new google.maps.Map(document.getElementById('map'), {
                                                        center: {
                                                            lat: lat,
                                                            lng: lng
                                                        },
                                                        zoom: 15,
                                                    });
                                                    addMaker({lat: lat, lng: lng });
                                                    //add maker function
                                                    function addMaker(coords) {
                                                        var marker = new google.maps.Marker({
                                                            map: map,
                                                            position: coords,
                                                            title: "your Location",
                                                            travelMode: 'DRIVING'
                                                        });
                                                    }

                                                    //console.log(locations);
                                                    var stop = '';
                                                    locations.forEach(function(item, index) {
                                                        var distance = calcCrow(item.lat,item.lng,lat,lng).toFixed(1);  
                                                        if(parseFloat(destancevalue) >= parseFloat(distance))
                                                        {
                                                            console.log(distance);
                                                            stop = 1; 
                                                        }
                                                    });
                                                    if(stop==1){
                                                        jQuery("#in_range_wrapper").show(); 
                                                        jQuery("#fail_wrapper").hide();
                                                        jQuery("#out_of_range_wrapper").hide();


                                                    }else{
                                                        jQuery("#in_range_wrapper").hide(); 
                                                        jQuery("#fail_wrapper").show(); 
                                                        jQuery("#out_of_range_wrapper").show();
                                                        $('.autocomplete_address').val('');
                                                    }
                                                    
                                                    //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
                                                    function calcCrow(lat1, lon1, lat2, lon2) 
                                                    {
                                                    var R = 6371; // km
                                                    var dLat = toRad(lat2-lat1);
                                                    var dLon = toRad(lon2-lon1);
                                                    var lat1 = toRad(lat1);
                                                    var lat2 = toRad(lat2);
                                                
                                                    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                                                        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
                                                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                                                    var d = R * c;
                                                    return d;
                                                    }
                                                
                                                    // Converts numeric degrees to radians
                                                    function toRad(Value) 
                                                    {
                                                        return Value * Math.PI / 180;
                                                    }


                                                    for (var i = 0; i < place.address_components.length; i++) {
                                                        for (var j = 0; j < place.address_components[i].types.length; j++) {
                                                            if (place.address_components[i].types[j] == "postal_code") {
                                                                $('#zipcode').val(place.address_components[i].long_name);
                                                            }
                                                            if (place.address_components[i].types[j] =="country") {
                                                                $('#country').val(place.address_components[i].long_name);
                                                            }
                                                            if (place.address_components[i].types[j] =="administrative_area_level_1") {
                                                                $('#state').val(place.address_components[i].long_name);
                                                            }

                                                            if (place.address_components[i].types[j] =="administrative_area_level_2") {
                                                                $('#city').val(place.address_components[i].long_name);
                                                            }
                                                        }
                                                    }


                                                    //get nurse 

                                                    var nurses = JSON.parse('{!! json_encode($nurses) !!}');
                                                    var userdestancevalue = '';
                                                    var nurse_id = '';
                                                    //console.log(nurses);
                                                    nurses.forEach(function(item, index) {
                                                        var lat2 = parseFloat(item.latitude);
                                                        var lng2 = parseFloat(item.longitude);
                                                        //console.log(lat2+" = lat2, lng2 = "+lng2);
                                                        var distance = calcCrow(lat2,lng2,lat,lng).toFixed(2);
                                                        if(userdestancevalue == ''){
                                                            userdestancevalue = distance;
                                                        }
                                                        //console.log("distance = "+distance+", userdestancevalue = "+userdestancevalue);
                                                        if(parseFloat(userdestancevalue) >= parseFloat(distance))
                                                        {
                                                            userdestancevalue = distance;
                                                            nurse_id = item.id;
                                                            //console.log(nurse_id+" nurse_id");
                                                        }
                                                    });

                                                    $('#nurse_id').val(nurse_id);
                                                    
                                                    //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
                                                    function calcCrow(lat1, lon1, lat2, lon2) 
                                                    {
                                                        var R = 6371; // km
                                                        var dLat = toRad(lat2-lat1);
                                                        var dLon = toRad(lon2-lon1);
                                                        var lat1 = toRad(lat1);
                                                        var lat2 = toRad(lat2);
                                                    
                                                        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                                                            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
                                                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                                                        var d = R * c;
                                                        return d;
                                                    }
                                                
                                                    // Converts numeric degrees to radians
                                                    function toRad(Value) 
                                                    {
                                                        return Value * Math.PI / 180;
                                                    }
                                                });



                                            });
                                        </script>

                                        <section class="intra-product-block-add-ons">
                                            <div class="container py-5">

                                                <div class="row pt-2 pb-2 mb-1">
                                                    <div class="col-12">

                                                        <h2 class="my-4">2. Add-ons</h2>
                                                        <p class="mb-4 pb-3">Upgrade your treatment with an IV booster
                                                            supplement or a COVID test.</p>

                                                        <div class="wc-pao-addons-container">
                                                            <div class="wc-pao-addon-container  wc-pao-addon wc-pao-addon-add-booster"
                                                                data-product-name="Energy Lift">


                                                                <label for="addon-460-add-booster-0">Add-Ons </label>

                                                                <p
                                                                    class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-460-add-booster-0">
                                                                    <select name="addons_id">

                                                                        <option value="">Select</option>
                                                                        @if(!empty($addons))
                                                                        @foreach($addons as $addon)
                                                                        <option value="{{$addon->id}}">{{$addon->name}}
                                                                        </option>
                                                                        @endforeach
                                                                        @endif
                                                                    </select>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="wc-pao-addons-container">
                                                            <div class="wc-pao-addon-container  wc-pao-addon wc-pao-addon-add-test"
                                                                data-product-name="Energy Lift">


                                                                <label for="addon-460-add-test-1"
                                                                    class="wc-pao-addon-name" data-addon-name="Add Test"
                                                                    data-has-per-person-pricing=""
                                                                    data-has-per-block-pricing="">Add Test </label>

                                                                <p
                                                                    class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-460-add-test-1">
                                                                    <select name="test_id">

                                                                        <option value="">Select</option>
                                                                        @if(!empty($tests))
                                                                        @foreach($tests as $test)
                                                                        <option value="{{$test->id}}">{{$test->name}}
                                                                        </option>
                                                                        @endforeach
                                                                        @endif
                                                                    </select>
                                                                </p>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                        </section>
<style>
    .timelight
   {
       background: lightgray;
   }
   .timelight.active
   {
       background: black;;
       color:white;
   }
</style>


                                        <section class="intra-product-block-schedule">
                                            <div class="container py-5">

                                                <div class="row pt-2 pb-2 mb-1">
                                                    <div class="col-12">

                                                        <h2 class="my-4">3. Schedule</h2>


                                                        <div class="mb-5" style="">
                                                            <div id="my-calendar"></div>
                                                        </div>

                                                        <div class="intra-subblock-booking-form">
                                                            <p class="mb-4 pb-3">Select a time for your hour long session.</p>
                                                            <div id="wc-bookings-booking-form"
                                                                class="wc-bookings-booking-form intra-booking-form d-none" style="">
                                                                @for($i=0;$i<=24;$i++)
                                                                    @php $time = date('h:i',strtotime($setting->start_time)+60*60*$i); @endphp 
                                                                    
                                                                    <label for="time" class="timelight px-3 py-2 mx-1" data-id="{{$i}}" style="border-radius:15px;cursor: pointer;">
                                                                        <input class="timeschedule d-none" type="checkbox" name="timeschedule" id="timeschedule_{{$i}}" value="{{ $time }}"> {{ $time }}
                                                                    </label>
                                                                    @php
                                                                        if($time == $setting->end_time){
                                                                            break; 
                                                                        }
                                                                    @endphp
                                                                @endfor
                                                                <p id="timeerrormsg" class="text-danger" style="display:none;font-weight:600:">This schedule Booked already, Please Book Another schedule. </p>
                                                                <div class="form-field form-field-wide">
                                                                    <input type="hidden" class="required_for_calculation" name="schedule" id="wc_bookings_field_start_date">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </section>

                                        <section class="intra-product-block-membership-offer">
                                            <div class="container py-5">

                                                <div class="row pt-2 mb-1">
                                                    <div class="col-12">

                                                        <h2 class="my-4">4. Membership</h2>
                                                        <p class="mb-4 pb-3">Activate a $99 recurring monthly membership
                                                            to
                                                            immediately apply a $129 credit towards this treatment. Save
                                                            23% on
                                                            a treatment each month and cancel any time.</p>



                                                    </div>
                                                </div>


                                                <div class="row ">

                                                    <div class="col-12 d-flex col-sm-6 mb-4 pb-3">

                                                        <div
                                                            class="intra-membership-block w-100 intra-block-spacing py-4 px-4 ">

                                                            <h3 class=" mb-4">Regular price</h3>
                                                            <h2 class="mt-4 mb-4">$129</h2>
                                                            <p class="mb-3 pb-1"></p>
                                                            <a href="#" onclick="return false;"
                                                                class="intra-button w-100 mt-1 border-0">Selected</a>
                                                        </div>

                                                    </div>

                                                    <div class="col-12 col-sm-6 mb-4 pb-3">
                                                        <div
                                                            class="intra-membership-block w-100 py-4 px-4 float-sm-right">

                                                            <h3 class=" mb-4">$99 Membership price</h3>
                                                            <h2 class="mt-4 mb-4">1 Credit</h2>
                                                            <p class="mb-3 pb-1"></p>
                                                            <a href="/product/book-your-first-month"
                                                                id="intra-button-membership-funnel"
                                                                class="intra-button-black w-100 mt-1">Activate
                                                                Membership</a>

                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </section>



                                        <section class="intra-product-block-confirm">
                                            <div class="container py-5">

                                                <div class="row pt-2 pb-2 mb-1">
                                                    <div class="col-12">


                                                        <!-- <input type="hidden" name="add-to-cart" value="460"
                                                            class="wc-booking-product-id"> -->

                                                        <button type="submit"
                                                            class="wc-bookings-booking-form-button single_add_to_cart_button button alt disabled intra-button-black w-100"
                                                            style="">Continue to Cart</button>

                                                    </div>
                                                </div>

                                            </div>
                                        </section>




                                </form>
                            </div>
                        </div>

                    </div>
                </section>
            </form>
        </div>
        <!-- #product-etc -->
</main>


@section('pagescript')

<script
    src="https://maps.google.com/maps/api/js?key={{$googleapi->api_key}}&libraries=places&callback=initMap"
    type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<!-- Without locals-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script src="{{asset('calender/tavo-calendar.js')}}" type="text/javascript"></script>

<script>
const calendar2 = document.querySelector('#my-calendar');

const myCalendar2 = new TavoCalendar(calendar2, {
    //date: "{{date('Y-m-d')}}",
    //selected: ["{{date('Y-m-d')}}"],
    //range_select: true
});
var picdate = myCalendar2.getSelected();
//document.getElementById('calendar_data').innerHTML = picdate;
document.getElementById('wc_bookings_field_start_date').value = picdate;

calendar2.addEventListener('calendar-select', (ev) => {
    var picdate = myCalendar2.getSelected();
    //document.getElementById('calendar_data').innerHTML = picdate;
    document.getElementById('wc_bookings_field_start_date').value = picdate;
    jQuery('#wc-bookings-booking-form').removeClass('d-none');
});
jQuery('.timelight').click(function(){
    var id = jQuery(this).data('id');
    jQuery("input[name='timeschedule']:checked").each(function() {
        jQuery(this).prop("checked", false);
    });
    jQuery('.timelight').removeClass('active');
    jQuery(this).addClass('active');
    document.getElementById("timeschedule_"+id).checked = true;
    var timeschedule = jQuery("input[name='timeschedule']:checked").val();
    var picdate = document.getElementById('wc_bookings_field_start_date').value;
    var nurse_id = jQuery('#nurse_id').val();
    jQuery.ajax({
        type: "get",
        url: "{{url('checknurseassign')}}",
        data:"timeschedule="+timeschedule+'&date='+picdate+'&nurse_id='+nurse_id,
        success: function(data) {
            if(data == 'OK'){
                jQuery('#timeerrormsg').hide();
            }else{
                jQuery('.timelight').removeClass('active');
                jQuery('#timeschedule_'+id).prop("checked", false);
                jQuery('#timeerrormsg').show();
            }
        }
    });
});
</script>
@stop

@endsection