<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title' => 'Products',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Products' => route('products.index')
            ]
        ];
        $products = Product::orderBy('id', 'desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.products.index', compact('meta', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title' => 'Add Product',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add Product' => route('products.create')
            ]
        ];
        $categories = Category::all();
        return view('admin.products.create', compact('meta', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_data = $request->all();
        unset($request_data['media']);
        $product = Product::create($request_data);
        if ($request->media) {
            foreach ($request->media as $image) {
                $file = $this->saveBase64Image('uploads/products/', $image);
                ProductImage::create(['product_id' => $product->id, 'path' => $file]);
            }
        }
        Session::flash('success', 'Product Created.');
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = [
            'title' => 'Edit Product',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit Product' => route('products.edit',$id)
            ]
        ];

        $product = Product::find($id);
        $cate = Category::all();
        return view('admin.products.edit', compact('meta','product', 'cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_data = $request->all();
        unset($request_data['_token']);
        unset($request_data['_method']);
        unset($request_data['media']);
        $product = Product::where('id', $id)->update($request_data);
        if ($request->media) {
            foreach ($request->media as $image) {
                $file = $this->saveBase64Image('uploads/products/', $image);
                ProductImage::create(['product_id' => $id, 'path' => $file]);
            }
        }
        Session::flash('success', 'Product Created.');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if (!empty($product->productImage->count() > 0)) {
            foreach ($product->productImage as $image) {
                try {
                    unlink($image->path);
                } catch (\Exception $e) {
                    // nothing to do
                }
            }
        }
        $product->delete();
        return response()->json([
            "status" => true
        ]);
    }

    /**
     * Change the product status active to deactive vice versa
     * @param int $id
     * @return resonse
     */
    public function changeStatus($id)
    {
        $product = Product::find($id);
        if($product->published == 'Active'){ 
            $product->published = 'Deactive';
        }else{
            $product->published = 'Active';
        }
        $product->save();
        return response()->json([
            "status" => true,
            'value' => $product->published
        ]);
    }

    /**
     * This function is used to delete media
     *
     * @param [int] $id
     * @return void
     */
    public function deletMedia($id)
    {
        $m = ProductImage::find($id);
        try{
            unlink($m->path);
            $m->delete();
        } catch(\Exception $e) {
            // nothing to do
        }

        return response()->json([
            "status"=>true
        ]);
    }
}
