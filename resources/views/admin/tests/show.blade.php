@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('test.index') }}" class="btn btn-danger">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Edit only if you are sure that what it is going to change.</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Slug</th>
                                            <th>Short Description</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        <tr id="record-{{ $test->id }}">
                                            <td>#{{ $test->id }}</td>
                                            <td>{{ $test->name }}</td>
                                            <td>{{ $test->slug }}</td>
                                            <td>{{ $test->short_description }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('test.edit',$test->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <th colspan="7">Description</th>
                                        </tr>
                                        <tr>
                                        <td colspan="7">{!! $test->description !!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row m-4" id="media-preview">
                                    @foreach($test->media as $media)
                                    <div class="col-sm-2" id="stored-image-{{$media->id}}" class="text-center">
                                        <img src="{{url('')}}/{{$media->media}}" class="image-preview">
                                        <a href="javascript:void(0)" onclick="deleteImage({{$media->id}})" class="image-preview-remove">
                                            <i class="fa fa-trash text-danger image-remove-btn"></i>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
<script>
    $('.customSwitches').click(function(){
        var url = $(this).data('url');
        fetch(url, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => response.json()).then( res => {
            if(res.status){
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    });

    function removeImage(id){
        $("#selected-image-"+id).remove();
        selectedImage--;
    }

    function deleteImage(id){
        swal({
            title: "Are you sure?",
            text: "You want to delete this image?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/delete-test-media') }}/"+id, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('stored-image-'+id).remove();
                        
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>
@stop

@endsection