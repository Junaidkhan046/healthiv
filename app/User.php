<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\UserMedia;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address', 'latitude', 'longitude', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function image()
    {
        return $this->hasOne(UserMedia::class,'user_id','id');
    }

    public function document()
    {
        return $this->hasOne(NurseDocument::class, 'user_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class,'user_id');
    }

    public function nurseorders()
    {
        return $this->hasMany(Order::class,'assigned_nurse_id');
    }
}
