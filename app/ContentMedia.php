<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;

class ContentMedia extends Model {

    protected $table = 'content_medias';
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content_id', 'media'];

    public function content()
    {
        return $this->belongsTo(Content::class);
    }
}