<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('customer_name');
            $table->string('customer_email', 100);
            $table->char('customer_phone', 13);
            $table->bigInteger('assigned_nurse_id')->unsigned()->default(NULL);
            $table->enum('tranking_status', ['New Order', 'Assigned', 'On The Way', 'Delivered', 'Cancelled'])->default('New Order');
            $table->integer('delivery_working_day_id')->default(0);
            $table->integer('delivery_time_slot_id')->default(0);
            $table->string('status', 20)->default('Process');
            $table->string('amount', 20)->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
