@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('membership.create') }}" class="btn btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Edit only if you are sure that what it is going to change.</h3>
                                <div class="card-tools">
                                    <?php echo $memberships->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 300px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Period</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($memberships as $membership)
                                        <tr id="record-{{ $membership->id }}">
                                            <td>#{{ $membership->id }}</td>
                                            <td>{{ $membership->name }}</td>
                                            <td>{{ $membership->price }}</td>
                                            <td>{{ $membership->period }}</td>
                                            <td class="text-center">
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input customSwitches" id="customSwitches{{$membership->id}}" data-url="{{url('admin/membership_status/'.$membership->id)}}" @if($membership->status==1) checked @endif>
                                                    <label class="custom-control-label" for="customSwitches{{$membership->id}}"></label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('membership.show',$membership->id) }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                                <a href="{{ route('membership.edit',$membership->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                                <button onclick="deletemembership(<?php echo $membership->id ?>)" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    $('.customSwitches').click(function(){
        var url = $(this).data('url');
        fetch(url, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => response.json()).then( res => {
            if(res.status){
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    });

    function deletemembership(id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this record?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/delete-membership') }}/"+id, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('record-'+id).remove();
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>
@stop

@endsection