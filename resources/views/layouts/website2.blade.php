<!doctype html>
<html lang="en-US">

<head>
    @include('website.partials.head')
</head>

<body class="single single-post single-format-standard theme-intra">
    <div id="page" class="site">
        @include('website.partials.nav')
        <main id="primary" class="site-main file-single intra-blog-post">
        @yield('content')
        </main>
    </div>
    <script src="{{asset('assets/js/navigation.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJcFlSQt_eVhAfXBAKYatsG2H55m8wu_M&#038;libraries=places&#038;ver=5.8"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    @include('website.partials.footer')
</body>

</html>