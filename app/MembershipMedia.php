<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipMedia extends Model
{
    protected $table = "membership_media";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['membership_id', 'media'];

    public function membership()
    {
        return $this->belongsTo(Membership::class);
    }
}
