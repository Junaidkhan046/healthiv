@extends('layouts.website2')

@section('content')
<link rel="stylesheet" href="{{asset('calender/tavo-calendar.css')}}">
<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 300px;
        width: 100%;
    }
    .timelight
    {
        background: lightgray;
    }
    .timelight.active
    {
        background: black;;
        color:white;
    }
</style>
<main id="primary" class="site-main file-woocommerce">
    <div id="product-460" class="product type-product post-460 status-publish first instock product_cat-treatments has-post-thumbnail virtual sold-individually purchasable product-type-booking">
        <!-- Product Hero -->
        <div class="jumbotron all-inclusive mb-0 " style="background: #F59D81;">
            <div class="container mt-5 pt-4 mb-3">
                <div style="clear:both;">
                    <h1 class="product_title entry-title mb-4 pb-2 pt-2">Re Schedule Order</h1>
                    <hr class="my-4">
                    <div class="pt-3">
                        <p>All your health IV questions, answered</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Product Hero -->

        <div class="summary entry-summary">

            <form action="{{url('re_schedule',$order->id)}}" method="post">
            @csrf
                <section class="intra-product-block-title">
                    <div class="container pt-5 pb-3">

                        <div class="row pt-2">
                            <div class="col-12">
                                <div id="map"></div>
                            </div>
                        </div>
                        <div class="row pt-5">
                            <div class="col-12">
                                <div class="mb-5" style="">
                                    <div id="my-calendar"></div>
                                </div>
                            </div>
                        </div>

                        <div class="intra-subblock-booking-form">
                            <p class="mb-4 pb-3">Select a time for your hour long session.</p>
                            <div id="wc-bookings-booking-form"
                                class="wc-bookings-booking-form intra-booking-form" style="">
                                @for($i=0;$i<=24;$i++)
                                    @php $time = date('h:i',strtotime($setting->start_time)+60*60*$i); @endphp 
                                    
                                    <label for="time" class="timelight px-3 py-2 mx-1 @if($order->time == $time) active @endif" data-id="{{$i}}" style="border-radius:15px;cursor: pointer;">
                                        <input class="timeschedule d-none" type="checkbox" name="timeschedule" id="timeschedule_{{$i}}" value="{{ $time }}" @if($order->time == $time) checked @endif> {{ $time }}
                                    </label>
                                    @php
                                        if($time == $setting->end_time){
                                            break; 
                                        }
                                    @endphp
                                @endfor
                                <p id="timeerrormsg" class="text-danger" style="display:none;font-weight:600:">This schedule Booked already, Please Book Another schedule. </p>
                                <div class="form-field form-field-wide">
                                    <input type="hidden" class="required_for_calculation" name="schedule" id="wc_bookings_field_start_date" value="{{$order->date}}">
                                </div>
                            </div>
                        </div>

                        <section class="intra-product-block-confirm">
                            <div class="container py-5">

                                <div class="row pt-2 pb-2 mb-1">
                                    <div class="col-12">
                                        <button type="submit" class="wc-bookings-booking-form-button single_add_to_cart_button button alt disabled intra-button-black w-100"
                                            style="">Submit</button>
                                    </div>
                                </div>

                            </div>
                        </section>

                    </div>
                </section>
            </form>
        </div>
    </div>
</main>



    @section('pagescript')
    
        <script src="https://maps.google.com/maps/api/js?key={{$googleapi->api_key}}&libraries=places&callback=initMap" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                var lat = {{$order->address->lat}};
                var lng = {{$order->address->lng}};

                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: lat,lng:lng},
                    zoom: 15,
                });
                addMaker({lat:lat,lng:lng});
                //add maker function
                function addMaker(coords)
                {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: coords,
                        title:"your Location"
                    });
                }
            });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
        <!-- Without locals-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

        <script src="{{asset('calender/tavo-calendar.js')}}" type="text/javascript"></script>

        <script>
            const calendar2 = document.querySelector('#my-calendar');

            const myCalendar2 = new TavoCalendar(calendar2, {
                date: '{{ $order->date }}',
                selected: ['{{ $order->date }}'],
                //range_select: true
            });
            var picdate = '{{ $order->date }}';
            //document.getElementById('calendar_data').innerHTML = picdate;
            document.getElementById('wc_bookings_field_start_date').value = picdate;

            calendar2.addEventListener('calendar-select', (ev) => {
                var picdate = myCalendar2.getSelected();
                //document.getElementById('calendar_data').innerHTML = picdate;
                document.getElementById('wc_bookings_field_start_date').value = picdate;
                jQuery('#wc-bookings-booking-form').removeClass('d-none');
            });
            jQuery('.timelight').click(function(){
                var id = jQuery(this).data('id');
                jQuery("input[name='timeschedule']:checked").each(function() {
                    jQuery(this).prop("checked", false);
                });
                jQuery('.timelight').removeClass('active');
                jQuery(this).addClass('active');
                document.getElementById("timeschedule_"+id).checked = true;
                var timeschedule = jQuery("input[name='timeschedule']:checked").val();
                var picdate = document.getElementById('wc_bookings_field_start_date').value;
                var nurse_id = '{{$order->assigned_nurse_id}}';
                jQuery.ajax({
                    type: "get",
                    url: "{{url('checknurseassign')}}",
                    data:"timeschedule="+timeschedule+'&date='+picdate+'&nurse_id='+nurse_id,
                    success: function(data) {
                        if(data == 'OK'){
                            jQuery('#timeerrormsg').hide();
                        }else{
                            jQuery('.timelight').removeClass('active');
                            jQuery('#timeschedule_'+id).prop("checked", false);
                            jQuery('#timeerrormsg').show();
                        }
                    }
                });
            });

        </script>
    @stop
@endsection