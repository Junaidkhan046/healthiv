@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('categories.create') }}" class="btn btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-danger">Edit only if you are sure that what it is going to change.</h3>
                                <div class="card-tools">
                                    <?php echo $categories->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 300px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Parent</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $cat)
                                        <tr id="record-{{ $cat->id }}">
                                            <td>#{{ $loop->index+1 }}</td>
                                            <td>{{ $cat->name }}</td>
                                            <td>{{ $cat->parent_id == 0 ? 'Parent' : getCategoryName($cat->parent_id) }}</td>
                                            <td>{!! $cat->description !!}</td>
                                            @php
                                            $image = json_decode($cat->image)
                                            @endphp
                                            <td>
                                                @if(!empty($image))
                                                @foreach($image as $val)
                                                <img src="{{ asset($val) }}" width="100px"/>
                                                @endforeach
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('categories.edit', $cat->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                                <button onclick="deletecat(<?php echo $cat->id ?>)" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    function deletecat(id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this record?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/categories') }}/"+id, {
                    method: "DELETE",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('record-'+id).remove();
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>
@stop

@endsection