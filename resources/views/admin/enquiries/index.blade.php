@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">&nbsp;</div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Enquiries raised by visitors on the website and mobile app.</h3>
                                <div class="card-tools">
                                    <?php echo $enquiries->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 300px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Visitor</th>
                                            <th>Message</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($enquiries as $e)
                                        <tr id="record-{{ $e->id }}">
                                            <td>{{ date('m-d-Y', strtotime($e->created_at)) }}</td>
                                            <td>
                                                <strong>{{ ucwords($e->first_name) }} {{ ucwords($e->last_name) }}</strong><br/>
                                                {{ $e->email }}<br/>
                                                {!! $e->phone != "" ? $e->phone."<br>" : "" !!} {{ $e->zip_code }}
                                            </td>
                                            <td>{{ $e->message }}</td>
                                            <td class="text-center">

                                                <a href="mailto:{{ $e->email }}" class="btn btn-default">
                                                    <i class="fa fa-reply"></i>
                                                </a>

                                                @if($e->status == 1)
                                                <button onclick="changeStatus(<?php echo $e->id ?>, <?php echo $e->status ?>)" class="btn btn-default">
                                                    <i class="fa fa-envelope"></i>
                                                </button>
                                                @endif

                                                @if($e->status == 2)
                                                <button onclick="changeStatus(<?php echo $e->id ?>, <?php echo $e->status ?>)" class="btn btn-default">
                                                    <i class="fa fa-envelope-open"></i>
                                                </button>
                                                @endif

                                                <button onclick="deleteRecord(<?php echo $e->id ?>)" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    function deleteRecord(id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this record?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/delete-enquiry') }}/"+id, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('record-'+id).remove();
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }

    function changeStatus(id, status) {
        swal({
            title: "Are you sure?",
            text: "You want to mark this as "+ (status == 1 ? "Read" : "Unread"),
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/change-status-enquiry') }}/"+id+"/"+status, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        location.reload();
                    }
                });
            }
        });
    }
</script>
@stop

@endsection