@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Edit only if you are sure that what it is going to change.</h3>
                                <div class="card-tools">
                                    <?php echo $orders->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 400px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Amount</th>
                                            <th>Nurse</th>
                                            <th class="text-center">Payment Status</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $order)
                                        <tr id="record-{{ $order->id }}">
                                            <td>#{{ $order->id }}</td>
                                            <td>{{ $order->customer_name }}</td>
                                            <td>{{ $order->customer_phone }}</td>
                                            <td>{{ $order->amount }}</td>
                                            <td>{{ @$order->nurse->name }}</td>
                                            <td class="text-center">
                                                {{ $order->status }}
                                            </td>
                                            <td class="text-center">
                                                {{ $order->tranking_status }}
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ url('admin/order/show/'.$order->id) }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                                @if($order->status=='Success')
                                                    <a class="btn btn-info assignnursebtn" data-href="{{ url('admin/order/assign/'.$order->id) }}" data-toggle="modal" data-target="#NurseModal"><i class="fa fa-key"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

<!-- The Modal -->
        <div class="modal fade" id="NurseModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header text-white" style="background-color: #007bff">
                        <h4 class="modal-title">Assign Nurse
                        </h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">
                                X
                            </button>
                    </div>

                    <!-- Modal body -->
                    <form id="formID" method="post" action="">
                        {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" >Nurse</label>
                            <div class="col-md-9">
                                <select class="form-control" name="nurse_id" required>
                                    @if(!empty($nurses))
                                        @foreach($nurses as $nurse)
                                            <option value="{{$nurse->id}}">{{$nurse->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success" name="submit">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    $('.assignnursebtn').click(function(){
        $('#formID').attr('action',$(this).data('href'));
    });
</script>
@stop

@endsection