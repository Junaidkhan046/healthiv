jQuery(document).ready(function( $ ) {

    // console.log( "ayyy" );

    // Navbar coloring

    // check on page-load
    var $nav = jQuery(".navbar.fixed-top");
    $nav.toggleClass('scrolled', jQuery(this).scrollTop() > $nav.height());

    // check on scroll
    jQuery(document).scroll(function () {
    	var $nav = jQuery(".navbar.fixed-top");
    	$nav.toggleClass('scrolled', jQuery(this).scrollTop() > $nav.height());
    });




    // Anchor smooth scroll
	$('a[href*=#]:not([href=#])').click(function() {
		// console.log('');
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top-240
				}, 400);
				return false;
			}
		}
	});

	$('.intra-onclick-show-login-form').on('click',function(){
		$('.intra-shop-login').fadeToggle( "slow" );
		return false;
	});


	$('.woocommerce-info .close-button').on('click', function(){
		$('.woocommerce-info').fadeOut(function(){$(this).remove();});
		$('.woocommerce-notices-wrapper').css({ 'padding-bottom' : 0 });
	});



	// Disable enter from submitting form on product page
	$('.product form.cart').on('keyup keypress', function(e) {
	    var keyCode = e.keyCode || e.which;
	    if (keyCode === 13) { 
	        e.preventDefault();
	        return false;
	    }
	});

	

});