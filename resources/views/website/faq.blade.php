@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<style>
      @media screen and (max-width: 767px) {
        .faqcontent {
          max-width:90%;
          margin:0 auto;
        }
      }
      @media screen and (min-width: 1200px) {
        .faqcontent {
            max-width:75%;
            margin:0 auto;
        }
      }
    </style>
<main id="primary" class="site-main file-woocommerce">
		
					
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">


	<!-- Product Hero -->
	<div class="jumbotron book-your-first-month mb-0 ">
		<div class="container mt-5 pt-4 mb-3">

			<div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
	<figure class="woocommerce-product-gallery__wrapper">
		<div data-thumb="{{asset('img/faq.png')}}" data-thumb-alt="" class="woocommerce-product-gallery__image">
        <a href="{{asset('img/faq.png')}}">
        <img style="max-width: 110px;" src="{{asset('img/faq.png')}}" class="wp-post-image" alt="">
    </a></div>	</figure>
</div>


			<div style="clear:both;">
				<h1 class="product_title entry-title mt-4 mb-4 pb-2 pt-4">FAQ</h1>
				
				<hr class="my-4">

				<div class="pt-3">

					<p>All your health IV questions, answered</p>

				</div>
			</div>



		</div>
	</div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">


    	<section class="intra-product-block-title">
	    	<div class="faqcontent py-5 my-4" style="">

	    		<div class="border-top justify-content-center">
	    			<div class="pt-4">
                        @if(!empty($faqs))
                            @foreach($faqs as $faq)
                                <div class="py-3 text-info" data-toggle="collapse" data-target="#demo_{{$faq->id}}"><i class="fa fa-check mr-2"></i>{{$faq->question}} 
                                    <span class="close"><i class="text-info fa fa-chevron-down" style="font-size:18px"></i></span>
                                
                                    <div id="demo_{{$faq->id}}" class="collapse mt-3">
                                        <div class="border-top">
                                            <div class="pl-3">
                                                <p>{!!$faq->answer!!} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div>
        </section>
    </div>


		
				</main>
                @endsection