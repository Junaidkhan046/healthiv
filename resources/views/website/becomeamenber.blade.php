@extends('layouts.website2')

@section('content')
<link rel="stylesheet" href="{{asset('calender/tavo-calendar.css')}}">
<main id="primary" class="site-main file-woocommerce">


    <div id="product-34"
        class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">


        <!-- Product Hero -->
        <div class="jumbotron book-your-first-month mb-0 ">
            <div class="container mt-5 pt-4 mb-3">

                <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images"
                    data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                    <figure class="woocommerce-product-gallery__wrapper">
                        <div data-thumb="https://healthiv.com/wp-content/uploads/2021/06/all_inclusive.svg"
                            data-thumb-alt="" class="woocommerce-product-gallery__image">
                            <a href="https://healthiv.com/wp-content/uploads/2021/06/all_inclusive.svg">
                                <img style="max-width: 110px;"
                                    src="https://healthiv.com/wp-content/uploads/2021/06/all_inclusive.svg"
                                    class="wp-post-image" alt="">
                            </a>
                        </div>
                    </figure>
                </div>


                <div style="clear:both;">
                    <h1 class="product_title entry-title mt-4 mb-4 pb-2 pt-4">Become a member and book your first month
                    </h1>
                    <h2 class="mt-4 mb-4 pt-2 pb-3 price">
                        $99
                    </h2>

                    <hr class="my-4">

                    <div class="pt-3">

                        <p>Become a member and get 23% off a treatment each month.</p>

                    </div>
                </div>



            </div>
        </div>
        <!-- END Product Hero -->



        <div class="summary entry-summary">


            <section class="intra-product-block-title">
                <div class="container pt-5 pb-3">

                    <div class="row pt-2">
                        <div class="col-12">

                            <h2 class="my-4">Fill out the forms below to book your session</h2>


                            <div class="woocommerce-notices-wrapper"></div>
                            <noscript>Your browser must support JavaScript in order to make a booking.</noscript>

                            <form class="cart" method="post" enctype="multipart/form-data" data-nonce="3faf32bfa9">



                            <section class="intra-product-block-location">
                                        <div class="container py-5">

                                            <div class="row pt-2 pb-2 mb-1">
                                                <div class="col-12">

                                                    <h2 class="my-4">1. Location</h2>
                                                    <p class="mb-4 pb-3">We come to you. Let’s make sure you’re in our
                                                        service area.</p>

                                                    <div class="form-group mb-3 pb-1">

                                                        <div class="wc-pao-addons-container">
                                                            <div class="wc-pao-addon-container wc-pao-required-addon wc-pao-addon wc-pao-addon-service-location"
                                                                data-product-name="Energy Lift">


                                                                <label for="addon-460-service-location-2">Service
                                                                    Location <em class="required"
                                                                        title="Required field">*</em>&nbsp;</label>


                                                                <p
                                                                    class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-460-service-location-2">
                                                                    <input type="text" name="location" required
                                                                        class="autocomplete_address input-text wc-pao-addon-field wc-pao-addon-custom-text pac-target-input"
                                                                        placeholder="Enter a location"
                                                                        autocomplete="off">
                                                                    <input type="hidden" name="country" id="country">
                                                                    <input type="hidden" name="state" id="state">
                                                                    <input type="hidden" name="city" id="city">
                                                                    <input type="hidden" name="zipcode" id="zipcode">
                                                                    <input type="hidden" name="latitude" id="latitude">
                                                                    <input type="hidden" name="longitute"
                                                                        id="longitute">

                                                                    @if(Session::has('address'))
                                                                <p class="text-danger">
                                                                    <b>{{ Session::get('address') }}</b>
                                                                <p>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                            <div id="ShowMap">
                                                                <div id="map" style="width:100%;height:300px"></div>
                                                            </div>
                                                        </div>
                                                        <div id="in_range_wrapper" style="display:none;">
                                                            <span class="">Great News! You're in range of one of our
                                                                locations. <br>Please book your session below.</span>

                                                        </div>

                                                        <div id="out_of_range_wrapper" style="display:none;">
                                                            <span>We apologize, you are not close enough to any of our
                                                                locations.</span>
                                                        </div>

                                                        <div id="fail_wrapper" style="display:none;">
                                                            <span>We can't find that, please try entering a different
                                                                address.</span>
                                                        </div>





                                                        <div class="wc-pao-addons-container">
                                                            <div class="wc-pao-addon-container  wc-pao-addon wc-pao-addon-healthivbranchzone"
                                                                data-product-name="Energy Lift">


                                                                <label class="wc-pao-addon-name"
                                                                    data-addon-name="HealthIVBranchZone"
                                                                    data-has-per-person-pricing=""
                                                                    data-has-per-block-pricing=""
                                                                    style="display:none;"></label>


                                                                <p
                                                                    class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-460-healthivbranchzone-3">
                                                                    <input type="text"
                                                                        class="input-text wc-pao-addon-field wc-pao-addon-custom-text"
                                                                        data-raw-price="" data-price=""
                                                                        name="addon-460-healthivbranchzone-3"
                                                                        id="addon-460-healthivbranchzone-3"
                                                                        data-price-type="flat_fee" value="">
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                    </section>


                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    document.getElementById("ShowMap").style.display = "none";
                                    //console.log("Google API related custom JS is loaded");

                                    var intra_location_input = document.querySelector(
                                        ".autocomplete_address");

                                    // create new autocomplete object from Maps API using our input
                                    var autocomplete = new google.maps.places.Autocomplete(
                                        intra_location_input);

                                    // when new places are selected, process the results
                                    autocomplete.addListener("place_changed", function() {
                                        var place = autocomplete.getPlace();
                                        document.getElementById("ShowMap").style.display =
                                            "block";
                                        //console.log(place);
                                        var lat = place.geometry['location'].lat();
                                        var lng = place.geometry['location'].lng();
                                        $('#latitude').val(lat);
                                        $('#longitute').val(lng);

                                        var map = new google.maps.Map(document.getElementById(
                                            'map'), {
                                            center: {
                                                lat: lat,
                                                lng: lng
                                            },
                                            zoom: 15,
                                        });
                                        addMaker({
                                            lat: lat,
                                            lng: lng
                                        });
                                        //add maker function
                                        function addMaker(coords) {
                                            var marker = new google.maps.Marker({
                                                map: map,
                                                position: coords,
                                                title: "your Location"
                                            });
                                        }

                                        for (var i = 0; i < place.address_components
                                            .length; i++) {
                                            for (var j = 0; j < place.address_components[i]
                                                .types.length; j++) {
                                                if (place.address_components[i].types[j] ==
                                                    "postal_code") {
                                                    $('#zipcode').val(place.address_components[
                                                        i].long_name);
                                                }
                                                if (place.address_components[i].types[j] ==
                                                    "country") {
                                                    $('#country').val(place.address_components[
                                                        i].long_name);
                                                }
                                                if (place.address_components[i].types[j] ==
                                                    "administrative_area_level_1") {
                                                    $('#state').val(place.address_components[i]
                                                        .long_name);
                                                }

                                                if (place.address_components[i].types[j] ==
                                                    "administrative_area_level_2") {
                                                    $('#city').val(place.address_components[i]
                                                        .long_name);
                                                }
                                            }
                                        }
                                    });



                                });
                                </script>


                                <section class="intra-product-block-add-ons">
                                    <div class="container py-5">

                                        <div class="row pt-2 pb-2 mb-1">
                                            <div class="col-12">

                                                <h2 class="my-4">2. Treatment</h2>
                                                <p class="mb-4 pb-3"></p>

                                                <div class="form-group mb-3 pb-1">
                                                    <div class="wc-pao-addons-container">
                                                        <div class="wc-pao-addon-container wc-pao-required-addon wc-pao-addon wc-pao-addon-first-months-treatment"
                                                            data-product-name="Become a member and book your first month">


                                                            <label for="addon-34-first-months-treatment-4"
                                                                class="wc-pao-addon-name"
                                                                data-addon-name="First Month’s Treatment"
                                                                data-has-per-person-pricing=""
                                                                data-has-per-block-pricing="">First Month’s Treatment
                                                                <em class="required"
                                                                    title="Required field">*</em>&nbsp;</label>

                                                            <p
                                                                class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-34-first-months-treatment-4">
                                                                <select class="wc-pao-addon-field wc-pao-addon-select"
                                                                    name="addon-34-first-months-treatment-4"
                                                                    id="addon-34-first-months-treatment-4" required="">

                                                                    <option value="">Select an option...</option>

                                                                    <option data-raw-price="" data-price=""
                                                                        data-price-type="flat_fee"
                                                                        value="all-inclusive-1"
                                                                        data-label="All Inclusive">All Inclusive
                                                                    </option>
                                                                    @if(!empty($products))
                                                                    @foreach($products as $product)
                                                                    <option value="{{$product->id}}">{{$product->name}}
                                                                    </option>
                                                                    @endforeach
                                                                    @endif

                                                                </select>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </section>


                                <section class="intra-product-block-add-ons">
                                    <div class="container py-5">

                                        <div class="row pt-2 pb-2 mb-1">
                                            <div class="col-12">

                                                <h2 class="my-4">3. Add-ons</h2>
                                                <p class="mb-4 pb-3">Upgrade your treatment with an IV booster
                                                    supplement or a COVID test.</p>

                                                <div class="wc-pao-addons-container">
                                                    <div class="wc-pao-addon-container  wc-pao-addon wc-pao-addon-add-booster"
                                                        data-product-name="Become a member and book your first month">


                                                        <label for="addon-34-add-booster-0" class="wc-pao-addon-name"
                                                            data-addon-name="Add Booster" data-has-per-person-pricing=""
                                                            data-has-per-block-pricing="">Add Booster </label>

                                                        <p
                                                            class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-34-add-booster-0">
                                                            <select class="wc-pao-addon-field wc-pao-addon-select"
                                                                name="addons">

                                                                <option value="">Select</option>
                                                                @if(!empty($addons))
                                                                @foreach($addons as $addon)
                                                                <option value="{{$addon->id}}">{{$addon->name}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="wc-pao-addons-container">
                                                    <div class="wc-pao-addon-container  wc-pao-addon wc-pao-addon-add-test"
                                                        data-product-name="Become a member and book your first month">


                                                        <label for="addon-34-add-test-1" class="wc-pao-addon-name"
                                                            data-addon-name="Add Test" data-has-per-person-pricing=""
                                                            data-has-per-block-pricing="">Add Test </label>

                                                        <p
                                                            class="form-row form-row-wide wc-pao-addon-wrap wc-pao-addon-34-add-test-1">
                                                            <select class="wc-pao-addon-field wc-pao-addon-select"
                                                                name="addon-460-add-test-1" id="addon-460-add-test-1">

                                                                <option value="">Select</option>
                                                                @if(!empty($tests))
                                                                @foreach($tests as $test)
                                                                <option value="{{$test->id}}">{{$test->name}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </p>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </section>



                                <section class="intra-product-block-schedule">
                                    <div class="container py-5">

                                        <div class="row pt-2 pb-2 mb-1">
                                            <div class="col-12">

                                                <h2 class="my-4">4. Schedule</h2>


                                                <div class="mb-5" style="">
                                                    <div id="my-calendar"></div>
                                                </div>

                                                <div class="intra-subblock-booking-form">
                                                    <p class="mb-4 pb-3">Select a time for your hour long session.</p>

                                                    <div id="wc-bookings-booking-form"
                                                        class="wc-bookings-booking-form intra-booking-form" style="">

                                                        <!--<div class="wrapper bg-light" style="border:1px solid lightgray;">
                                                                <div class="py-3 px-5" id="calendar_data">Please Select a Date</div>
                                                            </div> -->
                                                        <div class="form-field form-field-wide">
                                                            <input type="hidden" class="required_for_calculation"
                                                                name="schedule" id="wc_bookings_field_start_date">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </section>

                                <section class="intra-product-block-confirm">
                                    <div class="container py-5">

                                        <div class="row pt-2 pb-2 mb-1">
                                            <div class="col-12">


                                                <input type="hidden" name="add-to-cart" value="34"
                                                    class="wc-booking-product-id">

                                                <button type="submit"
                                                    class="wc-bookings-booking-form-button single_add_to_cart_button button alt disabled intra-button-black w-100"
                                                    style="">Continue to Cart</button>

                                            </div>
                                        </div>

                                    </div>
                                </section>



                            </form>



                        </div>
                    </div>

                </div>
            </section>


            <p class="first-payment-date"><small></small></p>

        </div>
        <!-- .summary -->





    </div>
    <!-- #product-etc -->



</main>


@section('pagescript')
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&libraries=places&callback=initMap" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<!-- Without locals-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('calender/tavo-calendar.js')}}" type="text/javascript"></script>

<script>
const calendar2 = document.querySelector('#my-calendar');

const myCalendar2 = new TavoCalendar(calendar2, {
    date: "{{date('Y-m-d')}}",
    selected: ["{{date('Y-m-d')}}"],
    //range_select: true
});
var picdate = myCalendar2.getSelected();
//document.getElementById('calendar_data').innerHTML = picdate;
document.getElementById('wc_bookings_field_start_date').value = picdate;

calendar2.addEventListener('calendar-select', (ev) => {
    var picdate = myCalendar2.getSelected();
    //document.getElementById('calendar_data').innerHTML = picdate;
    document.getElementById('wc_bookings_field_start_date').value = picdate;
});
</script>
@stop
@endsection