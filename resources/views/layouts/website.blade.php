<!doctype html>
<html lang="en-US">

<head>
    @include('website.partials.head')
</head>

<body class="page theme-intra intra-slug-home">
    <div id="page" class="site">
        @include('website.partials.nav')
        <main id="primary" class="site-main file-page-home">
        @yield('content')
        </main>
    </div>
    <script src='assets/js/navigation.js'></script>
    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDJcFlSQt_eVhAfXBAKYatsG2H55m8wu_M&#038;libraries=places&#038;ver=5.8'></script>
    <script src='assets/js/bootstrap.min.js'></script>
    @include('website.partials.footer')
</body>

</html>