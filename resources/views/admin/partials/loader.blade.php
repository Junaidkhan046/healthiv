<!-- Preloader -->
<div class="preloader flex-column justify-content-center align-items-center" style="background:#f5f4e9 !important;">
    <img class="animation__shake" src="{{asset('/')}}admin/dist/img/loader-01.gif" alt="HealthIV">
</div>