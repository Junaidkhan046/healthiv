<?php

namespace App\Http\Controllers;

use App\NurseDocument;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserManagementConroller extends Controller
{

    /**
     * This function is used to get the customer user list
     *
     * @return void
     */
    public function getCustomer(){

        $meta = [
            'title' => 'Customer',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Customer' => route('customer')
            ]
        ];
        $customers = User::role('user')->orderBy('id', 'desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.user.customer', compact('meta', 'customers'));
    }

    /**
     * This function is used to edit the customer
     * 
     * @param of customer id
     * @return view
     */
    public function editCustomer($id){
        $meta = [
            'title' => 'Edit Customer',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Customer' => route('customer')
            ]
        ];
        $customer = User::role('user')->find($id);
        return view('admin.user.customer_edit', compact('meta', 'customer'));
    }

    /**
     * This function is used to update the customer
     * @param of array
     * @return success
     */
    public function updateCustomer(Request $request){
        $validator =  $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->save();
        return redirect()->route('customer');
    }

     /**
     * This function is used to get the customer user list
     *
     * @return void
     */
    public function getManager(){

        $meta = [
            'title' => 'Manager',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Manager' => route('manager')
            ]
        ];
        $customers = User::role('admin')->orderBy('id', 'desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.user.managers', compact('meta', 'customers'));
    }

    /**
     * This function is used to edit the manager
     * 
     * @param of manager id
     * @return view
     */
    public function editManager($id){
        $meta = [
            'title' => 'Edit Manager',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Manager' => route('manager')
            ]
        ];
        $manager = User::role('admin')->find($id);
        return view('admin.user.manager_edit', compact('meta', 'manager'));
    }

    /**
     * This function is used to update the manager
     * @param of array
     * @return success
     */
    public function updateManager(Request $request){
        $validator =  $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->save();
        return redirect()->route('manager');
    }

    /**
     * This function is used to get the customer user list
     *
     * @return void
     */
    public function getNurse(){

        $meta = [
            'title' => 'Nurse',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Nurse' => route('nurse')
            ]
        ];
        $customers = User::role('nurse')->orderBy('id', 'desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.user.nurse', compact('meta', 'customers'));
    }


    /**
     * This function is used to edit the nurse
     * 
     * @param of nurse id
     * @return view
     */
    public function editNurse($id){
        $meta = [
            'title' => 'Edit Nurse',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Manager' => route('nurse')
            ]
        ];
        $nurse = User::role('nurse')->find($id);
        return view('admin.user.nurse_edit', compact('meta', 'nurse'));
    }

    /**
     * This function is used to update the manager
     * @param of array
     * @return success
     */
    public function updateNurse(Request $request){
        $validator =  $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->save();
        return redirect()->route('nurse');
    }

    /**
     * Change the user status active to deactive vice versa
     * @param int $id
     * @return resonse
     */
    public function changeStatus($id)
    {
        $user = User::find($id);
        if($user->active){ 
            $user->active = '0';
        }else{
            $user->active = '1';
        }
        $user->save();
        
        return response()->json([
            "status" => true,
            'value' => active($user->active)
        ]);
    }

    public function changeDocumentStatus($id)
    {
        $user = NurseDocument::find($id);
        if($user->status){ 
            $user->status = '0';
        }else{
            $user->status = '1';
        }
        $user->save();
        
        return response()->json([
            "status" => true,
            'value' => active($user->status)
        ]);
    }

    /**
     * This function is used to display create Manager role user
     */
    public function createManager(){
        $meta = [
            'title' => 'Add Manager',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Manager' => route('manager')
            ]
        ];
        return view('admin.user.add_manager', compact('meta'));
    }
    
    public function storeManager(Request $request)
    {
        $validator =  $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $user->assignRole(2);
        return redirect()->route('manager');
    }
}
