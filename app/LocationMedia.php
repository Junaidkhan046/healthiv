<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;

class LocationMedia extends Model {

    protected $table = "location_medias";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['location_id', 'media'];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}