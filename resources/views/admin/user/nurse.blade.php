@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-danger">Edit only if you are sure that what it is going to change.</h3>
                                <div class="card-tools">
                                    <?php echo $customers->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 300px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Document</th>
                                            <th>Document Verified</th>
                                            <th>Active</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($customers as $user)
                                        <tr id="record-{{ $user->id }}">
                                            <td>#{{ $loop->index+1 }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if(!empty($user->document))
                                                @foreach(json_decode($user->document->document) as $val)
                                                <a href="{{asset($val)}}" target="_blank"> <img src="{{asset($val)}}" width="100" height="100" /> </a>
                                                @endforeach
                                                @endif
                                            </td>
                                            <td>@if(!empty($user->document))
                                                <span id="doc_{{$user->document->id}}" onclick="changeVerificationStatus('{{$user->document->id}}')">
                                            
                                            {{ active($user->document->status) }}
                                            
                                          </span>
                                          @endif</td>

                                            <td><span id="status_{{$user->id}}" onclick="changeStatus('{{$user->id}}')">{{ active($user->active) }}</span></td>

                                            <td>
                                                <a href="{{ url('admin/nurse/edit') }}/{{ $user->id }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    function changeStatus(id) {
        fetch("{{ url('admin/change-user-status') }}/" + id, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        }).then(response => response.json()).then(res => {
            if (res.status) {
                $('#status_' + id).html(res.value);
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    }

    function changeVerificationStatus(id) {
        fetch("{{ url('admin/change-user-document-status') }}/" + id, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        }).then(response => response.json()).then(res => {
            if (res.status) {
                $('#doc_' + id).html(res.value);
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    }
</script>
@stop

@endsection