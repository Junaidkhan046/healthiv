<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventMedia;
use Session;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $meta = [
            'title' => 'Events',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Events' => route('admin-events')
            ]
        ];

        $events = Event::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.events.index', compact('meta','events'));
    }

    public function add()
    {
        $meta = [
            'title' => 'Add Event',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add Event' => route('admin-add-event')
            ]
        ]; 

        return view('admin.events.add', compact('meta'));
    }

    public function create(Request $request)
    {
        $event = Event::create([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'slug' => $this->getSlug($request->title),
            'description' => $request->description
        ]);

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/events/', $image);
                $media = EventMedia::create([
                    'event_id' => $event->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Event Created.'); 
        return redirect()->route('admin-events');
    }

    public function edit($id)
    {
        $meta = [
            'title' => 'Edit Event',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit Event' => route('admin-edit-event',$id)
            ]
        ];

        $event = Event::find($id);

        return view('admin.events.edit', compact('meta','event'));
    }

    public function update(Request $request)
    {
        $event = Event::find($request->id);

        $event->title = $request->title;
        $event->slug = $this->getSlug($request->title);
        $event->description = $request->description;
        

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/events/', $image);
                $media = EventMedia::create([
                    'event_id' => $event->id,
                    'media' => $file,
                ]);
            }
        }

        $event->save();

        Session::flash('success', 'Event Updated.'); 
        return redirect()->route('admin-events');
    }

    public function delete($id)
    {
        $c = Event::find($id);

        foreach($c->media as $m){
            try{
                unlink($m->media);
            } catch(\Exception $e) {
                // nothing to do
            }
            EventMedia::find( $m->id )->delete();
        }

        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function deletMedia($id)
    {
        $m = EventMedia::find($id);

        try{
            unlink($m->media);
        } catch(\Exception $e) {
            // nothing to do
        }
        
        $m->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
