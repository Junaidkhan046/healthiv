<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FAQ;
use Session;

class FAQController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $meta = [
            'title' => 'FAQ',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'FAQ' => route('admin-faq')
            ]
        ];

        $faq = FAQ::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.faq.index', compact('meta','faq'));
    }

    public function add()
    {
        $meta = [
            'title' => 'Add FAQ',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add FAQ' => route('admin-add-faq')
            ]
        ]; 

        return view('admin.faq.add', compact('meta'));
    }

    public function create(Request $request)
    {
        $faq = FAQ::create([
            'question' => $request->question,
            'slug' => $this->getSlug($request->question),
            'answer' => $request->answer
        ]);

        Session::flash('success', 'FAQ Created.'); 
        return redirect()->route('admin-faq');
    }

    public function edit($id)
    {
        $meta = [
            'title' => 'Edit FAQ',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit FAQ' => route('admin-edit-faq',$id)
            ]
        ];

        $faq = FAQ::find($id);

        return view('admin.faq.edit', compact('meta','faq'));
    }

    public function update(Request $request)
    {
        $faq = FAQ::find($request->id);

        $faq->question = $request->question;
        $faq->slug = $this->getSlug($request->question);
        $faq->answer = $request->answer;

        $faq->save();

        Session::flash('success', 'FAQ Updated.'); 
        return redirect()->route('admin-faq');
    }

    public function delete($id)
    {
        $c = FAQ::find($id)->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
