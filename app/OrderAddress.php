<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address_line', 'zipcode', 'city', 'state', 'country', 'lat', 'lng', 'comment', 'order_id'];
}
