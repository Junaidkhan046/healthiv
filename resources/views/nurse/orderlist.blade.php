@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">

                <!-- Product Hero -->
                <div class="jumbotron book-your-first-month mb-0 ">
                    <div class="container mt-5 pt-4 mb-3">
                        <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                            <figure class="woocommerce-product-gallery__wrapper">
                                <div data-thumb="https://static.thenounproject.com/png/1328079-200.png" data-thumb-alt="" class="woocommerce-product-gallery__image">
                                    <a href="https://static.thenounproject.com/png/1328079-200.png">
                                        <img style="max-width: 100px;" src="https://static.thenounproject.com/png/1328079-200.png" class="wp-post-image" alt="">
                                    </a>
                                </div>	
                            </figure>
                        </div>

                        <div style="clear:both;">
                            <h1 class="product_title entry-title mb-4 pb-2 pt-2">My Orders</h1>
                            
                            <hr class="my-4">

                            <div class="pt-3">

                                <p>My All Product List</p>

                            </div>
                        </div>

                    </div>
                </div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">
    	<section class="intra-product-block-title">
	    	<div class="container my-5" style="">

	    		<div class="justify-content-center">
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="d-flex" style="list-style:none;">
                            <li class="{{@$_REQUEST['status']== '' ? '' : 'bg-info'}} px-3 py-1 text-center border">
                                <a href="{{url('orders')}}?status=" class="text-decoration-none">{{Auth::user()->nurseorders->count()}} Total Order</a>
                            </li>
                            <li class="{{@$_REQUEST['status']== 'Assigned' ? '' : 'bg-warning'}}  px-3 py-1 mx-3 text-center border">
                                <a href="{{url('orders')}}?status=Assigned" class="text-decoration-none">{{Auth::user()->nurseorders->where('tranking_status','Assigned')->count()}} Assigned</a>
                            </li>
                            <li class="{{@$_REQUEST['status']== 'On The Way' ? '' : 'bg-primary'}}  px-3 py-1 text-center border">
                                <a href="{{url('orders')}}?status=On The Way" class="text-decoration-none">{{Auth::user()->nurseorders->where('tranking_status','On The Way')->count()}} On The Way</a>
                            </li>
                            <li class="{{@$_REQUEST['status']== 'Delivered' ? '' : 'bg-success'}}  px-3 py-1 mx-3 text-center border">
                                <a href="{{url('orders')}}?status=Delivered" class="text-decoration-none">{{Auth::user()->nurseorders->where('tranking_status','Delivered')->count()}} Delivered</a>
                            </li>
                            <li class="{{@$_REQUEST['status']== 'Cancelled' ? '' : 'bg-danger'}}  px-3 py-1 text-center border">
                                <a href="{{url('orders')}}?status=Cancelled" class="text-decoration-none text-dark">{{Auth::user()->nurseorders->where('tranking_status','Cancelled')->count()}} Cancelled</a>
                            </li>
                        </ul>
                        <div class="m-0 table-responsive p-0" style="height: 500px;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Time</th>
                                        <th>Date</th>
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <th>Amount</th>
                                        <th>Order Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($orders))
                                        @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->id}}</td>
                                                <td>{{$order->time}}</td>
                                                <td>{{date('d-m-Y',strtotime($order->date))}}</td>
                                                <td>{{@$order->user->name}}</td>
                                                <td>{{@$order->address->address_line}}</td>
                                                <td>${{@$order->amount}}</td>
                                                <td style="width:170px">
                                                    <select class="form-control" class="status_change" id="{{$order->id}}">
                                                        <option {{$order->tranking_status == 'New Order' ? 'selected' : '' }}" >New Order</option>
                                                        <option {{$order->tranking_status == 'Assign' ? 'selected' : '' }}" >Assign</option>
                                                        <option {{$order->tranking_status == 'On The Way' ? 'selected' : '' }}" >On The Way</option>
                                                        <option {{$order->tranking_status == 'Delay' ? 'selected' : '' }}" >Delay</option>
                                                        <option {{$order->tranking_status == 'Complete' ? 'selected' : '' }}" >Complete</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info btn-sm" href="{{url('order/details/'.$order->id)}}" title="View Details"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                            <!-- /.card-body -->
                </div>
            </div>
        </section>
    </div>


	@csrf	
</main>
@section('pagescript')
    @if(\Session::has('success'))
    <script>
        swal("Success!", "{{ Session::get('success') }}", "success");
    </script>
    @endif

    <script>
        alert("asdfgh");
    </script>
@stop
@endsection