<header id="masthead" class="site-header">
    <nav id="site-navigation" class="navbar navbar-dark navbar-expand-md fixed-top main-navigation">
        <a class="navbar-brand" href="/">
            <h3 class="intra-logo-header">Health<span class="intra-logo-header-iv">IV</span></h3>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/') }}"><span>Home</span></a>
                    </li>
                @endauth
                <li class="nav-item {{url()->current() == route('landing') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('landing') }}">
                        <span>Treatments</span>
                    </a>
                </li>
                <li class="nav-item {{url()->current() == route('blog') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('blog') }}">
                        <span>Blog</span>
                    </a>
                </li>
                <li class="nav-item {{url()->current() == route('event') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('event') }}">
                        <span>Event</span>
                    </a>
                </li>
                <li class="nav-item {{url()->current() == route('about') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('about') }}">
                        <span>About</span>
                    </a>
                </li>
                <li class="nav-item {{url()->current() == route('contact') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('contact') }}">
                        <span>Contact</span>
                    </a>
                </li>
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item dropdown dropleft">
                            <a href="#" class="nav-link dropdown-toggle pointer" data-toggle="dropdown"> {{ Auth::user()->name }}</a>
                            <div class="dropdown-menu">
                                    @if(Auth::user()->hasRole('nurse'))
                                    <a class="dropdown-item border-bottom pb-2" href="{{url('nurse/home')}}">
                                        Dashboard
                                    </a>
                                    @else
                                    <a class="dropdown-item border-bottom pb-2" href="{{url('home')}}">
                                        Dashboard
                                    </a>
                                    @endif
                                    <a class="dropdown-item border-bottom pb-2" href="{{url('profile')}}">
                                        My Profile
                                    </a>
                                    @if(Auth::user()->hasRole('nurse'))
                                        <a class="dropdown-item border-bottom pb-2" href="{{url('nurse/orders')}}">
                                        My Orders
                                    </a>
                                    @else
                                        <a class="dropdown-item border-bottom pb-2" href="{{url('orders')}}">
                                            My Orders
                                        </a>
                                    @endif
                                    <a class="dropdown-item py-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    </a>
                            </div>
                        </li>
                    @else
                        <li class="nav-item {{url()->current() == route('login') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('login') }}"><span>Login</span></a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item {{url()->current() == route('register') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('register') }}"><span>Register</span></a>
                        </li>
                        @endif
                    @endauth
                @endif
            </ul>
        </div>
    </nav>
</header>