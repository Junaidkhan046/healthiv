<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('sku')->unique();
            $table->string('name', 100)->nullable();
            $table->mediumText('description')->nullable();
            $table->char('selling_price', 10)->nullable();
            $table->char('purchasing_price', 10)->nullable();
            $table->string('slug', 191)->nullable();
            $table->enum('published', ['Active', 'Deactive'])->defualt('Deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
