@extends('layouts.website2')

@section('content')
<style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 500px;
        width: 100%;
       }
    </style>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34 mt-5" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">

                
	    </div> 

    <div class="summary entry-summary mt-4 pt-5">


    	<section class="intra-product-block-title">
	    	<div class="container-fluid my-5" style="">

	    		<div class="justify-content-center">
                    <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                
                                <div class="pull-left">
                                  <p> Nurse &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;<b>{{@$order->nurse->name}}</b> 
                                    <br> Customer&nbsp;&nbsp; - &nbsp;&nbsp;<b>{{@$order->customer_name}}</b>
                                    <br> Distance &nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;<b><span id="distance"></span> KM</b>
                                    <br> <a href="https://www.google.com/maps/@{{@$order->address->lat}},{{@$order->address->lng}},12z/{{@$order->address->address}}?hl=mr-IN" target="_blank"></a>
                                  </p>
                                
                                </div>

                                <div class="pull-right">
                                    <a href="{{url('order/details/'.$order->id)}}" class="btn btn-warning text-decoration-none px-5"> Back </a>
                                </div>
                                
                                <div id="map"></div>
                                <div class="mt-3" id="msg"></div>
                            </div>
                            <!-- /.card-body -->
                </div>
            </div>
        </section>
    </div>


		
</main>
@section('pagescript')
    
    <script type="text/javascript">
        // Initialize and add the map
        var nurse_lat = {{@$order->nurse->latitude}};
        var nurse_lng = {{@$order->nurse->longitude}};
        var nurse = "{{@$order->nurse->name}}";
        var user = "{{@$order->customer_name}}";
        var user_lat = {{@$order->address->lat}};
        var user_lng = {{@$order->address->lng}};
        var map;

       //center midpoint

       
       document.getElementById('distance').innerHTML = calcCrow(nurse_lat,nurse_lng,user_lat,user_lng).toFixed(1);
       function calcCrow(lat1, lon1, lat2, lon2) 
            {
            var R = 6371; // km
            var dLat = toRad(lat2-lat1);
            var dLon = toRad(lon2-lon1);
            var lat1 = toRad(lat1);
            var lat2 = toRad(lat2);

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c;
            return d;
            }

            // Converts numeric degrees to radians
            function toRad(Value) 
            {
                return Value * Math.PI / 180;
            }
        
       //

        function initMap(){
            //var directionsService = new google.maps.DirectionsService;
            //var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: nurse_lat,lng:user_lng},
                zoom: 10,
            });
            /*
                directionsDisplay.setMap(map);
                var onChangeHandler = function() {
                    calculateAndDisplayRoute(directionsService , directionsDisplay);
                };
                document.getElementById('start').addEventListener('change',onChangeHandler);
                document.getElementById('end').addEventListener('change',onChangeHandler);
            */
            //call add maker function
            addMaker({lat:nurse_lat,lng:nurse_lng},nurse);
            addMaker({lat:user_lat,lng:user_lng},user);
            //add maker function
            function addMaker(coords,name)
            {
                var marker = new google.maps.Marker({
                    map: map,
                    position: coords,
                    title: name,
                });
            }
        }

        /*function calculateAndDisplayRoute(directionsService , directionsDisplay)
        {
            directionsService.route({
                origin:document.getElementById('start').value,
                destination:document.getElementById('end').value,
                travelMode:'DRIVING'
            },function(response, status){
                if(status==='OK'){
                    directionsDisplay.setDirections(response);
                }
            });
        }*/

        var line = new google.maps.Polyline({
            path: [
                new google.maps.LatLng(nurse_lat, nurse_lng), 
                new google.maps.LatLng(user_lat, user_lng)
            ],
            strokeColor: "#FF0000",
            strokeOpacity: 1.0,
            strokeWeight: 10,
            map: map
        });

        
</script>

<script>
    // Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
let map, infoWindow;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: {lat: nurse_lat,lng:user_lng},
    zoom: 10,
  });
  infoWindow = new google.maps.InfoWindow();

  const locationButton = document.createElement("button");

  locationButton.textContent = "Pan to Current Location";
  locationButton.classList.add("custom-map-control-button");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
  locationButton.addEventListener("click", () => {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };

          infoWindow.setPosition(pos);
          infoWindow.setContent("Location found.");
          infoWindow.open(map);
          map.setCenter(pos);
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}
</script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVBluvCqAVP57iDmCxZPxkm5xWZV6QTeU&callback=initMap"></script>
@stop
@endsection
