<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\LocationMedia;
use Session;

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $meta = [
            'title' => 'Locations',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Locations' => route('admin-locations')
            ]
        ];

        $locations = location::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.locations.index', compact('meta','locations'));
    }

    public function add()
    {
        $meta = [
            'title' => 'Add Location',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add Location' => route('admin-add-location')
            ]
        ]; 

        return view('admin.locations.add', compact('meta'));
    }

    public function create(Request $request)
    {
        $post = Location::create([
            'title' => $request->title,
            'address' => $request->address,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'description' => $request->description
        ]);

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/locations/', $image);
                $media = LocationMedia::create([
                    'location_id' => $post->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Location Created.'); 
        return redirect()->route('admin-locations');
    }

    public function edit($id)
    {
        $meta = [
            'title' => 'Edit Location',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit Location' => route('admin-edit-location',$id)
            ]
        ];

        $location = Location::find($id);

        return view('admin.locations.edit', compact('meta','location'));
    }

    public function update(Request $request)
    {
        $location = Location::find($request->id);

        $location->title       = $request->title;
        $location->address     = $request->address;
        $location->lat         = $request->lat;
        $location->lng         = $request->lng;
        $location->description = $request->description;
        $location->save();

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/locations/', $image);
                $media = LocationMedia::create([
                    'location_id' => $location->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Location Updated.'); 
        return redirect()->route('admin-locations');
    }

    public function delete($id)
    {
        $c = Location::find($id);

        foreach($c->media as $m){
            try{
                unlink($m->media);
            } catch(\Exception $e) {
                // nothing to do
            }
            LocationMedia::find( $m->id )->delete();
        }

        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function deletMedia($id)
    {
        $m = LocationMedia::find($id);

        try{
            unlink($m->media);
        } catch(\Exception $e) {
            // nothing to do
        }
        
        $m->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
