<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\ContentMedia;
use Session;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $meta = [
            'title' => 'Website Contents',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Website Contents' => route('admin-content')
            ]
        ];

        $contents = Content::orderBy('id','desc')->paginate(env('PAGINATION_RECORDS'));
        return view('admin.content.index', compact('meta','contents'));
    }

    public function add()
    {
        $meta = [
            'title' => 'Add New Content',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Add New Content' => route('admin-add-content')
            ]
        ]; 

        return view('admin.content.add', compact('meta'));
    }

    public function create(Request $request)
    {
        $content = Content::create([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'description' => $request->description
        ]);

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/contents/', $image);
                $media = ContentMedia::create([
                    'content_id' => $content->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Content Created.'); 
        return redirect()->route('admin-content');
    }

    public function edit($id)
    {
        $meta = [
            'title' => 'Edit Content',
            'breadcrumbs' => [
                'Home' => route('admin-dashboard'),
                'Edit Content' => route('admin-edit-content',$id)
            ]
        ];

        $content = Content::find($id);

        return view('admin.content.edit', compact('meta','content'));
    }

    public function update(Request $request)
    {
        $content = Content::find($request->id);

        $content->title = $request->title;
        $content->subtitle = $request->subtitle;
        $content->description = $request->description;
        $content->save();

        if($request->media){
            foreach($request->media as $image){
                $file = $this->saveBase64Image('uploads/contents/', $image);
                $media = ContentMedia::create([
                    'content_id' => $content->id,
                    'media' => $file,
                ]);
            }
        }

        Session::flash('success', 'Content Updated.'); 
        return redirect()->route('admin-content');
    }

    public function delete($id)
    {
        $c = Content::find($id);

        foreach($c->media as $m){
            try{
                unlink($m->media);
            } catch(\Exception $e) {

            }
            ContentMedia::find( $m->id )->delete();
        }

        $c->delete();

        return response()->json([
            "status"=>true
        ]);
    }

    public function deletMedia($id)
    {
        $m = ContentMedia::find($id);
        
        try{
            unlink($m->media);
        } catch(\Exception $e) {

        }

        $m->delete();

        return response()->json([
            "status"=>true
        ]);
    }
}
