@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">
                    <a href="{{ route('products.create') }}" class="btn btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-danger">Edit only if you are sure that what it is going to change.</h3>
                                <div class="card-tools">
                                    <?php echo $products->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 300px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>SKU</th>
                                            <th>Selling Price</th>
                                            <th>Purchasing Price</th>
                                            <th>Slug</th>
                                            <th>Published</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $pro)
                                        <tr id="record-{{ $pro->id }}">
                                            <td>#{{ $loop->index+1 }}</td>
                                            <td>{{ $pro->name }}</td>
                                            <td>{{ $pro->categoryDetail->name }}</td>
                                            <td>{{ $pro->sku }}</td>
                                            <td>{{ $pro->selling_price  }}</td>
                                            <td>{{ $pro->purchasing_price }}</td>
                                            <td>{{ $pro->slug }}</td>
                                            <td><span id="status_{{$pro->id}}" onclick="changeStatus('{{$pro->id}}')">{{ $pro->published }}</span></td>
                                            <td class="text-center">
                                                <a href="{{ route('products.edit', $pro->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                                <button onclick="deleteEvent(<?php echo $pro->id ?>)" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')
@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    function deleteEvent(id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this record?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/products') }}/" + id, {
                    method: "DELETE",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    }
                }).then(response => response.json()).then(res => {
                    if (res.status) {
                        document.getElementById('record-' + id).remove();
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }

    function changeStatus(id) {
        fetch("{{ url('admin/products/change-status') }}/" + id, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        }).then(response => response.json()).then(res => {
            if (res.status) {
                $('#status_'+id).html(res.value);
                swal("Status Updated!", {
                    icon: "success",
                });
            }
        });
    }
</script>
@stop

@endsection