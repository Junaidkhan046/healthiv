@extends('layouts.website2')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>
<main id="primary" class="site-main file-woocommerce">
			<div id="product-34" class="product type-product post-34 status-publish first instock product_cat-first-month-of-subscription has-post-thumbnail virtual sold-individually purchasable product-type-booking">

                <!-- Product Hero -->
                <div class="jumbotron book-your-first-month mb-0 ">
                    <div class="container mt-5 pt-4 mb-3">
                        <div class="woocommerce-product-gallery intra-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
                            <figure class="woocommerce-product-gallery__wrapper">
                                <div data-thumb="https://static.thenounproject.com/png/1328079-200.png" data-thumb-alt="" class="woocommerce-product-gallery__image">
                                    <a href="https://static.thenounproject.com/png/1328079-200.png">
                                        <img style="max-width: 100px;" src="https://static.thenounproject.com/png/1328079-200.png" class="wp-post-image" alt="">
                                    </a>
                                </div>	
                            </figure>
                        </div>

                        <div style="clear:both;">
                            <h1 class="product_title entry-title mb-4 pb-2 pt-2">Stripe Payment</h1>
                            
                            <hr class="my-4">

                            <div class="pt-3">

                                <p>All your health IV</p>

                            </div>
                        </div>

                    </div>
                </div>
	<!-- END Product Hero -->
	</div> 

    <div class="summary entry-summary">
    	<section class="intra-product-block-title">
	    	<div class="container my-5" style="">

	    		<div class="justify-content-center">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="panel panel-default credit-card-box">
                                    <div class="panel-heading display-table" >
                                        <div class="row display-tr" >
                                            <h3 class="panel-title display-td" >Payment Details</h3>
                                            <div class="display-td" >                            
                                                <img class="img-responsive pull-right" src="{{asset('img/stripe.png')}}" width="100%">
                                            </div>
                                        </div>                    
                                    </div>
                                    <div class="panel-body">
                    
                                        @if (Session::has('success'))
                                            <div class="alert alert-success">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                <p>{{ Session::get('success') }}</p>
                                            </div>
                                        @endif
                    
                                        <form 
                                                role="form" 
                                                action="{{ route('stripe.post',$order->id) }}" 
                                                method="post" 
                                                class="require-validation"
                                                data-cc-on-file="false"
                                                data-stripe-publishable-key="{{ $apikey }}"
                                                id="payment-form">
                                            @csrf
                    
                                            <div class='form-row row'>
                                                <div class='col-md-12 form-group required'>
                                                    <label class='control-label'>Name on Card</label> 
                                                    <input class='form-control' type='text'>
                                                </div>
                                            </div>
                    
                                            <div class='form-row row'>
                                                <div class='col-md-12 form-group card required py-3'>
                                                    <label class='control-label'>Card Number</label> 
                                                    <input autocomplete='off' class='form-control card-number' type='text'>
                                                </div>
                                            </div>
                    
                                            <div class='form-row row'>
                                                <div class='col-xs-12 col-md-4 form-group cvc required'>
                                                    <label class='control-label'>CVC</label> <input autocomplete='off'
                                                        class='form-control card-cvc' placeholder='ex. 311'
                                                        type='text'>
                                                </div>
                                                <div class='col-xs-12 col-md-4 form-group expiration required'>
                                                    <label class='control-label'>Expiration Month</label> <input
                                                        class='form-control card-expiry-month' placeholder='MM' size='2'
                                                        type='text'>
                                                </div>
                                                <div class='col-xs-12 col-md-4 form-group expiration required'>
                                                    <label class='control-label'>Expiration Year</label> <input
                                                        class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                                        type='text'>
                                                </div>
                                            </div>
                    
                                            <div class='form-row row'>
                                                <div class='col-md-12 error form-group d-none'>
                                                    <div class='alert-danger alert'>Please correct the errors and try
                                                        again.</div>
                                                </div>
                                            </div>
                    
                                            <div class="row justify-content-center">
                                                <div class="col-md-6">
                                                    <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now (${{ $order->amount }})</button>
                                                </div>
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


		
</main>
    @section('pagescript')
        @if(\Session::has('success'))
        <script>
            swal("Success!", "{{ Session::get('success') }}", "success");
        </script>
        @endif
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        
        <script type="text/javascript">
        $(function() {
        
            var $form         = $(".require-validation");
        
            $('form.require-validation').bind('submit', function(e) {
                var $form         = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                                'input[type=text]', 'input[type=file]',
                                'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;
                $errorMessage.addClass('d-none');
        
                $('.has-error').removeClass('has-error');
                $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('d-none');
                    e.preventDefault();
                }
                });
        
                if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
                }
        
        });
        
        function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('.error')
                        .removeClass('d-none')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    /* token contains id, last4, and card type */
                    var token = response['id'];
                    
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }
        
        });
        </script>
    @stop
@endsection