<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPost extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'Title', 'long_description', 'image'];
}
