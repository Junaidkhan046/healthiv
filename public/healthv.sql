-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 04, 2021 at 09:23 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `healthvi`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_ons`
--

DROP TABLE IF EXISTS `add_ons`;
CREATE TABLE IF NOT EXISTS `add_ons` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `add_ons`
--

INSERT INTO `add_ons` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(3, 'testing 2', '<p>dfghj</p>', 'uploads/addons/61520c724a490.png', '2021-09-24 10:24:27', '2021-09-27 12:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(14, 0, 'Drip', '<p>This is testing</p>', '[\"uploads\\/categories\\/6128fa1f7c788.png\"]', '2021-08-27 09:13:43', '2021-08-27 09:13:43');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
CREATE TABLE IF NOT EXISTS `contents` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `subtitle`, `description`, `created_at`, `updated_at`) VALUES
(1, 'IV INFUSION THERAPY', 'Delivered to you', '<p>Delivered to you</p>', '2021-08-23 08:17:06', '2021-09-07 22:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `content_medias`
--

DROP TABLE IF EXISTS `content_medias`;
CREATE TABLE IF NOT EXISTS `content_medias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_medias_content_id_foreign` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_configs`
--

DROP TABLE IF EXISTS `email_configs`;
CREATE TABLE IF NOT EXISTS `email_configs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `driver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `host` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `encryption` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_configs`
--

INSERT INTO `email_configs` (`id`, `driver`, `host`, `port`, `username`, `password`, `encryption`, `from_address`, `from_name`, `created_at`, `updated_at`) VALUES
(1, 'smtp asdfgh', 'smtp.mailtrap.io', '2525', 'info@gmail.com', '123456', 'ssl', 'rajputumesh117@gmail.com', 'umesh rajput', '2021-09-23 11:40:36', '2021-09-23 12:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

DROP TABLE IF EXISTS `enquiries`;
CREATE TABLE IF NOT EXISTS `enquiries` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` char(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `slug`, `title`, `subtitle`, `description`, `created_at`, `updated_at`) VALUES
(4, 'what-if-we-could-change-how-drugs-are-developed', 'What if we could change how drugs are developed?', 'What if we could change how drugs are developed?', '<p>What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?What if we could change how drugs are developed?<br></p>', '2021-09-08 20:14:24', '2021-09-08 20:14:24');

-- --------------------------------------------------------

--
-- Table structure for table `event_medias`
--

DROP TABLE IF EXISTS `event_medias`;
CREATE TABLE IF NOT EXISTS `event_medias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_medias_event_id_foreign` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `slug`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'what-is-google-my-business-and-what-do-i-get-with-it', 'What is Google My Business and what do I get with it?', '<p><span style=\"color: rgb(32, 33, 36); font-family: Roboto, Arial, Helvetica, sans-serif;\">Google My Business is a free tool that allows you to promote your Business Profile and business website on Google Search and Maps. With your Google My Business account, you can see and connect with your customers, post updates to your Business Profile and see how customers are interacting with your business on Google.</span><br></p>', '2021-09-24 13:19:21', '2021-09-24 13:19:21'),
(2, 'how-does-google-my-business-help-me-reach-more-customers', 'How does Google My Business help me reach more customers?', '<p><span style=\"color: rgb(32, 33, 36); font-family: Roboto, Arial, Helvetica, sans-serif;\">Google My Business gives you the power to attract and engage customers when they search for your business on Google. Your Business Profile gives you a presence on Google Search and Maps, allowing you to respond to Reviews, post photos of products or special offers and add/edit your business details. Your Google business website lets you present your business professionally online, showcasing your photos and business details in a beautiful way. Insights shows you how you can optimise your customer engagement for better results, and you can encourage more engagement by interacting with your customers from the Customers tab.</span><br></p>', '2021-09-24 13:19:46', '2021-09-24 13:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `google_apis`
--

DROP TABLE IF EXISTS `google_apis`;
CREATE TABLE IF NOT EXISTS `google_apis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `api_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `google_apis`
--

INSERT INTO `google_apis` (`id`, `api_key`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 'sdfg54646sdf5645sfgd4sfasdfgh', 'sfdgfh4564fsgd454gd65f', '2021-09-23 11:58:57', '2021-09-23 12:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE IF NOT EXISTS `holidays` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `title`, `subtitle`, `description`, `created_at`, `updated_at`) VALUES
(4, 'New York', NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">Glendale</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">Valley Stream</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">East Setauket</span><br></p>', '2021-09-02 13:26:43', '2021-09-02 13:26:43'),
(5, 'Florida', NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">Orlanda</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">Miami</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">Tampa</span><br></p>', '2021-09-02 13:27:32', '2021-09-02 13:27:32'),
(6, 'Texas', NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">Austin</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">San Antonio</span><br></p>', '2021-09-02 13:28:04', '2021-09-02 13:28:04'),
(7, 'Los Angeles', NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">West Hollywood</span><br></p>', '2021-09-02 13:28:43', '2021-09-02 13:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `location_medias`
--

DROP TABLE IF EXISTS `location_medias`;
CREATE TABLE IF NOT EXISTS `location_medias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_medias_location_id_foreign` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `location_medias`
--

INSERT INTO `location_medias` (`id`, `location_id`, `media`, `created_at`, `updated_at`) VALUES
(2, 4, 'uploads/locations/61311e6b5e850.png', '2021-09-02 13:26:43', '2021-09-02 13:26:43'),
(3, 5, 'uploads/locations/61311e9c07b74.png', '2021-09-02 13:27:32', '2021-09-02 13:27:32'),
(4, 6, 'uploads/locations/61311ebcd3107.png', '2021-09-02 13:28:04', '2021-09-02 13:28:04'),
(5, 7, 'uploads/locations/61311ee397bcc.png', '2021-09-02 13:28:43', '2021-09-02 13:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

DROP TABLE IF EXISTS `memberships`;
CREATE TABLE IF NOT EXISTS `memberships` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `period` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1-active,2-inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `memberships`
--

INSERT INTO `memberships` (`id`, `name`, `description`, `price`, `period`, `status`, `created_at`, `updated_at`) VALUES
(4, 'testing', '<p>sdfghj asdfgh</p>', '100', 22, 1, '2021-09-23 12:46:28', '2021-09-23 12:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `membership_media`
--

DROP TABLE IF EXISTS `membership_media`;
CREATE TABLE IF NOT EXISTS `membership_media` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `membership_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `membership_media_membership_id_foreign` (`membership_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `membership_media`
--

INSERT INTO `membership_media` (`id`, `membership_id`, `media`, `created_at`, `updated_at`) VALUES
(2, 4, 'uploads/membership/614cc47c0a886.png', '2021-09-23 12:46:28', '2021-09-23 12:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(17, '2021_08_17_193101_contents', 2),
(18, '2021_08_18_061329_create_categories_table', 2),
(19, '2021_08_18_061824_create_products_table', 2),
(20, '2021_08_18_062917_create_product_images_table', 2),
(21, '2021_08_18_063553_create_product_inventories_table', 2),
(22, '2021_08_18_064327_create_product_posts_table', 2),
(23, '2021_08_18_064612_create_product_metas_table', 2),
(24, '2021_08_18_065010_create_add_ons_table', 2),
(25, '2021_08_18_065300_create_orders_table', 2),
(26, '2021_08_18_070159_create_order_addresses_table', 2),
(27, '2021_08_18_072544_create_order_items_table', 2),
(28, '2021_08_18_073002_create_order_payments_table', 2),
(29, '2021_08_18_074256_create_nurse_documents_table', 2),
(30, '2021_08_18_074923_create_time_slots_table', 2),
(31, '2021_08_18_075219_create_working_days_table', 2),
(49, '2021_08_21_181836_content_medias', 3),
(50, '2021_08_21_182242_enquiry', 3),
(51, '2021_08_21_182808_events', 3),
(52, '2021_08_21_183432_event_medias', 3),
(53, '2021_08_21_183846_faqs', 3),
(54, '2021_08_21_184215_locations', 3),
(55, '2021_08_21_184630_location_medias', 3),
(56, '2021_08_21_184943_posts', 3),
(57, '2021_08_21_185231_post_medias', 3),
(58, '2021_08_21_185747_create_settings_table', 3),
(59, '2021_08_21_185942_user_medias', 3),
(60, '2021_09_04_202005_create_waiting_lists_table', 4),
(61, '2021_09_11_183711_create_permission_tables', 5),
(62, '2021_09_14_161533_add_new_active_status_field', 5),
(63, '2021_09_16_183441_change_data_type', 6),
(64, '2021_09_21_104509_create_tests_table', 7),
(65, '2021_09_21_104612_create_memberships_table', 7),
(66, '2021_09_21_154332_create_test_media_table', 8),
(67, '2021_09_21_165024_create_membership_media_table', 9),
(68, '2021_09_23_152449_create_email_configs_table', 10),
(69, '2021_09_23_152556_create_payment_gateways_table', 10),
(70, '2021_09_23_152654_create_google_apis_table', 10),
(71, '2021_09_23_152747_create_holidays_table', 10),
(72, '2021_09_24_142620_create_add_on_media_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 7),
(3, 'App\\User', 3),
(3, 'App\\User', 4),
(3, 'App\\User', 6),
(3, 'App\\User', 8),
(3, 'App\\User', 9),
(4, 'App\\User', 5),
(4, 'App\\User', 10),
(4, 'App\\User', 11),
(4, 'App\\User', 12),
(4, 'App\\User', 13);

-- --------------------------------------------------------

--
-- Table structure for table `nurse_documents`
--

DROP TABLE IF EXISTS `nurse_documents`;
CREATE TABLE IF NOT EXISTS `nurse_documents` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `document` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 for verified and 0 for not verified',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nurse_documents_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nurse_documents`
--

INSERT INTO `nurse_documents` (`id`, `user_id`, `document`, `status`, `created_at`, `updated_at`) VALUES
(1, 6, '[\"uploads\\/document\\/\\/Screen Shot 2021-09-17 at 1.21.29 AM_1631822150.png\"]', 1, '2021-09-16 19:55:50', '2021-09-20 20:02:20'),
(3, 9, '[\"uploads\\/document\\/\\/B_1632851867.png\"]', 1, '2021-09-28 12:27:47', '2021-10-02 12:22:04');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assigned_nurse_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tranking_status` enum('New Order','Assigned','On The Way','Delivered','Cancelled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'New Order',
  `delivery_working_day_id` int(11) NOT NULL DEFAULT 0,
  `delivery_time_slot_id` int(11) NOT NULL DEFAULT 0,
  `amount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'Process' COMMENT 'Process, Success, Failed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `customer_name`, `customer_email`, `customer_phone`, `assigned_nurse_id`, `tranking_status`, `delivery_working_day_id`, `delivery_time_slot_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(3, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 08:40:50', '2021-09-27 08:40:50'),
(4, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:19:20', '2021-09-27 09:19:20'),
(5, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:33:14', '2021-09-27 09:33:14'),
(6, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:34:27', '2021-09-27 09:34:27'),
(7, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:37:41', '2021-09-27 09:37:41'),
(8, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:39:11', '2021-09-27 09:39:11'),
(9, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:39:36', '2021-09-27 09:39:36'),
(10, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:42:37', '2021-09-27 09:42:37'),
(11, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:51:01', '2021-09-27 09:51:01'),
(12, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:52:28', '2021-09-27 09:52:28'),
(13, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:55:05', '2021-09-27 09:55:05'),
(14, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 09:56:11', '2021-09-27 09:56:11'),
(15, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 10:10:11', '2021-09-27 10:10:11'),
(16, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 10:10:43', '2021-09-27 10:10:43'),
(17, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 10:11:28', '2021-09-27 10:11:28'),
(18, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 10:11:46', '2021-09-27 10:11:46'),
(19, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 10:12:28', '2021-09-27 10:12:28'),
(20, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 10:12:59', '2021-09-27 10:12:59'),
(21, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:18:26', '2021-09-27 11:18:26'),
(22, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:19:06', '2021-09-27 11:19:06'),
(23, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:22:06', '2021-09-27 11:22:06'),
(24, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:23:25', '2021-09-27 11:23:25'),
(25, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:25:43', '2021-09-27 11:25:43'),
(26, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:26:38', '2021-09-27 11:26:38'),
(27, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:26:50', '2021-09-27 11:26:50'),
(28, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:27:55', '2021-09-27 11:27:55'),
(29, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:40:54', '2021-09-27 11:40:54'),
(30, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:46:04', '2021-09-27 11:46:04'),
(31, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:48:42', '2021-09-27 11:48:42'),
(32, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', NULL, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:52:11', '2021-09-27 11:52:11'),
(33, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', 3, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:52:35', '2021-09-28 09:56:20'),
(34, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '+917078049692', 6, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 11:52:58', '2021-09-28 09:56:10'),
(35, 0, 'Umesh Rajput', 'rajputumesh117@gmail.com', '07078049692', 9, 'New Order', 0, 0, NULL, 'Process', '2021-09-27 13:19:22', '2021-09-29 09:45:46'),
(36, 13, 'Umesh Rajput', 'umesh117@gmail.com', '07078049692', 9, 'New Order', 0, 0, NULL, 'Process', '2021-09-29 14:35:51', '2021-09-29 14:39:44'),
(37, 1, 'Umesh Rajput', 'rajputumesh117@gmail.com', '07078049692', NULL, 'New Order', 0, 0, '10', 'Success', '2021-10-01 15:18:31', '2021-10-02 12:13:41'),
(38, 1, 'Supper Admin', 'admin@healthiv.com', '7078049692', NULL, 'New Order', 0, 0, '20', 'Process', '2021-10-01 15:20:44', '2021-10-01 15:20:44'),
(39, 13, 'Umesh Rajput', 'umesh117@gmail.com', '7078049692', 9, 'New Order', 0, 0, NULL, 'Success', '2021-10-02 11:35:17', '2021-10-02 11:59:00'),
(40, 13, 'Umesh Rajput', 'umesh117@gmail.com', '7078049692', 9, 'New Order', 0, 0, '129', 'Success', '2021-10-02 11:39:40', '2021-10-02 11:58:52');

-- --------------------------------------------------------

--
-- Table structure for table `order_addresses`
--

DROP TABLE IF EXISTS `order_addresses`;
CREATE TABLE IF NOT EXISTS `order_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `address_line` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` int(11) NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_addresses_order_id_foreign` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_addresses`
--

INSERT INTO `order_addresses` (`id`, `address_line`, `zipcode`, `city`, `state`, `country`, `lat`, `lng`, `comment`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'Palakkad Kerala India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 3, '2021-09-27 08:40:50', '2021-09-27 08:40:50'),
(2, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 4, '2021-09-27 09:19:21', '2021-09-27 09:19:21'),
(3, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 5, '2021-09-27 09:33:14', '2021-09-27 09:33:14'),
(4, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 6, '2021-09-27 09:34:27', '2021-09-27 09:34:27'),
(5, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 7, '2021-09-27 09:37:41', '2021-09-27 09:37:41'),
(6, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 8, '2021-09-27 09:39:11', '2021-09-27 09:39:11'),
(7, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 9, '2021-09-27 09:39:36', '2021-09-27 09:39:36'),
(8, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 10, '2021-09-27 09:42:37', '2021-09-27 09:42:37'),
(9, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 11, '2021-09-27 09:51:01', '2021-09-27 09:51:01'),
(10, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 12, '2021-09-27 09:52:28', '2021-09-27 09:52:28'),
(11, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 13, '2021-09-27 09:55:05', '2021-09-27 09:55:05'),
(12, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 14, '2021-09-27 09:56:11', '2021-09-27 09:56:11'),
(13, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 15, '2021-09-27 10:10:11', '2021-09-27 10:10:11'),
(14, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 16, '2021-09-27 10:10:43', '2021-09-27 10:10:43'),
(15, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 17, '2021-09-27 10:11:28', '2021-09-27 10:11:28'),
(16, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 18, '2021-09-27 10:11:46', '2021-09-27 10:11:46'),
(17, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 19, '2021-09-27 10:12:28', '2021-09-27 10:12:28'),
(18, 'Dhoni Waterfalls, Puthuppariyaram, Palakkad, Kerala, India', 678591, 'Palakkad', 'Kerala', 'India', '10.8597097', '76.6228866', NULL, 20, '2021-09-27 10:12:59', '2021-09-27 10:12:59'),
(19, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 21, '2021-09-27 11:18:26', '2021-09-27 11:18:26'),
(20, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 22, '2021-09-27 11:19:06', '2021-09-27 11:19:06'),
(21, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 23, '2021-09-27 11:22:06', '2021-09-27 11:22:06'),
(22, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 24, '2021-09-27 11:23:25', '2021-09-27 11:23:25'),
(23, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 25, '2021-09-27 11:25:43', '2021-09-27 11:25:43'),
(24, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 26, '2021-09-27 11:26:38', '2021-09-27 11:26:38'),
(25, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 27, '2021-09-27 11:26:50', '2021-09-27 11:26:50'),
(26, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 28, '2021-09-27 11:27:56', '2021-09-27 11:27:56'),
(27, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 29, '2021-09-27 11:40:54', '2021-09-27 11:40:54'),
(28, 'Lal madir dhanauli jagner road agra uttar pradesh, Jagner Road, Dhanauli, Agra, Uttar Pradesh, India', 282001, 'Agra', 'Uttar Pradesh', 'India', '27.1374174', '77.96576639999999', NULL, 32, '2021-09-27 11:52:11', '2021-09-27 11:52:11'),
(29, 'Lal madir dhanauli jagner road agra uttar pradesh, Jagner Road, Dhanauli, Agra, Uttar Pradesh, India', 282001, 'Agra', 'Uttar Pradesh', 'India', '27.1374174', '77.96576639999999', NULL, 33, '2021-09-27 11:52:35', '2021-09-27 11:52:35'),
(30, 'Lal madir dhanauli jagner road agra uttar pradesh, Jagner Road, Dhanauli, Agra, Uttar Pradesh, India', 282001, 'Agra', 'Uttar Pradesh', 'India', '27.1374174', '77.96576639999999', NULL, 34, '2021-09-27 11:52:59', '2021-09-27 11:52:59'),
(31, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '26.1797682', '77.0199335', NULL, 35, '2021-09-27 13:19:22', '2021-09-27 13:19:22'),
(32, 'Sikandra, Agra, Uttar Pradesh, India', 282007, 'Agra', 'Uttar Pradesh', 'India', '27.217135', '77.94667559999999', NULL, 36, '2021-09-29 14:35:51', '2021-09-29 14:35:51'),
(33, 'Dhanauli, Agra, Uttar Pradesh, India', 282003, 'Agra', 'Uttar Pradesh', 'India', '27.1797686', '78.0199343', NULL, 38, '2021-10-01 15:20:44', '2021-10-01 15:20:44'),
(34, 'Kheriya Mod, Ajeet Nagar, Arjun Nagar, Agra, Uttar Pradesh, India', 282001, 'Agra', 'Uttar Pradesh', 'India', '27.1632229', '77.9840146', NULL, 39, '2021-10-02 11:35:17', '2021-10-02 11:35:17'),
(35, 'Kheriya Mod, Ajeet Nagar, Arjun Nagar, Agra, Uttar Pradesh, India', 282001, 'Agra', 'Uttar Pradesh', 'India', '27.1632229', '77.9840146', NULL, 40, '2021-10-02 11:39:40', '2021-10-02 11:39:40');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amount` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `sku`, `qty`, `product_id`, `type`, `price`, `total_amount`, `order_id`, `created_at`, `updated_at`) VALUES
(7, 2, 1, '15', 'product', '129', '129', 3, '2021-09-27 08:40:50', '2021-09-27 08:40:50'),
(8, NULL, NULL, '3', 'addons', NULL, NULL, 3, '2021-09-27 08:40:50', '2021-09-27 08:40:50'),
(9, NULL, NULL, '10', 'tests', NULL, NULL, 3, '2021-09-27 08:40:50', '2021-09-27 08:40:50'),
(10, 5, 1, '18', 'product', '129', '129', 4, '2021-09-27 09:19:21', '2021-09-27 09:19:21'),
(11, NULL, NULL, '3', 'addons', NULL, NULL, 4, '2021-09-27 09:19:21', '2021-09-27 09:19:21'),
(12, NULL, NULL, '10', 'tests', NULL, NULL, 4, '2021-09-27 09:19:21', '2021-09-27 09:19:21'),
(13, 2, 1, '15', 'product', '129', '129', 5, '2021-09-27 09:33:14', '2021-09-27 09:33:14'),
(14, NULL, NULL, '3', 'addons', NULL, NULL, 5, '2021-09-27 09:33:14', '2021-09-27 09:33:14'),
(15, NULL, NULL, '10', 'tests', NULL, NULL, 5, '2021-09-27 09:33:14', '2021-09-27 09:33:14'),
(16, 2, 1, '15', 'product', '129', '129', 6, '2021-09-27 09:34:27', '2021-09-27 09:34:27'),
(17, NULL, NULL, '3', 'addons', NULL, NULL, 6, '2021-09-27 09:34:27', '2021-09-27 09:34:27'),
(18, NULL, NULL, '10', 'tests', NULL, NULL, 6, '2021-09-27 09:34:27', '2021-09-27 09:34:27'),
(19, 2, 1, '15', 'product', '129', '129', 7, '2021-09-27 09:37:41', '2021-09-27 09:37:41'),
(20, NULL, NULL, '3', 'addons', NULL, NULL, 7, '2021-09-27 09:37:41', '2021-09-27 09:37:41'),
(21, NULL, NULL, '10', 'tests', NULL, NULL, 7, '2021-09-27 09:37:41', '2021-09-27 09:37:41'),
(22, 2, 1, '15', 'product', '129', '129', 8, '2021-09-27 09:39:11', '2021-09-27 09:39:11'),
(23, NULL, NULL, '3', 'addons', NULL, NULL, 8, '2021-09-27 09:39:11', '2021-09-27 09:39:11'),
(24, NULL, NULL, '10', 'tests', NULL, NULL, 8, '2021-09-27 09:39:11', '2021-09-27 09:39:11'),
(25, 2, 1, '15', 'product', '129', '129', 9, '2021-09-27 09:39:36', '2021-09-27 09:39:36'),
(26, NULL, NULL, '3', 'addons', NULL, NULL, 9, '2021-09-27 09:39:36', '2021-09-27 09:39:36'),
(27, NULL, NULL, '10', 'tests', NULL, NULL, 9, '2021-09-27 09:39:36', '2021-09-27 09:39:36'),
(28, 2, 1, '15', 'product', '129', '129', 10, '2021-09-27 09:42:37', '2021-09-27 09:42:37'),
(29, NULL, NULL, '3', 'addons', NULL, NULL, 10, '2021-09-27 09:42:37', '2021-09-27 09:42:37'),
(30, NULL, NULL, '10', 'tests', NULL, NULL, 10, '2021-09-27 09:42:37', '2021-09-27 09:42:37'),
(31, 2, 1, '15', 'product', '129', '129', 11, '2021-09-27 09:51:01', '2021-09-27 09:51:01'),
(32, NULL, NULL, '3', 'addons', NULL, NULL, 11, '2021-09-27 09:51:01', '2021-09-27 09:51:01'),
(33, NULL, NULL, '10', 'tests', NULL, NULL, 11, '2021-09-27 09:51:01', '2021-09-27 09:51:01'),
(34, 2, 1, '15', 'product', '129', '129', 12, '2021-09-27 09:52:28', '2021-09-27 09:52:28'),
(35, NULL, NULL, '3', 'addons', NULL, NULL, 12, '2021-09-27 09:52:28', '2021-09-27 09:52:28'),
(36, NULL, NULL, '10', 'tests', NULL, NULL, 12, '2021-09-27 09:52:28', '2021-09-27 09:52:28'),
(37, 2, 1, '15', 'product', '129', '129', 13, '2021-09-27 09:55:05', '2021-09-27 09:55:05'),
(38, NULL, NULL, '3', 'addons', NULL, NULL, 13, '2021-09-27 09:55:05', '2021-09-27 09:55:05'),
(39, NULL, NULL, '10', 'tests', NULL, NULL, 13, '2021-09-27 09:55:05', '2021-09-27 09:55:05'),
(40, 2, 1, '15', 'product', '129', '129', 14, '2021-09-27 09:56:11', '2021-09-27 09:56:11'),
(41, NULL, NULL, '3', 'addons', NULL, NULL, 14, '2021-09-27 09:56:11', '2021-09-27 09:56:11'),
(42, NULL, NULL, '10', 'tests', NULL, NULL, 14, '2021-09-27 09:56:11', '2021-09-27 09:56:11'),
(43, 2, 1, '15', 'product', '129', '129', 15, '2021-09-27 10:10:11', '2021-09-27 10:10:11'),
(44, NULL, NULL, '3', 'addons', NULL, NULL, 15, '2021-09-27 10:10:11', '2021-09-27 10:10:11'),
(45, NULL, NULL, '10', 'tests', NULL, NULL, 15, '2021-09-27 10:10:11', '2021-09-27 10:10:11'),
(46, 2, 1, '15', 'product', '129', '129', 16, '2021-09-27 10:10:43', '2021-09-27 10:10:43'),
(47, NULL, NULL, '3', 'addons', NULL, NULL, 16, '2021-09-27 10:10:43', '2021-09-27 10:10:43'),
(48, NULL, NULL, '10', 'tests', NULL, NULL, 16, '2021-09-27 10:10:43', '2021-09-27 10:10:43'),
(49, 2, 1, '15', 'product', '129', '129', 17, '2021-09-27 10:11:28', '2021-09-27 10:11:28'),
(50, NULL, NULL, '3', 'addons', NULL, NULL, 17, '2021-09-27 10:11:28', '2021-09-27 10:11:28'),
(51, NULL, NULL, '10', 'tests', NULL, NULL, 17, '2021-09-27 10:11:28', '2021-09-27 10:11:28'),
(52, 2, 1, '15', 'product', '129', '129', 18, '2021-09-27 10:11:46', '2021-09-27 10:11:46'),
(53, NULL, NULL, '3', 'addons', NULL, NULL, 18, '2021-09-27 10:11:46', '2021-09-27 10:11:46'),
(54, NULL, NULL, '10', 'tests', NULL, NULL, 18, '2021-09-27 10:11:46', '2021-09-27 10:11:46'),
(55, 2, 1, '15', 'product', '129', '129', 19, '2021-09-27 10:12:28', '2021-09-27 10:12:28'),
(56, NULL, NULL, '3', 'addons', NULL, NULL, 19, '2021-09-27 10:12:28', '2021-09-27 10:12:28'),
(57, NULL, NULL, '10', 'tests', NULL, NULL, 19, '2021-09-27 10:12:28', '2021-09-27 10:12:28'),
(58, 2, 1, '15', 'product', '129', '129', 20, '2021-09-27 10:12:59', '2021-09-27 10:12:59'),
(59, NULL, NULL, '3', 'addons', NULL, NULL, 20, '2021-09-27 10:12:59', '2021-09-27 10:12:59'),
(60, NULL, NULL, '10', 'tests', NULL, NULL, 20, '2021-09-27 10:12:59', '2021-09-27 10:12:59'),
(61, 2, 1, '15', 'product', '129', '129', 21, '2021-09-27 11:18:26', '2021-09-27 11:18:26'),
(62, NULL, NULL, '3', 'addons', NULL, NULL, 21, '2021-09-27 11:18:26', '2021-09-27 11:18:26'),
(63, NULL, NULL, '10', 'tests', NULL, NULL, 21, '2021-09-27 11:18:26', '2021-09-27 11:18:26'),
(64, 2, 1, '15', 'product', '129', '129', 22, '2021-09-27 11:19:06', '2021-09-27 11:19:06'),
(65, NULL, NULL, '3', 'addons', NULL, NULL, 22, '2021-09-27 11:19:06', '2021-09-27 11:19:06'),
(66, NULL, NULL, '10', 'tests', NULL, NULL, 22, '2021-09-27 11:19:06', '2021-09-27 11:19:06'),
(67, 2, 1, '15', 'product', '129', '129', 23, '2021-09-27 11:22:06', '2021-09-27 11:22:06'),
(68, NULL, NULL, '3', 'addons', NULL, NULL, 23, '2021-09-27 11:22:06', '2021-09-27 11:22:06'),
(69, NULL, NULL, '10', 'tests', NULL, NULL, 23, '2021-09-27 11:22:06', '2021-09-27 11:22:06'),
(70, 2, 1, '15', 'product', '129', '129', 24, '2021-09-27 11:23:25', '2021-09-27 11:23:25'),
(71, NULL, NULL, '3', 'addons', NULL, NULL, 24, '2021-09-27 11:23:25', '2021-09-27 11:23:25'),
(72, NULL, NULL, '10', 'tests', NULL, NULL, 24, '2021-09-27 11:23:25', '2021-09-27 11:23:25'),
(73, 2, 1, '15', 'product', '129', '129', 25, '2021-09-27 11:25:43', '2021-09-27 11:25:43'),
(74, NULL, NULL, '3', 'addons', NULL, NULL, 25, '2021-09-27 11:25:43', '2021-09-27 11:25:43'),
(75, NULL, NULL, '10', 'tests', NULL, NULL, 25, '2021-09-27 11:25:43', '2021-09-27 11:25:43'),
(76, 2, 1, '15', 'product', '129', '129', 26, '2021-09-27 11:26:38', '2021-09-27 11:26:38'),
(77, NULL, NULL, '3', 'addons', NULL, NULL, 26, '2021-09-27 11:26:38', '2021-09-27 11:26:38'),
(78, NULL, NULL, '10', 'tests', NULL, NULL, 26, '2021-09-27 11:26:38', '2021-09-27 11:26:38'),
(79, 2, 1, '15', 'product', '129', '129', 27, '2021-09-27 11:26:50', '2021-09-27 11:26:50'),
(80, NULL, NULL, '3', 'addons', NULL, NULL, 27, '2021-09-27 11:26:50', '2021-09-27 11:26:50'),
(81, NULL, NULL, '10', 'tests', NULL, NULL, 27, '2021-09-27 11:26:50', '2021-09-27 11:26:50'),
(82, 2, 1, '15', 'product', '129', '129', 28, '2021-09-27 11:27:56', '2021-09-27 11:27:56'),
(83, NULL, NULL, '3', 'addons', NULL, NULL, 28, '2021-09-27 11:27:56', '2021-09-27 11:27:56'),
(84, NULL, NULL, '10', 'tests', NULL, NULL, 28, '2021-09-27 11:27:56', '2021-09-27 11:27:56'),
(85, 2, 1, '15', 'product', '129', '129', 29, '2021-09-27 11:40:54', '2021-09-27 11:40:54'),
(86, NULL, NULL, '3', 'addons', NULL, NULL, 29, '2021-09-27 11:40:54', '2021-09-27 11:40:54'),
(87, NULL, NULL, '10', 'tests', NULL, NULL, 29, '2021-09-27 11:40:54', '2021-09-27 11:40:54'),
(88, 2, 1, '15', 'product', '129', '129', 30, '2021-09-27 11:46:05', '2021-09-27 11:46:05'),
(89, NULL, NULL, '3', 'addons', NULL, NULL, 30, '2021-09-27 11:46:05', '2021-09-27 11:46:05'),
(90, NULL, NULL, '10', 'tests', NULL, NULL, 30, '2021-09-27 11:46:05', '2021-09-27 11:46:05'),
(91, 2, 1, '15', 'product', '129', '129', 31, '2021-09-27 11:48:42', '2021-09-27 11:48:42'),
(92, NULL, NULL, '3', 'addons', NULL, NULL, 31, '2021-09-27 11:48:42', '2021-09-27 11:48:42'),
(93, NULL, NULL, '10', 'tests', NULL, NULL, 31, '2021-09-27 11:48:42', '2021-09-27 11:48:42'),
(94, 2, 1, '15', 'product', '129', '129', 32, '2021-09-27 11:52:11', '2021-09-27 11:52:11'),
(95, NULL, NULL, '3', 'addons', NULL, NULL, 32, '2021-09-27 11:52:11', '2021-09-27 11:52:11'),
(96, NULL, NULL, '10', 'tests', NULL, NULL, 32, '2021-09-27 11:52:11', '2021-09-27 11:52:11'),
(97, 2, 1, '15', 'product', '129', '129', 33, '2021-09-27 11:52:35', '2021-09-27 11:52:35'),
(98, NULL, NULL, '3', 'addons', NULL, NULL, 33, '2021-09-27 11:52:35', '2021-09-27 11:52:35'),
(99, NULL, NULL, '10', 'tests', NULL, NULL, 33, '2021-09-27 11:52:35', '2021-09-27 11:52:35'),
(100, 2, 1, '15', 'product', '129', '129', 34, '2021-09-27 11:52:59', '2021-09-27 11:52:59'),
(101, NULL, NULL, '3', 'addons', NULL, NULL, 34, '2021-09-27 11:52:59', '2021-09-27 11:52:59'),
(102, NULL, NULL, '10', 'tests', NULL, NULL, 34, '2021-09-27 11:52:59', '2021-09-27 11:52:59'),
(103, 2, 1, '15', 'product', '129', '129', 35, '2021-09-27 13:19:22', '2021-09-27 13:19:22'),
(104, 6, 1, '19', 'product', '111', '111', 36, '2021-09-29 14:35:51', '2021-09-29 14:35:51'),
(105, 2, 1, '15', 'product', '129', '129', 37, '2021-10-01 15:18:31', '2021-10-01 15:18:31'),
(106, 2, 1, '15', 'product', '129', '129', 38, '2021-10-01 15:20:44', '2021-10-01 15:20:44'),
(107, 2, 1, '15', 'product', '129', '129', 39, '2021-10-02 11:35:17', '2021-10-02 11:35:17'),
(108, 2, 1, '15', 'product', '129', '129', 40, '2021-10-02 11:39:40', '2021-10-02 11:39:40');

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

DROP TABLE IF EXISTS `order_payments`;
CREATE TABLE IF NOT EXISTS `order_payments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `amount` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 for success and 0 for failed',
  `payment_code` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_payments_order_id_foreign` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `order_id`, `amount`, `transaction_id`, `status`, `payment_code`, `created_at`, `updated_at`) VALUES
(1, 4, '129', '5444445233184', 1, 0, '2021-09-27 09:19:21', '2021-09-27 09:19:21'),
(2, 5, '129', '5444445233184', 1, 0, '2021-09-27 09:33:14', '2021-09-27 09:33:14'),
(3, 6, '129', '5444445233184', 1, 0, '2021-09-27 09:34:27', '2021-09-27 09:34:27'),
(4, 7, '129', '5444445233184', 1, 0, '2021-09-27 09:37:41', '2021-09-27 09:37:41'),
(5, 8, '129', '5444445233184', 1, 0, '2021-09-27 09:39:11', '2021-09-27 09:39:11'),
(6, 9, '129', '5444445233184', 1, 0, '2021-09-27 09:39:36', '2021-09-27 09:39:36'),
(7, 10, '129', '5444445233184', 1, 0, '2021-09-27 09:42:37', '2021-09-27 09:42:37'),
(8, 11, '129', '5444445233184', 1, 0, '2021-09-27 09:51:01', '2021-09-27 09:51:01'),
(9, 12, '129', '5444445233184', 1, 0, '2021-09-27 09:52:28', '2021-09-27 09:52:28'),
(10, 13, '129', '5444445233184', 1, 0, '2021-09-27 09:55:05', '2021-09-27 09:55:05'),
(11, 14, '129', '5444445233184', 1, 0, '2021-09-27 09:56:11', '2021-09-27 09:56:11'),
(12, 15, '129', '5444445233184', 1, 0, '2021-09-27 10:10:11', '2021-09-27 10:10:11'),
(13, 16, '129', '5444445233184', 1, 0, '2021-09-27 10:10:43', '2021-09-27 10:10:43'),
(14, 17, '129', '5444445233184', 1, 0, '2021-09-27 10:11:28', '2021-09-27 10:11:28'),
(15, 18, '129', '5444445233184', 1, 0, '2021-09-27 10:11:46', '2021-09-27 10:11:46'),
(16, 19, '129', '5444445233184', 1, 0, '2021-09-27 10:12:28', '2021-09-27 10:12:28'),
(17, 20, '129', '5444445233184', 1, 0, '2021-09-27 10:12:59', '2021-09-27 10:12:59'),
(18, 21, '129', '5444445233184', 1, 0, '2021-09-27 11:18:26', '2021-09-27 11:18:26'),
(19, 22, '129', '5444445233184', 1, 0, '2021-09-27 11:19:06', '2021-09-27 11:19:06'),
(20, 23, '129', '5444445233184', 1, 0, '2021-09-27 11:22:06', '2021-09-27 11:22:06'),
(21, 24, '129', '5444445233184', 1, 0, '2021-09-27 11:23:25', '2021-09-27 11:23:25'),
(22, 25, '129', '5444445233184', 1, 0, '2021-09-27 11:25:43', '2021-09-27 11:25:43'),
(23, 26, '129', '5444445233184', 1, 0, '2021-09-27 11:26:38', '2021-09-27 11:26:38'),
(24, 27, '129', '5444445233184', 1, 0, '2021-09-27 11:26:50', '2021-09-27 11:26:50'),
(25, 28, '129', '5444445233184', 1, 0, '2021-09-27 11:27:56', '2021-09-27 11:27:56'),
(26, 29, '129', '5444445233184', 1, 0, '2021-09-27 11:40:54', '2021-09-27 11:40:54'),
(27, 30, '129', '5444445233184', 1, 0, '2021-09-27 11:46:05', '2021-09-27 11:46:05'),
(28, 31, '129', '5444445233184', 1, 0, '2021-09-27 11:48:42', '2021-09-27 11:48:42'),
(29, 32, '129', '5444445233184', 1, 0, '2021-09-27 11:52:11', '2021-09-27 11:52:11'),
(30, 33, '129', '5444445233184', 1, 0, '2021-09-27 11:52:35', '2021-09-27 11:52:35'),
(31, 34, '129', '5444445233184', 1, 0, '2021-09-27 11:52:58', '2021-09-27 11:52:58'),
(32, 35, '129', '5444445233184', 1, 0, '2021-09-27 13:19:22', '2021-09-27 13:19:22'),
(33, 36, '111', '5444445233184', 1, 0, '2021-09-29 14:35:51', '2021-09-29 14:35:51'),
(34, 40, '129', 'pi_3JgBj0SBDYV29PLY0XYGexeM', 1, 0, '2021-10-02 11:40:09', '2021-10-02 11:40:09'),
(35, 37, '10', 'pi_3JgCFSSBDYV29PLY1KTjqpOU', 1, 0, '2021-10-02 12:13:41', '2021-10-02 12:13:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

DROP TABLE IF EXISTS `payment_gateways`;
CREATE TABLE IF NOT EXISTS `payment_gateways` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stripe_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_gateways`
--

INSERT INTO `payment_gateways` (`id`, `stripe_key`, `stripe_secret`, `created_at`, `updated_at`) VALUES
(1, 'pk_test_51JfrtvSBDYV29PLYmhEC9PycLxtzpkzSv0Gv49vD45pjY70DbidWF8jcFOOIDDXLAmbfhhID1N3csNqWpdUPB6DK006nbwulat', 'sk_test_51JfrtvSBDYV29PLYYDDviapTl1hOhrS3SNXIlWuR0FGotawCM9kRvYyIY1WnsgbzTaJP55O6LSqgEqufwc8B3G4j00ZwoUt57y', '2021-09-23 11:53:21', '2021-10-01 15:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `slug`, `title`, `short_description`, `description`, `created_at`, `updated_at`) VALUES
(2, 'test-blog', 'Test Blog', 'Short description', '<p>complete description</p>', '2021-09-08 20:11:45', '2021-09-08 20:11:45');

-- --------------------------------------------------------

--
-- Table structure for table `post_medias`
--

DROP TABLE IF EXISTS `post_medias`;
CREATE TABLE IF NOT EXISTS `post_medias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_medias_post_id_foreign` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `sku` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selling_price` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchasing_price` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` enum('Active','Deactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sku`, `name`, `description`, `selling_price`, `purchasing_price`, `slug`, `published`, `created_at`, `updated_at`) VALUES
(14, 14, 1, 'Cetirizine', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">An invigorating blend of electrolytes and IV fluids designed to replenish the body before or after intense physical activity so it can function at its best.</span><br></p>', '2000', '1500', 'cetirizine', 'Active', '2021-08-27 09:15:30', '2021-09-02 12:23:26'),
(15, 14, 2, 'Hangover', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">A blend of detoxifying vitamins, minerals, and electrolytes designed to hydrate and revitalize your body, swiftly alleviating symptoms after a night of drinking.</span><br></p>', '129', '100', 'hangover', 'Active', '2021-09-02 12:30:06', '2021-09-02 12:30:06'),
(16, 14, 3, 'Myers Cocktail', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">An original blend named after Dr. John Myers that’s designed to prevent inflammation and treat the symptoms of chronic illnesses like CFS, migraines, fibromyalgia, and asthma.</span><br></p>', '200', '150', 'myers-cocktail', 'Active', '2021-09-02 12:33:17', '2021-09-02 12:33:17'),
(17, 14, 4, 'Energy Lift', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">A special blend of essential vitamins, minerals, and electrolytes that boost your vitality and energy levels when you’re feeling fatigued or overworked.</span><br></p>', '321', '250', 'energy-lift', 'Active', '2021-09-02 12:35:24', '2021-09-02 12:35:24'),
(18, 14, 5, 'Recovery', 'A unique blend of vitamins and minerals that supports immune function, reduces inflammation, and aids muscular healing while recovering from intense exercise, illness, or injury.', '129', '100', 'recovery', 'Active', '2021-09-02 12:37:56', '2021-09-02 12:48:09'),
(19, 14, 6, 'Beauty', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: center;\">A targeted blend of vitamins, minerals, and electrolytes specifically crafted toboost your complexion and promote vibrant wellness and beauty—inside and out.</span><br></p>', '111', '100', 'beauty', 'Active', '2021-09-02 12:39:31', '2021-09-02 12:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE IF NOT EXISTS `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_image` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_images_product_id_foreign` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `path`, `cover_image`, `created_at`, `updated_at`) VALUES
(17, 14, 'uploads/products/61310f9647e55.png', '0', '2021-09-02 12:23:26', '2021-09-02 12:23:26'),
(18, 15, 'uploads/products/6131112691d30.png', '0', '2021-09-02 12:30:06', '2021-09-02 12:30:06'),
(19, 16, 'uploads/products/613111e59e3d7.png', '0', '2021-09-02 12:33:17', '2021-09-02 12:33:17'),
(20, 17, 'uploads/products/6131126469303.png', '0', '2021-09-02 12:35:24', '2021-09-02 12:35:24'),
(21, 18, 'uploads/products/613112fc06df7.png', '0', '2021-09-02 12:37:56', '2021-09-02 12:37:56'),
(22, 19, 'uploads/products/6131135b6b814.png', '0', '2021-09-02 12:39:31', '2021-09-02 12:39:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_inventories`
--

DROP TABLE IF EXISTS `product_inventories`;
CREATE TABLE IF NOT EXISTS `product_inventories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_inventories_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_metas`
--

DROP TABLE IF EXISTS `product_metas`;
CREATE TABLE IF NOT EXISTS `product_metas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `Title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_metas_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_posts`
--

DROP TABLE IF EXISTS `product_posts`;
CREATE TABLE IF NOT EXISTS `product_posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `Title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_posts_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'web', '2021-09-14 17:49:34', '2021-09-14 17:49:34'),
(2, 'admin', 'web', '2021-09-14 17:49:34', '2021-09-14 17:49:34'),
(3, 'nurse', 'web', '2021-09-14 17:49:34', '2021-09-14 17:49:34'),
(4, 'user', 'web', '2021-09-14 17:49:34', '2021-09-14 17:49:34');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
CREATE TABLE IF NOT EXISTS `tests` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`, `slug`, `short_description`, `description`, `created_at`, `updated_at`) VALUES
(10, 'sdf', 'sdf', 'dfgh', '<p>sdcfvgbhn</p>', '2021-09-23 12:45:21', '2021-09-23 12:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `test_media`
--

DROP TABLE IF EXISTS `test_media`;
CREATE TABLE IF NOT EXISTS `test_media` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `test_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `test_media_test_id_foreign` (`test_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `test_media`
--

INSERT INTO `test_media` (`id`, `test_id`, `media`, `created_at`, `updated_at`) VALUES
(8, 10, 'uploads/tests/614cc439a35d0.png', '2021-09-23 12:45:21', '2021-09-23 12:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `time_slots`
--

DROP TABLE IF EXISTS `time_slots`;
CREATE TABLE IF NOT EXISTS `time_slots` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slot` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `address` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `active`, `address`, `latitude`, `longitude`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Supper Admin', 'admin@healthiv.com', NULL, '$2y$10$nKT8qKf5Qf/xgOxGFKs0KO4iMVy00IwtwREwUs6QCyWYfmQ6PKQgG', '1', NULL, NULL, NULL, NULL, '2021-08-16 12:53:43', '2021-09-22 10:59:29'),
(2, 'Customer 1', 'customer1@yopmail.com', NULL, '$2y$10$Kt9vbvD9xquc4qrYeuwUeemaDQzwT1v93RdX1oDqraEIFJPh2vayq', '1', NULL, NULL, NULL, NULL, '2021-09-14 17:58:10', '2021-09-23 12:49:14'),
(3, 'Shahrukh Nurse', 'nurse1@yopmail.com', NULL, '$2y$10$m3Q99Y4KJKJhoGQ/1CpmueBS5Y2Rjd0wBltpSKhRqwqJy3bux9Ula', '0', NULL, NULL, NULL, NULL, '2021-09-14 17:59:13', '2021-09-23 12:50:47'),
(4, 'Shahrukh Nurse2', 'nurse2@yopmail.com', NULL, '$2y$10$4L6D7ZEBTrLVdFLrzDebpO5nWq8KazM4Aj.LlBv7XUrIchYEsUbCq', '1', NULL, '27.1424014', '77.9577073', NULL, '2021-09-14 18:21:39', '2021-09-23 12:50:49'),
(5, 'Shahrukh Custom', 'customer2@yopmail.com', NULL, '$2y$10$veF0GR3hU4cio26Rk90XB.DfRnCXH1yuCqWphTaDM49btXTjEXqF2', '1', NULL, NULL, NULL, NULL, '2021-09-14 18:24:46', '2021-09-23 12:48:58'),
(6, 'Shahrukh Khan', 'nurse5@yopmail.com', NULL, '$2y$10$/dc5RJ9lyx6Gh/Bp3uOXs.wlK6o6t6OqMv5q5rxC2JupHneBE2Pgq', '1', NULL, NULL, NULL, NULL, '2021-09-16 19:55:50', '2021-09-23 12:50:42'),
(7, 'Umesh Rajput', 'demo123@healthiv.com', NULL, '$2y$10$qoh1kykk8ZPlmT0Q9e8ZYu6zMNeDgpKw.OBhHcGY7g/HuY90P8AKW', '1', NULL, NULL, NULL, NULL, '2021-09-23 12:50:17', '2021-09-23 12:50:17'),
(9, 'Sakshi', 'sakshi@gmail.com', NULL, '$2y$10$8utpqc33MbgwPbpPzIvG0u99Y4AKEiBX.B28W6m6eAW2wVk398Fn.', '1', 'Dhanauli, Agra, Uttar Pradesh, India', '27.1797686', '78.0199343', NULL, '2021-09-28 12:27:47', '2021-09-28 12:27:47'),
(10, 'Umesh Rajput', 'rajputumesh117@gmail.com', NULL, '$2y$10$U913q922O2HFK5DJIXScd.vhNppnuIZxxmPl5SeZqohKagJhxrSki', '1', 'Zaiba InfoTech, Jagner Rd, near Ganesh Hospital, Dhanauli, Agra, Uttar Pradesh, India', '27.1424014', '77.9577073', NULL, '2021-09-28 12:30:53', '2021-09-28 12:30:53'),
(11, 'demo testing', 'demo212@gmail.com', NULL, '$2y$10$qVBeSFFJN7fDEXvc8qBoce8JaUGiaPlbIG.5z4K.SXcR6op0mF2FK', '1', NULL, NULL, NULL, NULL, '2021-09-28 14:20:05', '2021-09-28 14:20:05'),
(13, 'Umesh Rajput', 'umesh117@gmail.com', NULL, '$2y$10$A7H3fXL61sJUZtiRT8otHuIZg75YH99dAFGj56j9Lr4Gr0Ji9VD32', '1', 'Sikandra, Uttar Pradesh, India', '26.3678114', '79.6291561', NULL, '2021-09-29 14:35:51', '2021-09-30 00:43:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_medias`
--

DROP TABLE IF EXISTS `user_medias`;
CREATE TABLE IF NOT EXISTS `user_medias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_medias_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_medias`
--

INSERT INTO `user_medias` (`id`, `user_id`, `media`, `created_at`, `updated_at`) VALUES
(2, 1, 'uploads/user_profile/614b59d74b96f.png', '2021-09-22 10:59:11', '2021-09-22 10:59:11'),
(3, 13, 'uploads/user_profile/615553fdce948WhatsApp Image 2021-08-06 at 12.56.07 PM.jpeg', '2021-09-30 00:36:53', '2021-09-30 00:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `waiting_lists`
--

DROP TABLE IF EXISTS `waiting_lists`;
CREATE TABLE IF NOT EXISTS `waiting_lists` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `waiting_lists`
--

INSERT INTO `waiting_lists` (`id`, `email`, `address`, `created_at`, `updated_at`) VALUES
(1, 'shahrukh@yopmail.com', '58/291 A Nagla Mehrab Khan, Sarai Khwaja, Agra 282001', '2021-09-07 16:56:03', '2021-09-07 16:56:03'),
(2, 'shahrukh@healthiv.com', 'test address', '2021-09-08 20:14:57', '2021-09-08 20:14:57'),
(4, 'shahrukh@healthiv.com', 'address where service not available', '2021-09-08 20:15:52', '2021-09-08 20:15:52');

-- --------------------------------------------------------

--
-- Table structure for table `working_days`
--

DROP TABLE IF EXISTS `working_days`;
CREATE TABLE IF NOT EXISTS `working_days` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `day` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `content_medias`
--
ALTER TABLE `content_medias`
  ADD CONSTRAINT `content_medias_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `event_medias`
--
ALTER TABLE `event_medias`
  ADD CONSTRAINT `event_medias_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `location_medias`
--
ALTER TABLE `location_medias`
  ADD CONSTRAINT `location_medias_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `nurse_documents`
--
ALTER TABLE `nurse_documents`
  ADD CONSTRAINT `nurse_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_addresses`
--
ALTER TABLE `order_addresses`
  ADD CONSTRAINT `order_addresses_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD CONSTRAINT `order_payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_medias`
--
ALTER TABLE `post_medias`
  ADD CONSTRAINT `post_medias_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_inventories`
--
ALTER TABLE `product_inventories`
  ADD CONSTRAINT `product_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_metas`
--
ALTER TABLE `product_metas`
  ADD CONSTRAINT `product_metas_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_posts`
--
ALTER TABLE `product_posts`
  ADD CONSTRAINT `product_posts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_medias`
--
ALTER TABLE `user_medias`
  ADD CONSTRAINT `user_medias_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
