@extends('admin.layout.admin-layout')

@section('content')

@include('admin.partials.breadcrumbs')

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">&nbsp;</h3>
                <div class="card-tools">&nbsp;</div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title  text-danger">Waiting list registration address request by visitors on the website and mobile app.</h3>
                                <div class="card-tools">
                                    <?php echo $waiting->render(); ?>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: 300px;">
                                <table class="table table-bordered table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($waiting->isNotEmpty())
                                        @foreach($waiting as $e)
                                        <tr id="record-{{ $e->id }}">
                                            <td>{{ $e->email }}</td>
                                            <td>{{ $e->address }}</td>
                                            <td class="text-center">

                                                <a href="mailto:{{ $e->email }}" class="btn btn-default">
                                                    <i class="fa fa-reply"></i>
                                                </a>

                                                <button onclick="deleteRecord(<?php echo $e->id ?>)" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('pagescript')

@if(\Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif

<script>
    function deleteRecord(id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this record?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((flag) => {
            if (flag) {
                fetch("{{ url('admin/waiting') }}/"+id, {
                    method: "DELETE",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    }
                }).then(response => response.json()).then( res => {
                    if(res.status){
                        document.getElementById('record-'+id).remove();
                        swal("Record deleted!", {
                            icon: "success",
                        });
                    }
                });
            }
        });
    }
</script>
@stop

@endsection